<?php
session_start();
setlocale(LC_ALL,"fr","fr_FR","fr_FR@euro","fr_FR.utf8","fr_FR.UTF-8","fr-FR","fra");
require_once("../jeanp/xdb.php");
if(isset($_GET["ifo"])){
	phpinfo();
	exit;
}
if(isset($_GET["raccoon"])){
	Xdb::reset();?>
	<style>html{background:url("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Raccoon_(Procyon_lotor)_2.jpg/220px-Raccoon_(Procyon_lotor)_2.jpg");}</style>
	<h1 style="text-align:center;background:blue;font-weight:bold;font-size:60px;color:white">RACCOON!</h1>
<?php exit;}?>
<link rel="stylesheet" href=".static/css/style.css">
<script type="text/javascript" src=".static/js/script.min.js"></script>
<div id="pageWrapper">
	<!-- START -->
	<?php require(".data/.php/header.php");?>
	<!-- END -->
	<h1>Bienvenue sur mon <b style="font-size:60px;">superbe</b> portfolio!</h1>
	<h2>Your reaction right now: <b id="emoji">(<a class="rose">╯</a><a class="blue">°</a><a class="red">□</a><a class="blue">°</a>）<a class="rose">╯</a><a class="grey">︵</a> <a class="red">┻━┻</a></b></h2>
	<div id="main">
		<h2>Formulaire de contact</h2>
<?php require(".data/.php/formulaire.php");$get = Xdb::get();if(!empty($get)):?>
		<p class="messages">Corentin, vous avez <b><?=count($get);?></b> message<?php if(count($get) > 1){?>s<?php }?> en absence.</p>
<?php if(isset($_GET["get"])) : ?>
		<table id="messages" border="1">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nom</th>
					<th>Prénom</th>
					<th>Mail</th>
					<th>Message</th>
					<th>Destinataire</th>
					<th>Time</th>
					<!-- <th></th> -->
				</tr>
			</thead>
			<tbody>
<?php foreach($get as $cle => $value) : ?>
				<tr>
					<td><?=$cle + 1;?></td>
					<td><?=$value["nom"];?></td>
					<td><?=$value["prenom"];?></td>
					<td><?=$value["mail"];?></td>
					<td><?=$value["message"];?></td>
					<td><?=$value["destinataire"];?></td>
					<td><?=ucfirst(strftime("%A %e %B %Y à %k:%M:%S",$value["time"]));?></td>
					<!-- <td><input type="checkbox" name="cb" /></td> -->
				</tr>
<?php endforeach; ?>
			</tbody>
		</table>
<?php endif;endif;?>
	</div>
	<footer>
		<h3>Réalisé par PERROT Corentin (Kasai.) | © 2017</h3>
	</footer>
</div>