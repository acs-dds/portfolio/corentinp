<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>Open Source Contributions</title>

		<link rel="stylesheet" href="https://www.codeschool.com/assets/application-740b9dc3c9a2e22a740eaa55aca46cdd2e2f8ce75aed7df7315327a5f480dba3.css">
	</head>
	<body class="beginners_guide beginners_guide-show">
		<main class="content">
			<section class="row">
				<div class="cell well well--l">
					<div class="mv-flexGrid mv-flexGrid--center">
						<div class="mv-flexGrid-box mv-flexGrid-box--4of6_m db">
							<div class="mv-guide">
								<div class="mbm">
									<img class="mv-guide-badge" width="143.0" src="https://www.codeschool.com/assets/custom/beginners-guide/badge-open-source-contributions-c14ce20e411ecc0764fa9936869f8b18a9843c594094f861789f5cf0f8064142.svg" alt="Badge open source contributions">
								</div>
								<div class="mbl tac">
									<h1 class="h h--1 ts48px--m th115 twb">Contributions Open Source</h1>
									<p class="mbf tcs tss">
										<i class="icn icn--clock"></i>
										8 minutes nécessaires
									</p>
								</div>
								<div class="mv-guide-content">
									<p>Un logiciel open source veut généralement dire du code ouvert au public, et nimporte qui est le bienvenue pour proposer des changements ou améliorations. (Gardez en tête, cependant, que parce-que simplement un logiciel open source est ouvert au public ne veut pas dire qu'il est sujet à une licence et aux lois de copyright.)</p>

									<p>La plupart des codes open source du monde vivent aujourd'hui à partir d'un service appelé <a href="https://github.com/">GitHub</a>, qui n'heberge pas seulement du code mais autorise ses membres à gérer et modérer les repositories. Avoir un compte GitHub actif avec "relevant open source contributions" dans leur vue du travail qui est devenu beaucoup plus attractif pour les employés prospectifs et lentement deviennent une mesure standart pour la qualitée d'un ingénieur.</p> <!-- BullShit -->

									<h2>Pourquoi faire du code Open Source ?</h2>

									<p>Cette question est occationnelement posée par ceux qui commencent à coder ou qui ne sont pas habitués vers quelle direction l'industrie avance. C'est une bonne question — pourquoi dans le monde les companies ou même des solistes partageraient leur code avec le reste du monde ? La réponse peut être réduite à un seul mot : <strong>collaboration</strong>.</p>

									<p>La qualité d'un projet ne peut augmenter quand plus de personnes se soucient de son code mettant leur cerveau ensemble pour l'améliorer. Quand plus de personnes sont impliquées dans le développement d'une librairie, plus de cas d'usages sont mis à jour, et le code devient plus robuste à travers le temps au fur et a mesure qu'il devient utilisé par beaucoup de personnes autour du monde.</p>

									<p>En fin de compte, les pro de faire du code open source dépassent largement les inconvénients, mais même avec ça en tête, l'open source n'est pas encore pour tout le monde. Les politiques de certaines entreprises, et même les lois gouvernant quelques institutions publiques, interdisent toujours que le code soit open source, et il n'y as pas de problèmes.</p>

									<h2>5 Étapes Faciles pour Contribuer</h2>

									<p>Si vous est prêt à être remarqué par certaines communautées dont vous vous souciez, et même par des employeurs potentiels, alors il est temps de commencer à contribuer pour l'open source ! Si vous venez de commencer à écrire des logiciels et vous n'êtes sûr de votre niveau, contribuer pour des projects open source est un bon moyen pour appendre comment collaboreravec d'autre développeurs. Ça peut peut être parraître un petit peut intimidant, mais ne le laissez pas vous arreter — la plus part des communautées sont très compréhensibles et accueillent les débutants, tant que vous suivez les règles.</p>

									<p>Passons maintenant à la loi non écrite de la terre dans le monde open source. Imaginez que vous avez utilisé une librairie open source et trouvé un bug dedans. Habituellement le flux de contribution dans le monde open source fonctionne comme ça :</p>

									<h3>1. Lire le fichier CONTRIBUTING</h3>

									<p>Quelques projets open source ont un fichier sur leur racine de répertoire appellé <code>CONTRIBUTING.md</code>. Ce fichier contient habituellement des instruction des étapes préliminaires que vous devez prendre avant de devenir un contributeur.</p>

									<p>Dans ce fichier, ils pourraient vous demander une certaine convention sur la façon de récuperer des bugs, etc. Soyez sûr d'avoir bien lu le fichier et d'avoir suivit les instructions. Voici quelques exemples du fichier <code>CONTRIBUTING</code> dans les repository <a href="https://github.com/jquery/jquery/blob/master/CONTRIBUTING.md">jQuery</a> et <a href="https://github.com/angular/angular/blob/master/CONTRIBUTING.md">Angular</a>.</p>

									<h3>2. Ficher un bug dans le repository du projet</h3>

									<p>À ce stade, ce processus varie selon ce qui à été demandé par les autheurs du projet dans le fichier <code>CONTRIBUTING</code>. Mais en général, la prochaine étape pour contribuer dans un projet open source est de ficher les bugs du projet.</p>

									<p>C'est là que GitHub brille vraiment, car il rend vraiment facile pour quiconque de communiquer avec les auteurs de la bibliothèque en question. Là vous "ouvririez un problème" et informeriez les auteurs du bug que vous avez trouvé et des mesures qu'ils doivent prendre pour reproduire le bogue.</p>

									<p>Le plus d'information que vous écrivez sur le problème, mieux c'est. Donc si vous avez trouvé quelle partie du code provoque le bogue dans le code du projet, n'oubliez pas de leur faire savoir où le problème est et que vous seriez heureux de le corriger pour eux en ouvrant une pull request. La plupart des auteurs accueillerons une "pull request", mais il est généralement considéré de bonne courtoisie d'attendre une réponse de l'auteur dans une "issue" avant d'ouvir la "pull request".</p>

									<h3>3. "Forking" le code</h3>

									<p>Pendant que vous attendez une réponse de l'auteur sur l'"issue" que vous avez ouvert, il n'y a pas de mal à "forker" le code et débutter à travailler sur une solution d'un problème sur votre version du code.</p>

									<p>"Forking" veut dire faire une copie du projet sur votre compte GitHub afin de pouvoir faire librement des changements avant de pouvoir le renvoyer vers le code principal. Le processus de renvoi du code vers le code principal est appellé "pull request", ce qui veut basiquement dire que vous demandez l'autorisation de l'auteur pour "merger" les changements que vous avez faits vers le code princpal.</p>

									<p>Une fois que vous avez fini les changements sur votre version du code, soyez sur de bien rechercher pour n'importe quelle instruction spéciale que l'auteur aurais pu ajoutter dans le <code>CONTRIBUTING.md</code> pour "commit" vos modifications. S'il y a des instructions spéciales, alors suivez leurs conventions. Sinon, alors quelques lignes directrices pour les bon messages de "commit" sont d'être bien décrits sur les modifications que vous avez effectuées, et faites le moins de "commit" possible. Quelques librairies vous demanderons même de <a href="https://ariejan.net/2011/07/05/git-squash-your-latests-commits-into-one/">"squash" tous vous changements en un seul "commit"</a> avant d'ouvrir une "pull request".</p>

									<p>En outre, si la librairie à des tests unitaires, soyez sûr de les lancer et de vous assurer que tout fonctionne proprement avant d'envoyer votre "pull request". Si vous avez ajouté des nouvelles fonctionnalitées qui pourraient nécessiter de nouveaux test, soyez sur de bien les écrire et de les "commit" dans le projet en même temps.</p>

									<h3>4. Faire une "pull request"</h3>

									<p>Une fois que vous avez terminé et êtes fièr de votre code, et que l'auteur vous as donné la permission d'ouvrir une "pull request", "push" votre code vers votre projet GitHub et que la "pull request" est ouverte. Une bonne recommandation quand vous ouvez des "pull request"s est de mentionner l'identifiant de l'"issue" qui se fait modifier avec cette "pull request". C'est aussi facile qu'ajoutter l'#ID-DE-L'"ISSUE" à votre déscription. Pour trouver quel est l'ID de l'issue, cherchez simplement son nom dans la liste des "issue"s, cliquez dessus, et regardez à la fin de l'url. L'ID seras le numéro à la fin de l'url.</p>

									<p>Une fois que votre "pull request" est faite, Il appartiendras à l'auteur de la librairie d'éxaminer votre code et de décider ou non s'ils acceptent les changements que vous avez faits. Habituellement, s'ils voient un problème avec votre code, ils vous le feront savoir afin que vous puissiez y répondre.  Ne vous inquiétez pas si cela arrive — c'est normal. Une fois que vous aurez réglé tous les problèmes ils peuvent | Once your pull request is open, it will be up to the library author to review your code and decide whether or not they are going to accept the changes you made. Usually if they see a problem with your code, they will let you know so you can address it. Don’t worry if that happens — it’s normal. Once you have addressed any problems they might have found and pushed all the latest changes, the author will be able to accept your pull request, causing your code to merge to the original codebase.</p>

									<h3>5. TA-DA! 🎉</h3>

									<p>Congrats! Once your code is accepted, you have officially become an open source contributor. But don’t stop there — keep looking for more ways to help improve that library (and others) by proactively looking at their list of GitHub issues and offering to help fix some of the problems you see you. (Hint: Not everyone who opens an issue is looking to fix it themselves.)</p>

									<h2>What Projects to Start Contributing To</h2>

									<p>It’s generally a good idea to start contributing to smaller projects. So, for example, instead of contributing to the <a href="https://github.com/jquery/jquery">jQuery source code</a>, which gets a lot of attention and has been pretty stable for a few years, it might be a better idea to start with one of the community-backed plugins that were built on top of jQuery.</p>

									<p>Those authors tend to get less attention and usually need a lot of help fixing bugs or writing new features for their codebases. Those smaller libraries also tend to not have as much of a process when contributing, so it’s good to know some of the general unwritten rules laid out above when seeking to help them out.</p>

									<p>You may be surprised to find some authors simply don’t want any help, which is fine. In those cases, it’s better to move on and look for other libraries that are genuinely looking for more help. Again, a great way to get to know an author is by simply opening an issue on their repo and getting a conversation started.</p>

									<p>Sometimes you will also find great projects that have been abandoned. In those cases, feel free to message the authors and let them know you would like to either take over the project or simply help them manage issues and pull requests by other developers. Both of these solutions will help the open source project tremendously, as well as increase your experience as a developer by collaborating with other developers.</p>

									<h3>Contributing With Documentation</h3>

									<p>If contributing to a project’s codebase still feels a little too intimidating, you might want to look into other ways of getting started — documentation, for instance. After all, how good is an open source library if nobody knows how to use it? Many libraries evolve quickly and their documentation often gets overlooked and quickly becomes outdated.</p>

									<p>If you’re looking to start contributing to open source projects and don’t know how to start, consider helping a project with their documentation. Documentation is an essential part of libraries and is usually a low-hanging fruit for prospective contributors that want a foot in the door — not to mention, it helps the project immensely.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
	</body>
</html>