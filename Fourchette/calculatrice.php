<!doctype html>
<html>
	<head>
		<title>Calculator</title>

		<style type="text/css">
			html{background-color:#ccc;width:60%;margin:auto;font-size:0}section > *{font-size:initial}section{background-color:#efefef;padding:1%;padding-bottom:10%;border-radius:7px;}input[type="button"]{outline:none;background-color:#ddd;border:1px solid #888;border-radius:7px;width:20.1%;padding:.5% 1%;font-weight:bold;font-size:15px;margin:2.1%}input[type="number"]{outline:none;border:1px solid #888;border-radius:7px;padding:1% 2.6%;width:40.2%;margin:1.8%}form{margin-bottom:5%}output{box-sizing:border-box;background-color:#ddd;border:1px solid #888;border-radius:7px;margin:0 2%;padding:1.5% 47.7%;width:92%;text-align:center;}
		</style>
	</head>

	<body>
		<section id="fourth">
			<form name="form6" id="form6" method="post" action="#">
				<input type="number" id="s1" name="s1" placeholder="Input 1">
				<input type="number" id="s2" name="s2" placeholder="Input 2">
				<input type="button" id="plus" value="+">
				<input type="button" id="minus" value="-">
				<input type="button" id="multiply" value="*">
				<input type="button" id="divide" value="/">
			</form>
			<output id="output2"></output>
		</section>
		<script type="text/javascript">
			var d = true // Debug
			var o = document.getElementById("output2"); // Output

			document.getElementById("plus").addEventListener("click",
				function(){
					calc("plus");
				}
			);

			document.getElementById("minus").addEventListener("click",
				function(){
					calc("minus");
				}
			);

			document.getElementById("multiply").addEventListener("click",
				function(){
					calc("multiply");
				}
			);

			document.getElementById("divide").addEventListener("click",
				function(){
					calc("divide");
				}
			);
			
			/**
			 * calc function.
			 * 
			 * @param str param
			 * @return object
			 */
			var calc = function(param){
				s1 = parseFloat(document.getElementById("s1").value);
				s2 = parseFloat(document.getElementById("s2").value);

				o.style.display = "block";

				console.log(s1);
				console.log(s2);

				if((isNaN(s1)) && (isNaN(s2))){
					o.value = "Please insert values";
					o.style.padding = "1.5%";
				}
				else{
					o.style.padding = "1.5%";
					if(param == "plus")
						o.value = s1 + s2;
					if(param == "minus")
						o.value = s1 - s2;
					if(param == "multiply")
						o.value = s1 * s2;
					if(param == "divide")
						o.value = s1 / s2;

					document.getElementById("s1").value = "";
					document.getElementById("s2").value = "";
				}
			}
		</script>
	</body>
</html>