<!doctype html>
<html>
	<head>
		<title>JS Scripts</title>

		<style type="text/css">
			body{width:95%;margin:auto;}#form,#form2,#form3,#form4,#form5,#form6{padding:1%;border-radius:10px;border:2px solid black}#send,#send2,#input,#receive,#s1,#s2{margin:1em;height:2em;padding-left:.5em}#clue,output,#output{display:none;border:1px #000 dashed;margin:1em;line-height:2em;padding-left:.5em}#retry{display:none;height:2em}#form2{margin-bottom:1%}#poop{animation:colored 1s infinite}@keyframes colored{0%{color:red;font-size:15px;font-family:arial;transform:rotate(360deg);}10%{color:brown;font-size:25px;font-family:"Monoton";transform:rotate(0deg);}15%{color:yellow;font-size:15px;font-family:cursive;transform:rotate(360deg);}25%{color:white;font-size:25px;font-family:arial;transform:rotate(0deg);}50%{color:blue;font-size:15px;font-family:"Monoton";transform:rotate(360deg);}75%{color:pink;font-size:25px;font-family:cursive;transform:rotate(0deg);}100%{color:red;font-size:15px;font-family:arial;transform:rotate(360deg);}}#first{width:41%}#second{width:41%}#js-error,#change,#guess,#guess2,#plus,#minus,#multiply,#divide,#touche{height:2em}#input{width:13%}::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,0.3);background-color:#CCCCCC;border-radius:10px;}::-webkit-scrollbar{width:10px;background-color:#F5F5F5;}::-webkit-scrollbar-thumb{border-radius:10px;background-image:-webkit-gradient(linear,left bottom,left top,color-stop(0.44,rgb(122,153,217)),color-stop(0.72,rgb(73,125,189)),color-stop(0.86,rgb(28,58,148)));}
		</style>
	</head>
	<body id="body" onload="demo.init()">
		<script type="text/javascript">
			const SETTINGS = {
				rebound:{
					tension:20,
					friction:5
				},
				spinner:{
					id:"spinner",
					radius:90,
					sides:3,
					depth:15,
					colors:{
						/*background:"#231E76",
						stroke:"#231E76",*/
						background:"#f0f0f0",
						stroke:"#272633",
						base:null,
						child:"#272633"
					},
					alwaysForward:true, // When false the spring will reverse normally.
					restAt:0.5, // A number from 0.1 to 0.9 || null for full rotation
					renderBase:false
				}
			};
		</script>
		<section id="first">
			<form id="form">
				<input type="number" id="input" name="min" placeholder="Min">
				<input type="number" id="input" name="max" placeholder="Max">
				<input type="number" id="input" name="a" placeholder="Attemps">
				<input type="button" id="js-error" value="Send">
			</form>
			<p></p>
			<form name="computer" id="form2" method="post" action="javascript:guess();">
				<h1>Try to find out the number!</h1>
				<hr>
				<p>U must found out the number between <b id="min"></b> &amp; <b id="max"></b>. U've got only <b id="attemps"></b> attemps. Think carrefully!</p>
				<b>U'r choice :</b> 
				<input type="number" id="send" name="send" placeholder="Number">
				<input type="button" id="guess" value="Try to guess">
				<input type="button" id="change" value="Change the type of the input">
			</form>
			<p id="clue"></p>
			<input type="button" id="retry" value="retry">
		</section><br><hr id="hr"><br>
		<section id="second">
			<form id="form3">
				<input type="number" id="input" name="min" placeholder="Min">
				<input type="number" id="input" name="max" placeholder="Max">
				<input type="number" id="input" name="a" placeholder="Attemps">
				<input type="button" id="js-error" value="Send">
			</form>
			<p></p>
			<form name="player" id="form4" method="post" action="javascript:guess2();">
				<h1>Try to make me guess out your number!</h1>
				<hr>
				<p>U must think of a (random) number between <b>0</b> &amp; <b>1000</b>. I've got only <b>10</b> attemps. Think carrefully!</p>
				<b>U'r choice :</b> 
				<input type="text" id="receive" name="send" placeholder="Text">
				<input type="button" id="guess2" value="Try to guess">
			</form>
		</section><br><hr id="hr"><br>
		<section id="third">
			<form name="form5" id="form5" action="javascript:touche();">
				<input type="text" id="send2" name="send" placeholder="Hello world!">
				<input type="button" id="touche" value="Send">
			</form>
			<p id="output"></p>
		</section><br><hr id="hr"><br>
		<section id="fourth">
			<form name="form6" id="form6">
				<input type="number" id="s1" name="s1" placeholder="Input 1">
				<input type="number" id="s2" name="s2" placeholder="Input 2">
				<input type="button" id="plus" value="+">
				<input type="button" id="minus" value="-">
				<input type="button" id="multiply" value="*">
				<input type="button" id="divide" value="/">
			</form>
			<output id="output2"></output>
		</section>

		<script type="text/javascript">
			var u = 1000;                               // Max (Upper)
			var l = 0;                                  // Min (Lower)
			var c = 10;                                 // Choosen Attemps (Well, yeah ther nothing more appealing.)
			var a = 0;                                  // Attemps
			var e = false;                              // Error
			var d = true;                               // Debug
			var o = document.getElementById("output2"); // Output

			/////////////////////////////////////////////////////////////////
			////					Get URL Param						 ////
			/////////////////////////////////////////////////////////////////

			function getParamValue(param){
				var urlParamString = location.search.split(param + "=");
				if(urlParamString.length <= 1)
					return "";
				else{
					var tmp = urlParamString[1].split("&");
					return tmp[0];
				}
			}

			if((getParamValue("omin") != "") && (getParamValue("omax") != "") && (getParamValue("a") !== "")){
				if((getParamValue("omin") > getParamValue("omax")) && (getParamValue("omax") < getParamValue("omin"))){
					write("min",getParamValue("omin"));
					write("max",getParamValue("omax"));
					write("attemps",getParamValue("a"));
					write("clue","Your <b>min</b> is bigger than the <b>max</b>! Please change it.");
					document.getElementById("clue").style.display = "block";
					e = true;
				}
				else{
					var r = Math.floor(parseInt(getParamValue("omin")) + (parseInt(getParamValue("omax")) - parseInt(getParamValue("omin")) + 1) * Math.random());

					write("min",getParamValue("omin"));
					write("max",getParamValue("omax"));
					write("attemps",getParamValue("a"));

					u = getParamValue("omax");
					l = getParamValue("omin");
					c = getParamValue("a");
				}
			}
			else{
				var r = Math.floor(Math.random() * 1000) + 1;
				write("min","0");
				write("max","1000");
				write("attemps","10");
			}

			/////////////////////////////////////////////////////////////////
			////				Log the Random number					 ////
			/////////////////////////////////////////////////////////////////

			if(d)
				console.log(r);

			/////////////////////////////////////////////////////////////////
			////					Utils Functions						 ////
			/////////////////////////////////////////////////////////////////

			function write(id,text){
				return document.getElementById(id).innerHTML = text;
			}

			function style(id,var2){
				return document.getElementById(id).style.display = var2;
			}

			/////////////////////////////////////////////////////////////////
			////					Event Listeners						 ////
			/////////////////////////////////////////////////////////////////

			document.getElementById("js-error").addEventListener("click",
				function(){
					alert("HTML don't support the file protocol !\n\nAdd url args to change the random number like '?omin=0&omax=50&a=10'");
				}
			);

			document.getElementById("change").addEventListener("click",
				function(){
					if(document.getElementById("send").type == "number"){
						document.getElementById("send").type = "text";
						document.getElementById("send").placeholder = "Text";
					}
					else{
						document.getElementById("send").type = "number";
						document.getElementById("send").placeholder = "Number";
					}
				}
			);

			document.getElementById("guess").addEventListener("click",guess);

			document.getElementById("guess2").addEventListener("click",guess2);

			document.getElementById("retry").addEventListener("click",location.reload);

			document.getElementById("touche").addEventListener("click",touche);

			document.getElementById("plus").addEventListener("click",
				function(){
					calc("plus");
				}
			);

			document.getElementById("minus").addEventListener("click",
				function(){
					calc("minus");
				}
			);

			document.getElementById("multiply").addEventListener("click",
				function(){
					calc("multiply");
				}
			);

			document.getElementById("divide").addEventListener("click",
				function(){
					calc("divide");
				}
			);

			/////////////////////////////////////////////////////////////////
			////					Guess Functions						 ////
			/////////////////////////////////////////////////////////////////

			/**
			 * guess function.
			 *
			 * @param int min
			 * @param int max
			 * @return object
			 */
			function guess(min,max){
				var s = document.getElementById("send").value;

				style("clue","block");

				if(e){
					style("clue","block");
					write("clue","Your <b>min</b> is bigger than the <b>max</b>! Please change it.");
				}
				else{
					if(s != ""){
						if((s > u) || (s < l)){
							write("clue","Your number: <b>" + s + "</b>, is not between <b>" + l + "</b> et <b>" + u + "</b>, Please change it!");
						}
						else{
							if(isNaN(s)){
								write("clue","I'm verry sorry but i only understand <b>numbers</b>.");
							}
							else{
								a++;

								if(s != r){
									if(s < r)
										write("clue","Nope, it's bigger than <b>" + s + "</b>.");
										var i = "Bigger";
									if(s > r)
										write("clue","Nope, it's smaller than <b>" + s + "</b>.");
										var i = "Smaller";
									if(a >= c){
										write("clue","Well, sorry u'r too dumb for this game,<br>btw the correct answer (the one u didn't get) was: <b>" + a + "</b>");

										style("clue","block");
										style("form","none");
										style("form2","none");
										style("form3","none");
										style("form4","none");
										style("hr","none");

										setTimeout(function(){
											document.getElementById("retry").value = "Sorry I'm too dumb for this game please let me retry!";
											setTimeout(function(){
												style("retry","block");
											},2000);
										},4000);
									}

									if(d)
										console.log("Attemp n°%c" + a + "%c: %c" + s + "%c | Next expected answer (%c" + s/2 + "%c or %c" + s*2 + "%c)","font-weight:bold","font-weight:normal","font-weight:bold","font-weight:normal","font-weight:bold","font-weight:normal","font-weight:bold","font-weight:normal");
								}
								else{
									if(a == 1){
										style("form","none");
										style("form2","none");
										style("form3","none");
										style("form4","none");
										style("hr","none");

										write("clue","<b>Wow u just cheat, u'r too smart!</b>");

										setTimeout(function(){
											write("clue","<b>Please, restart and don't cheat!</b>");
											document.getElementById("retry").value = "Sorry I'm too dumb for this game please let me retry!";
											setTimeout(function(){
												style("retry","block");
											},2000);
										},4000);

										if(d)
											console.log("%cYou Cheat!%c- In %c" + a + "%c attemp(s) - The correct answer was: %c" + s,"font-weight:bold;color:red;font-size:20px","font-weight:normal","font-weight:bold;font-size:20px","font-weight:normal","font-weight:bold;font-size:20px");
									}
									else{
										write("clue","Wow u'r so smart u found out the correct answer in only <b>" + a + "</b> attemp(s)!");
										style("form","none");
										style("retry","block");

										setTimeout(function(){
											write("","");
										})

										if(d)
											console.log("%cYou Win!%c- In %c" + a + "%c attemp(s) - The correct answer was: %c" + s,"font-weight:bold;color:red;font-size:20px","font-weight:normal","font-weight:bold;font-size:20px","font-weight:normal","font-weight:bold;font-size:20px");
									}
								}

								document.getElementById("send").value = "";
							}
						}
					}
					else{
						write("clue","Please enter a number for playing!");
					}
				}
			}

			/**
			 * guess2 function.
			 *
			 * @return object
			 */
			function guess2(){
				var ordi = 50;
				var player;
				var min = 0;
				var max = 101;

				while(player != "!"){
					player = prompt("Plus grand ou plus petit que " + ordi + " ?");

					if(player == "+"){
						min = ordi;
						ordi = Math.floor((min + max) / 2);
					}
					if(player == "-"){
						max = ordi;
						ordi= Math.floor((min + max) / 2);
					}
				}
				if(player == "!"){
					alert("YEEESSS EZ GAME");
				}
			}

			/**
			 * touche function.
			 * 
			 * @return object
			 */
			function touche(){
				str = document.getElementById("send2").value;

				document.getElementById("output").style.display = "block";

				if(str != ""){
					if(str.match(/[bmp]+/gi) != null)
						write("output","<b>" + str + "</b>, Ça touche");
					else
						write("output","<b>" + str + "</b>, Ça touche pas");
				}
				else
					write("output","<b>Sorry i don't understand that!<br>*.*</b>");
			}

			/**
			 * calc function.
			 * 
			 * @param str param
			 * @return object
			 */
			function calc(param){
				s1 = Number(document.getElementById("s1").value);
				s2 = Number(document.getElementById("s2").value);

				o.style.display = "block";

				if(param == "plus")
					return o.value = s1 + s2;
				if(param == "minus")
					return o.value = s1 - s2;
				if(param == "multiply")
					return o.value = s1 * s2;
				if(param == "divide")
					return o.value = s1 / s2;
			}
		</script>
		<script src="js/main.js"></script>
	</body>
</html>