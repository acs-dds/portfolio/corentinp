<?php
$themename = "Fansub";
$shortname = "fs";

$options = array (

	array(	"name" => "Torrent:",
			"type" => "title"),
			
	array(	"type" => "open"),
	
	array(	"name" => "NyaaTorrent User ID",
			"desc" => "example: http://www.nyaa.se/?user=73859, this is user ID NyaaTorrent > 73859",
            "id" => $shortname."_nyaauser",
            "type" => "text"),

	array(	"name" => "DDL:",
			"type" => "title"),
			
	array(	"type" => "open"),

	array(	"name" => "URL DDL",
			"desc" => "example ddl page: http://fffddl.org/, it must end with a slash '/' ",
            "id" => $shortname."_ddl",
            "type" => "text"),

	array(	"name" => "IRC Rizon:",
			"type" => "title"),
			
	array(	"type" => "open"),

	array(	"name" => "Rizon Channel",
			"desc" => "example channel: rizon, without '#'. This just for network Rizon ",
            "id" => $shortname."_irc",
            "type" => "text"),

	array(	"name" => "Themes:",
			"type" => "title"),
			
	array(	"type" => "open"),

	array(	"name" => "Change General Style",
			"desc" => "choose: 'style.css' for light mode or 'dark.css' for dark mode",
            "id" => $shortname."_style",
            "type" => "select",
	    "options" => array("style.css" => "style.css", "dark.css" => "dark.css", "akastyle.css" => "akastyle.css"),
            "std" => "style.css",),

	array(	"name" => "Change Homepage Post Style",
			"desc" => "choose: 'list.php' for list style or 'anipost.php' for anime post style or 'grid.php' for grid mode",
            "id" => $shortname."_post",
            "type" => "select",
	    "options" => array("list.php" => "list.php", "anipost.php" => "anipost.php", "grid.php" => "grid.php"),
            "std" => "list.php",),
			
	array(	"type" => "close")
	
);

function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {
    
        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

                foreach ($options as $value) {
                    if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

                header("Location: themes.php?page=functions.php&saved=true");
                die;

        } else if( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
                delete_option( $value['id'] ); }

            header("Location: themes.php?page=functions.php&reset=true");
            die;

        }
    }

    add_theme_page($themename." Options", "".$themename." Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
    
?>
<div class="wrap">
<h2><?php echo $themename; ?> settings</h2>

<form method="post">



<?php foreach ($options as $value) { 
    
	switch ( $value['type'] ) {
	
		case "open":
		?>
        <table width="100%" style="background-color: #FFFFFF; padding:5px 10px;border: 1px solid #DDD;border-top: 0;margin-bottom: 10px;">
		
        
        
		<?php break;
		
		case "close":
		?>
		
        </table><br />
        
        
		<?php break;
		
		case "title":
		?>
		<table width="100%" style="background-color: #67ADFA; color: #FFF;padding:5px 10px;border: 1px solid #DDD;"><tr>
        	<td colspan="2"><h3 style="font-family: segoe ui;font-weight: bold;color: #FFF;"><?php echo $value['name']; ?></h3></td>
        </tr>
                
        
		<?php break;

		case 'text':
		?>
        
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><input style="width:400px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" /></td>
        </tr>

        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php 
		break;
		
		case 'textarea':
		?>
        
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><textarea name="<?php echo $value['id']; ?>" style="width:400px; height:200px;" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?></textarea></td>
            
        </tr>

        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php 
		break;
		
		case 'select':
		?>
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><select style="width:240px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php foreach ($value['options'] as $option) { ?><option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?></select></td>
       </tr>
                
       <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
       </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php
        break;
            
		case "checkbox":
		?>
            <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
                <td width="80%"><? if(get_settings($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = ""; } ?>
                        <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
                        </td>
            </tr>
                        
            <tr>
                <td><small><?php echo $value['desc']; ?></small></td>
           </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>
            
        <?php 		break;
	
 
} 
}
?>

<!--</table>-->

<p class="submit">
<input name="save" type="submit" value="Save changes" />    
<input type="hidden" name="action" value="save" />
</p>
</form>
<form method="post">
<p class="submit">
<input name="reset" type="submit" value="Reset" />
<input type="hidden" name="action" value="reset" />
</p>
</form>

<?php
}

add_action('admin_menu', 'mytheme_add_admin'); ?>
<?php
global $options;
foreach ($options as $value) {
	if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); }
}
?>
<?php

if ( function_exists('register_sidebar') )
    register_sidebar(array(
    	'name' => 'Sidebar Right',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
register_sidebar( array(
'name' => 'Bot XDCC',
'id' => 'botxdcc',
'description' => 'Appears in the XDCC area',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h3>',
'after_title' => '</h3>',
) );

add_action( 'init', 'register_my_menus' );

function register_my_menus() {
register_nav_menus(
array(
'main' => __( 'Header Menu' )
)
);}
if ( function_exists( 'add_theme_support' ) ) { 
add_theme_support( 'post-thumbnails' );
}
$args = array(
	'flex-width'    => true,
	'width'         => 960,
	'flex-height'    => true,
	'height'        => 225,
	'random-default'  => true,
	'default-image' => get_template_directory_uri() . '/images/headers/header.jpg',
);
add_theme_support( 'custom-header', $args );

add_filter( 'wp_title', 'filter_wp_title' );
/**
 * Filters the page title appropriately depending on the current page
 *
 * This function is attached to the 'wp_title' fiilter hook.
 *
 * @uses	get_bloginfo()
 * @uses	is_home()
 * @uses	is_front_page()
 */
function filter_wp_title( $title ) {
	global $page, $paged;

	if ( is_feed() )
		return $title;

	$site_description = get_bloginfo( 'description' );

	$filtered_title = $title . get_bloginfo( 'name' );
	$filtered_title .= ( ! empty( $site_description ) && ( is_home() || is_front_page() ) ) ? ' – ' . $site_description: '';
	$filtered_title .= ( 2 <= $paged || 2 <= $page ) ? ' – ' . sprintf( __( 'Page %s' ), max( $paged, $page ) ) : '';

	return $filtered_title;
}

add_action( 'widgets_init', 'chatango_widget'); 
 
function chatango_widget() {
register_widget( 'chatango_widget_info' );
}
 
class chatango_widget_info extends WP_Widget {
 
//Name the widget, here Buffercode Widget will be displayed as widget name, $widget_ops may be an array of value, which may holds the title, description like that.
 
function chatango_widget_info () {
 
                                $this->WP_Widget('chatango_widget_info', 'Light Chatango Widget', $widget_ops );        }
 
//Designing the form widget, which will be displayed in the admin dashboard widget location.
 
public function form( $instance ) {
 
if ( isset( $instance[ 'name' ]) && isset ($instance[ 'domain' ]) ) {
$name = $instance[ 'name' ];
$domain = $instance[ 'domain' ];
}
else {
$name = __( '', 'cth_widget_title' );
$domain = __( '', 'cth_widget_title' );
} ?>
<p>Title: <input class="widefat" name="<?php echo $this->get_field_name( 'name' ); ?>" type="text" value="<?php echo esc_attr( $name );?>" /></p>
 
<p>Chatango Name: <input class="widefat" name="<?php echo $this->get_field_name( 'domain' ); ?>" type="text" value="<?php echo esc_attr( $domain ); ?>" /></p>
 
<?php
 
}
 
// update the new values in database
 
function update($new_instance, $old_instance) {
 
$instance = $old_instance;
 
$instance['name'] = ( ! empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
 
$instance['domain'] = ( ! empty( $new_instance['domain'] ) ) ? strip_tags( $new_instance['domain'] ) : '';
 
return $instance;
 
}
 
//Display the stored widget information in webpage.
 
function widget($args, $instance) {
 
extract($args);
 
echo $before_widget;
 
$name = apply_filters( 'widget_title', $instance['name'] );
 
$domain = empty( $instance['domain'] ) ? '&nbsp;' : $instance['domain'];
 
if ( !empty( $name ) ) { echo $before_title . $name . $after_title; };
echo "<div id=\"chatango\"><script id=\"sid0010000072389344604\">(function() {function async_load()\n"; 
echo "{s.id=\"cid0010000072389344604\";s.src='http://st.chatango.com/js/gz/emb.js';s.style.cssText=\"width:300px;height:440px;\";s.asy\n"; 
echo "nc=true;s.text='{\"handle\":\"$domain\",\"styles\":{\"b\":1,\"f\":0,\"i\":0,\"l\":\"DDDDDD\",\"o\":0,\"q\":\"DDDDDD\",\"r\":100,\"v\":0}}';var \n"; 
echo "ss = document.getElementsByTagName('script');for (var i=0, l=ss.length; i < l; i++){if (ss[i].id=='sid0010000072389344604')\n"; 
echo "{ss[i].id +='_';ss[i].parentNode.insertBefore(s, ss[i]);break;}}}var s=document.createElement('script');if \n"; 
echo "(s.async==undefined){if (window.addEventListener) {addEventListener('load',async_load,false);}else if (window.attachEvent) \n"; 
echo "{attachEvent('onload',async_load);}}else {async_load();}})();</script></div>\n";
echo $after_widget; //Widget ends printing information
} }

add_action( 'widgets_init', 'bot_widget'); 
 
function bot_widget() {
register_widget( 'bot_widget_info' );
}
 
class bot_widget_info extends WP_Widget {
function bot_widget_info () {
 
                                $this->WP_Widget('bot_widget_info', 'Bot XDCC Widget', $widget_ops );        }
  
public function form( $instance ) {
if ( isset( $instance[ 'hostxdcc' ]) && isset ($instance[ 'bot' ]) ) {
$hostxdcc = $instance[ 'hostxdcc' ];
$bot = $instance[ 'bot' ];
}
else {
$hostxdcc = __( '', 'botx_widget_title' );
$bot = __( '', 'botx_widget_title' );
} ?>
<p>Host XDCC: <input class="widefat" name="<?php echo $this->get_field_name( 'hostxdcc' ); ?>" type="text" value="<?php echo esc_attr( $hostxdcc );?>" /></p> 
<p>Bot Name: <input class="widefat" name="<?php echo $this->get_field_name( 'bot' ); ?>" type="text" value="<?php echo esc_attr( $bot ); ?>" /></p> 
<?php 
}
 
function update($new_instance, $old_instance) { 
$instance = $old_instance; 
$instance['hostxdcc'] = ( ! empty( $new_instance['hostxdcc'] ) ) ? strip_tags( $new_instance['hostxdcc'] ) : ''; 
$instance['bot'] = ( ! empty( $new_instance['bot'] ) ) ? strip_tags( $new_instance['bot'] ) : ''; 
return $instance;
}
 
 
function widget($args, $instance) {
extract($args);
echo $before_widget;
$hostxdcc = empty( $instance['hostxdcc'] ) ? '&nbsp;' : $instance['hostxdcc']; 
$bot = empty( $instance['bot'] ) ? '&nbsp;' : $instance['bot']; 
echo "<li><a href=\"?host=$hostxdcc&bot=$bot\">$bot</a></li>";
} }

add_action( 'widgets_init', 'dark_cth_widget'); 
 
function dark_cth_widget() {
register_widget( 'dark_cth_widget_info' );
}
 
class dark_cth_widget_info extends WP_Widget {
  
function dark_cth_widget_info () {
 
                                $this->WP_Widget('dark_cth_widget_info', 'Dark Chatango Widget', $widget_ops );        }
  
public function form( $instance ) {
 
if ( isset( $instance[ 'namewid' ]) && isset ($instance[ 'dark' ]) ) {
$namewid = $instance[ 'namewid' ];
$dark = $instance[ 'dark' ];
}
else {
$namewid = __( '', 'cth_widget_title' );
$dark = __( '', 'cth_widget_title' );
} ?>
<p>Title: <input class="widefat" name="<?php echo $this->get_field_name( 'namewid' ); ?>" type="text" value="<?php echo esc_attr( $namewid );?>" /></p>
 
<p>Chatango Name: <input class="widefat" name="<?php echo $this->get_field_name( 'dark' ); ?>" type="text" value="<?php echo esc_attr( $dark ); ?>" /></p>
 
<?php
 
}
  
function update($new_instance, $old_instance) {
$instance = $old_instance;
$instance['namewid'] = ( ! empty( $new_instance['namewid'] ) ) ? strip_tags( $new_instance['namewid'] ) : '';
$instance['dark'] = ( ! empty( $new_instance['dark'] ) ) ? strip_tags( $new_instance['dark'] ) : '';
return $instance;
}
 
function widget($args, $instance) {
 
extract($args);
 
echo $before_widget;
 
$namewid = apply_filters( 'widget_title', $instance['namewid'] );
 
$dark = empty( $instance['dark'] ) ? '&nbsp;' : $instance['dark'];
 
if ( !empty( $namewid ) ) { echo $before_title . $namewid . $after_title; };
echo "<div id=\"chatango\"><script id=\"sid0010000072903977977\">(function() {function async_load(){s.id=\"cid0010000072903977977\";s.src='http://st.chatango.com/js/gz/emb.js';s.style.cssText=\"width:300px;height:440px;\";s.async=true;s.text='{\"handle\":\"$dark\",\"styles\":{\"a\":\"000000\",\"b\":37,\"c\":\"FFFFFF\",\"d\":\"CCCCCC\",\"e\":\"000000\",\"f\":50,\"g\":\"F6F6F4\",\"i\":80,\"k\":\"666666\",\"l\":\"333333\",\"m\":\"000000\",\"n\":\"FFFFFF\",\"r\":5,\"s\":1,\"t\":0,\"v\":0,\"w\":0,\"ab\":0}}';var ss = document.getElementsByTagName('script');for (var i=0, l=ss.length; i < l; i++){if (ss[i].id=='sid0010000072903977977'){ss[i].id +='_';ss[i].parentNode.insertBefore(s, ss[i]);break;}}}var s=document.createElement('script');if (s.async==undefined){if (window.addEventListener) {addEventListener('load',async_load,false);}else if (window.attachEvent) {attachEvent('onload',async_load);}}else {async_load();}})();</script></div>\n";
echo $after_widget; //Widget ends printing information
} }
?>