<div class="anipost">
<?php if ( has_post_thumbnail() ) { ?> <?php the_post_thumbnail(thumbnail); ?> <?php } else { ?>
<img src="#" /> <?php } ?>
<div class="left">
<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<span><b><i class="icon-user"></i> Posted by</b> <?php the_author(); ?>, <b><i class="icon-calendar"></i> released on</b> <?php the_time('F jS, Y'); ?>, <?php the_time('g:i a'); ?></span>
<span><b><i class="icon-comment"></i> Comments</b> <?php echo $post->comment_count?><?php echo ($post->comment_count==1?' comment':' comments');?>, <b><i class="icon-folder-open"></i> Category</b> <?php the_category(' '); ?></span>
<span><div class="fb-like" data-href="<?php the_permalink() ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></span>
<span class="viewer"><?php if(function_exists('the_views')) { the_views(); } ?></span>
</div>
</div>