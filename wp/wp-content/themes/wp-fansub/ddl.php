<?php 
/*
Template Name: DDL
*/
get_header(); ?>
<div id="content">
<?php
global $options;
foreach ($options as $value) {
	if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); }
}
?>
<div class="ddl">

<div class="headpost"><h1>DDL</h1></div>
<ul>
<?php
echo "<meta charset='utf-8'/>";
if(!isset($_GET['path']))
{
        $contents = file_get_contents("$fs_ddl");
        preg_match_all("/<a href=\"(.*?)\">(.*?)<\/a>/", $contents, $elements);
        foreach($elements[1] as $element)
        {
                if($element != "../")
                {
                        $string=str_replace("/","",urldecode($element));
                        $encoded_string = str_replace("/","", $element);
                        echo "<li><a href='?path=$encoded_string'>".$string."</a></li>".PHP_EOL;
                }
        }
} else {
                $uri="$fs_ddl".$_GET['path'];
                $contents = file_get_contents($uri);
                $uri_back=dirname(str_replace("$fs_ddl","?path=",$uri));
                if($uri_back!="http:" && $uri_back != ".")
                {
                echo "<a href='$uri_back'>← Back</a><br/>\n";
                }
                else
                {
                echo "<a href='?'>← Back</a><br/>\n";
                }
                preg_match_all("/<a href=\"(.*?)\">(.*?)<\/a>/", $contents, $elements);
                foreach($elements[1] as $element)
                {
                        if($element != "../")
                        {
                                $c = curl_init();
                                curl_setopt_array($c, array(
                                CURLOPT_URL => "$fs_ddl".rtrim($_GET['path'],"/")."/".$element,
                                CURLOPT_HEADER=>true,
                                CURLOPT_RETURNTRANSFER=>true,
                                CURLOPT_NOBODY => true
                                ));
                                $respon=curl_exec($c);
                                if(preg_match("/application\/octet-stream/", $respon, $gakpenting) <1)
                                {
                                        $string=str_replace("/","",urldecode($element));
                                        $encoded_string = rtrim($_GET['path'],"/") . "/" . str_replace("/","", $element);
                                        echo "<li><a href='?path=$encoded_string'>".$string."</a></li>".PHP_EOL;
                                }
                                else
                                {
                                        $string=str_replace("/","",urldecode($element));
                                        $encoded_string = rtrim($_GET['path'],"/") . "/" . str_replace("/","", $element);
                                        echo "<li><a href='$fs_ddl$encoded_string'>".$string."</a></li>".PHP_EOL;
                                }
                        }
                }
}
?>
</ul>
</div>

</div>
<?php get_footer(); ?>