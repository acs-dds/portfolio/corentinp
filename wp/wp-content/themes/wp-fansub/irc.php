<?php 
/*
Template Name: IRC Rizon
*/
get_header(); ?>
<?php
global $options;
foreach ($options as $value) {
	if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); }
}
?>
<div id="content">
<div id="irc">
<div class="headpost" style="margin-bottom: 0;"><h1>IRC Rizon Network - <a href="irc://irc.rizon.net/<?php echo $fs_irc; ?>">#<?php echo $fs_irc; ?>@irc.rizon.net</a></h1></div>
<iframe src="http://qchat.rizon.net/?channels=<?php echo $fs_irc; ?>&uio=d4" width="99.8%" height="500" style="border: none;width: 99.8%;"></iframe>
<style>
.qwebirc-qui .lines {
color: black;
overflow: auto;
font-size: 0.8em;
background: #FFF;
}
</style>
</div>
</div>
<?php get_footer(); ?>