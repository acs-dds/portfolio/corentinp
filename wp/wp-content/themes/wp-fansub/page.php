<?php get_header(); ?>

<div id="content">

<div class="postsbody">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="headpost"><h1><?php the_title(); ?></h1></div>

<div class="bodypost">
<?php the_content( ); ?>
<?php link_pages('<p><strong>Pages:</strong> ', '</p>', 'number'); ?>
</div>

<?php endwhile; endif; ?>
</div>


<?php include (TEMPLATEPATH . '/sidebar_right.php'); ?>

</div>

</div>
<?php get_footer(); ?>