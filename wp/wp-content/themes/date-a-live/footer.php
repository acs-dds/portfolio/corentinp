			</div><!-- #main -->
			<footer id="footer">
				<ul class="menu">
					<li><a target="_blank" href="https://kasai.moe">ミミック-プロジェクト</a></li>
					<li><a target="_blank" href="https://dev.kasai.moe/wp/dal">デモ</a></li>
					<li><a target="_blank" href="http://www.compileheart.com">コンパイルハート</a></li>
					<li><a target="_blank" href="http://www.compileheart.com/date">デート・ア・ライブ</a></li>
					<li><a target="_blank" href="https://wordpress.org">ワードプレス</a></li>
				</ul>
				<?php noel_credit();?>

			</footer><!-- #footer -->
		</section><!-- #wrapper -->
		<div class="noel-toolbar">
			<?php noel_facebook();?>

			<?php noel_twitter();?>

			<?php noel_googleplus();?>

			<?php noel_rss();?>


			<form id="searchform">
				<input type="text" class="field" name="s" id="s" placeholder="<?php esc_attr_e('Search','noel');?>" />
			</form>
		</div><!-- .noel-toolbar -->
		<?php wp_footer(); ?>

	</body>
</html>