﻿<?php
require_once("globals.php");

if(!current_user_can("fansub-manager")){
	die("Acesso Negado");
}
if($_POST){
	if($_POST["nyaa"])update_option("fnsb_nyaa",($_POST["nyaa"] == "2" ? "0" : "1"));
	if($_POST["nyaa_id"])update_option("fnsb_nyaa_id",$_POST["nyaa_id"]);
	if($_POST["xdcc"])update_option("fnsb_xdcc",($_POST["xdcc"] == "2" ? "0" : "1"));
?>
<div class="updated">
		<p>
			<strong><?php _e("Configurações guardadas","wp_fansub-manager");?></strong>
		</p>
	</div>
<? }if(!$_GET["do"]){?>
<div class="wrap">
		<h2><?php _e("Fansub Manager - Settings","wp_fansub-manager");?></h2>
		<form method="post">
			<div id="poststuff" class="metabox-holder has-right-sidebar">
				<div id="side-info-column" class="inner-sidebar">
					<div id="side-sortables" class="meta-box-sortables">
						<div id="linksubmitdiv" class="postbox">
							<div class="inside">
								<div class="submitbox" id="submitlink">
									<div id="minor-publishing">
										<div id="misc-publishing-actions">
											<div class="misc-pub-section misc-pub-section-last">
												Submit modifications.
											</div>
										</div>
									</div>
									<div id="major-publishing-actions">
										<div id="publishing-action">
											<input name="save" type="submit" class="button-primary" id="publish" accesskey="p" value="Submit" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="post-body">
					<div id="post-body-content">
						<div id="namediv" class="stuffbox">
							<h3>
								<label for="nyaa">Activate NyaaTorrents</label>
							</h3>
							<div class="inside">
								<select name="nyaa" id="nyaa">
									<option value="1" <?php if(get_option("fnsb_nyaa"))print("selected");?>>Yes</option>
									<option value="2" <?php if(!get_option("fnsb_nyaa"))print("selected");?>>No</option>
								</select>
							</div>
						</div>
<?php if(get_option("fnsb_nyaa")) : ?>
						<div id="namediv" class="stuffbox">
							<h3>
								<label for="nyaa_id">NyaaTorrents Account ID</label>
							</h3>
							<div class="inside">
								<input type="text" name="nyaa_id" id='nyaa_id' size="30" tabindex="1" value="<?=get_option('fnsb_nyaa_id');?>" />
								<p>Example: 1234,12345,123456... (add a comma for separate the others)</p>
							</div>
						</div>
<?php endif; ?>
						<div id="namediv" class="stuffbox">
							<h3>
								<label for="xdcc">Activate XDCC</label>
							</h3>
							<div class="inside">
								<select name="xdcc" id="xdcc">
									<option value="1" <?php if(get_option("fnsb_xdcc"))print("selected");?>>Yes</option>
									<option value="2" <?php if(!get_option("fnsb_xdcc"))print("selected");?>>No</option>
								</select>
							</div>
						</div>
						<div id="advanced-sortables" class="meta-box-sortables"></div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php }?>