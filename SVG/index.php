<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>SVG</title>

		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/3.1.1/jquery.min.js"></script>

		<style type="text/css" media="screen">
			.color{
				color:red;
				font-size:60em;
				margin-left:400px;
				margin-top:200px;
			}
		</style>
	</head>
	<body>
		<svg class="color" width="500" height="500">
			<rect x="100" y="100" width="200" height="200" style="fill:red"></rect>
			<circle cx="100" cy="100" r="50" style="fill:blue"></circle>
			<circle cx="300" cy="300" r="50" style="fill:blue"></circle>
			<circle cx="300" cy="100" r="50" style="fill:blue"></circle>
			<circle cx="100" cy="300" r="50" style="fill:blue"></circle>
		</svg>

		<script type="text/javascript">
			(function($){
				$(function(){
					var $selec = $(".color"),$selec2 = $(".color2"),degree = 0,timer;
					rotate();
					function rotate(){
						timer = setTimeout(function(){
							$selec.css({WebkitTransform:"rotate(" + degree + "deg)"});
							$selec2.css({WebkitTransform:"rotate(-" + degree + "deg)"});
							++degree;
							rotate();
						},5);
					}
				}); 
			})(jQuery);
		</script>
	</body>
</html>