--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: Projetm; Type: COMMENT; Schema: -; Owner: walidb
--

COMMENT ON DATABASE "Projetm" IS 'Projet site 
- Détails 

1) Base register
2) Base login 
3) Base article 
4) Base livre d''or 
';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: login; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE login (
    id integer NOT NULL,
    name character varying(30) NOT NULL,
    mdp character varying(30) NOT NULL,
    status character varying(50) NOT NULL,
    numero character varying(50) NOT NULL
);


ALTER TABLE login OWNER TO walidb;

--
-- Name: login_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE login_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE login_id_seq OWNER TO walidb;

--
-- Name: login_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE login_id_seq OWNED BY login.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY login ALTER COLUMN id SET DEFAULT nextval('login_id_seq'::regclass);


--
-- Data for Name: login; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY login (id, name, mdp, status, numero) FROM stdin;
2	test	1234	standard	20
\.


--
-- Name: login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('login_id_seq', 2, true);


--
-- Name: login_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY login
    ADD CONSTRAINT login_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

