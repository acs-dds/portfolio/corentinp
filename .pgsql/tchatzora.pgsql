--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: email; Type: DOMAIN; Schema: public; Owner: tchatzora
--

CREATE DOMAIN email AS text
	CONSTRAINT email_check CHECK ((VALUE ~ '^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,}$'::text));


ALTER DOMAIN email OWNER TO tchatzora;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: forum; Type: TABLE; Schema: public; Owner: tchatzora; Tablespace: 
--

CREATE TABLE forum (
    id integer NOT NULL,
    discussion character varying(100) NOT NULL,
    mot_de_passe character(32)
);


ALTER TABLE forum OWNER TO tchatzora;

--
-- Name: forum_id_seq; Type: SEQUENCE; Schema: public; Owner: tchatzora
--

CREATE SEQUENCE forum_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forum_id_seq OWNER TO tchatzora;

--
-- Name: forum_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchatzora
--

ALTER SEQUENCE forum_id_seq OWNED BY forum.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: tchatzora; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    contenu_message text NOT NULL,
    id_forum integer,
    id_utilisateur integer,
    date timestamp without time zone DEFAULT now()
);


ALTER TABLE message OWNER TO tchatzora;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: tchatzora
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO tchatzora;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchatzora
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: tchatzora; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    pseudo character varying(50) NOT NULL,
    nom character varying(20) NOT NULL,
    prenom character varying(30) NOT NULL,
    date_inscription timestamp without time zone,
    email email NOT NULL,
    password character(32) NOT NULL,
    ville character varying(100),
    page_perso text,
    avatar character varying(100)
);


ALTER TABLE utilisateur OWNER TO tchatzora;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: tchatzora
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO tchatzora;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchatzora
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchatzora
--

ALTER TABLE ONLY forum ALTER COLUMN id SET DEFAULT nextval('forum_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchatzora
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchatzora
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: forum; Type: TABLE DATA; Schema: public; Owner: tchatzora
--

COPY forum (id, discussion, mot_de_passe) FROM stdin;
1	general	\N
\.


--
-- Name: forum_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchatzora
--

SELECT pg_catalog.setval('forum_id_seq', 1, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: tchatzora
--

COPY message (id, contenu_message, id_forum, id_utilisateur, date) FROM stdin;
2	b	\N	24	2017-03-08 09:20:35.883214
3	b	\N	\N	2017-03-08 09:20:35.883214
4	b	\N	\N	2017-03-08 09:20:35.883214
5	oioiho	\N	44	2017-03-08 09:20:35.883214
6	dfdsf	\N	\N	2017-03-08 09:20:35.883214
7	dfdsf	\N	\N	2017-03-08 09:20:35.883214
8	dfdsf	\N	\N	2017-03-08 09:20:35.883214
9	dfdsf	\N	\N	2017-03-08 09:20:35.883214
10		\N	\N	2017-03-08 09:20:35.883214
11	sdf	\N	\N	2017-03-08 09:20:35.883214
12	sdf	\N	44	2017-03-08 09:20:35.883214
13	hello	\N	\N	2017-03-06 11:50:00
16	ciao	\N	\N	2017-03-08 09:28:12.905984
17	ciao	\N	\N	2017-03-08 09:29:26.269032
19	bonjour	\N	\N	2017-03-08 09:30:28.224834
20	bonjour	\N	\N	2017-03-08 09:30:48.014116
21	bonjour	\N	\N	2017-03-08 09:32:37.952993
14	hello	\N	24	2017-03-06 11:50:00
15	ciao	\N	44	2017-03-08 09:25:29.558489
18	bonjour	\N	24	2017-03-08 09:30:25.369583
23	salut	\N	\N	2017-03-08 09:37:15.057794
24	salut	\N	\N	2017-03-08 09:38:05.310834
25	salut	\N	\N	2017-03-08 09:38:35.517451
26	salut	\N	\N	2017-03-08 09:39:48.00833
22	salut	\N	44	2017-03-08 09:36:22.451131
27	salut	\N	\N	2017-03-08 09:40:14.083658
28	hug	\N	24	2017-03-08 09:40:24.008462
29	hug	\N	\N	2017-03-08 09:40:45.435155
30	hug	\N	\N	2017-03-08 09:41:08.311502
31	hug	\N	\N	2017-03-08 09:42:11.896965
32	hug	\N	\N	2017-03-08 09:42:15.587684
33	hug	\N	\N	2017-03-08 09:42:28.473053
34	hug	\N	\N	2017-03-08 09:47:37.924356
35	wha	\N	\N	2017-03-08 09:47:54.052996
36	wha	\N	\N	2017-03-08 10:03:52.238558
37	bim	\N	\N	2017-03-08 10:04:05.83447
38	bim	\N	\N	2017-03-08 10:04:29.761797
39	bim	\N	\N	2017-03-08 10:07:50.713018
40	boom	\N	44	2017-03-08 10:08:02.661969
41	kawa	\N	24	2017-03-08 10:55:23.918702
42	g	\N	\N	2017-03-08 10:59:44.526906
43	g	\N	\N	2017-03-08 11:00:46.089158
44	g	\N	\N	2017-03-08 11:04:16.255105
45	h	\N	44	2017-03-08 11:04:26.451958
46	h	\N	\N	2017-03-08 11:05:04.897102
47	h	1	24	2017-03-08 14:04:04.966284
48	hello	1	24	2017-03-08 14:04:10.007989
49	go	1	24	2017-03-08 14:04:17.64679
50		1	24	2017-03-08 14:04:21.549273
51		1	24	2017-03-08 14:04:23.686909
52		1	24	2017-03-08 14:04:47.996461
53	g	1	24	2017-03-08 14:04:53.21601
54	g	1	24	2017-03-08 14:05:00.866074
55	g	1	24	2017-03-08 14:05:13.567134
56		1	24	2017-03-08 14:05:28.374511
57		1	24	2017-03-08 14:05:32.345096
58		1	24	2017-03-08 14:12:48.931671
59	go	1	24	2017-03-08 14:13:01.844041
60	go	1	24	2017-03-08 14:13:05.212531
61	go	1	24	2017-03-08 14:13:08.925919
62	go	1	24	2017-03-08 14:14:58.014227
63	bonjour	1	24	2017-03-08 14:15:04.063034
64	bonjour	1	24	2017-03-08 14:17:58.090767
65	bonjour	1	24	2017-03-08 14:19:29.235804
66	bonjour	1	24	2017-03-08 14:21:16.822459
67	bonjour	1	24	2017-03-08 14:21:38.880477
68	bonjour	1	24	2017-03-08 14:26:47.821767
69	bonjour	1	24	2017-03-08 14:27:57.119553
70	bonjour	1	24	2017-03-08 14:36:29.078082
71	bonjour	1	24	2017-03-08 14:36:36.47884
72	go	1	24	2017-03-08 14:36:42.678232
73	go	1	24	2017-03-08 14:37:14.361488
74	go	1	24	2017-03-08 14:37:20.104007
75	go	1	24	2017-03-08 14:52:06.47545
76	go	1	24	2017-03-08 14:52:51.94974
77	he	1	24	2017-03-08 14:53:07.566597
78	he	1	24	2017-03-08 14:54:03.225038
79	he	1	24	2017-03-08 14:54:53.877965
80	he	1	24	2017-03-08 14:55:28.037979
81	he	1	24	2017-03-08 14:55:38.028553
82	go	1	24	2017-03-08 14:58:01.0944
83	bonjour	1	24	2017-03-08 15:00:48.97718
84	bonjour	1	24	2017-03-08 15:00:56.227364
85	bonjour	1	24	2017-03-08 15:02:56.66715
86	go	1	24	2017-03-08 15:05:10.788283
87	boo	1	24	2017-03-08 15:40:23.61317
88	hello	1	24	2017-03-08 16:13:18.754422
89	hello	1	24	2017-03-08 16:13:51.287702
91	hello	1	24	2017-03-08 16:13:54.266274
122	ciao	1	24	2017-03-08 16:15:18.871171
123	ciao	1	24	2017-03-08 16:16:49.145658
124	ciao	1	24	2017-03-08 16:17:26.198924
125	v	1	24	2017-03-08 16:17:29.61648
126	x	1	24	2017-03-08 16:23:01.797445
127	q	1	24	2017-03-08 16:25:31.195061
128	s	1	24	2017-03-08 16:25:36.132903
129	sdfsdfsdf	1	24	2017-03-08 16:25:43.465216
130	w	1	24	2017-03-08 16:32:43.944339
131	w	1	24	2017-03-08 16:38:23.182136
132	go	1	24	2017-03-08 16:38:27.213215
133	go	1	24	2017-03-08 16:40:38.30284
134	go	1	24	2017-03-08 16:41:10.275574
135	v	1	24	2017-03-08 16:41:14.483957
136	v	1	24	2017-03-08 16:43:51.746693
137	v	1	24	2017-03-08 16:44:56.710988
138	v	1	24	2017-03-08 16:46:26.329263
139	v	1	24	2017-03-08 16:47:08.057532
144	h	1	24	2017-03-08 16:50:37.412014
158	h	1	24	2017-03-08 16:51:05.030063
159	h	1	24	2017-03-08 16:52:02.598612
160	h	1	24	2017-03-08 16:54:26.724755
161	d	1	24	2017-03-08 16:57:46.311011
162	d	1	24	2017-03-08 16:58:59.875479
163	f	1	24	2017-03-08 17:00:09.139029
164	f	1	24	2017-03-08 17:00:40.958289
165	f	1	24	2017-03-08 17:01:15.45312
166	a	1	24	2017-03-08 17:01:29.68359
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchatzora
--

SELECT pg_catalog.setval('message_id_seq', 166, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: tchatzora
--

COPY utilisateur (id, pseudo, nom, prenom, date_inscription, email, password, ville, page_perso, avatar) FROM stdin;
24	mille	coquart	camille	\N	camillecoquart@hotmail.fr	chat                            	\N	\N	\N
28	czmillou	coquart	camille	\N	camillec@hotmail.fr	chat                            	\N	\N	\N
42	c	c	c	\N	c@hotmail.fr	chat                            	\N	\N	\N
44	Tchou	Cadouot 	Jeremy	\N	J.cadouot@icloud.com	Camille                         	\N	\N	\N
45	gg	Cdro	fee	\N	j@iclod.co	plut                            	\N	\N	\N
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchatzora
--

SELECT pg_catalog.setval('utilisateur_id_seq', 50, true);


--
-- Name: forum_pkey; Type: CONSTRAINT; Schema: public; Owner: tchatzora; Tablespace: 
--

ALTER TABLE ONLY forum
    ADD CONSTRAINT forum_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: tchatzora; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_email_key; Type: CONSTRAINT; Schema: public; Owner: tchatzora; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_email_key UNIQUE (email);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: tchatzora; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_pseudo_key; Type: CONSTRAINT; Schema: public; Owner: tchatzora; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pseudo_key UNIQUE (pseudo);


--
-- Name: message_id_forum_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tchatzora
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_forum_fkey FOREIGN KEY (id_forum) REFERENCES forum(id);


--
-- Name: message_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tchatzora
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

