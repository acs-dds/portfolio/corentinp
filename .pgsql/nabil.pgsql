--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: email; Type: DOMAIN; Schema: public; Owner: nabilb
--

CREATE DOMAIN email AS text
	CONSTRAINT email_check CHECK ((VALUE ~ '^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,}$'::text));


ALTER DOMAIN email OWNER TO nabilb;

--
-- Name: nom; Type: DOMAIN; Schema: public; Owner: nabilb
--

CREATE DOMAIN nom AS text
	CONSTRAINT nom_check CHECK ((VALUE ~ '/^[a-zA-Z]{1,60}$/'::text));


ALTER DOMAIN nom OWNER TO nabilb;

--
-- Name: pwd; Type: DOMAIN; Schema: public; Owner: nabilb
--

CREATE DOMAIN pwd AS text
	CONSTRAINT pwd_check CHECK ((VALUE ~ '/^[a-z0-9_-]{6,18}$/'::text));


ALTER DOMAIN pwd OWNER TO nabilb;

--
-- Name: username; Type: DOMAIN; Schema: public; Owner: nabilb
--

CREATE DOMAIN username AS text
	CONSTRAINT username_check CHECK ((VALUE ~ '/^[a-z0-9_-]{3,16}$/'::text));


ALTER DOMAIN username OWNER TO nabilb;

--
-- Name: www; Type: DOMAIN; Schema: public; Owner: nabilb
--

CREATE DOMAIN www AS text
	CONSTRAINT www_check CHECK ((VALUE ~ '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'::text));


ALTER DOMAIN www OWNER TO nabilb;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: message; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    date timestamp without time zone NOT NULL,
    contenu character(1) NOT NULL,
    id_utilisateur integer NOT NULL,
    id_salon integer NOT NULL
);


ALTER TABLE message OWNER TO nabilb;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO nabilb;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: salon; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE salon (
    id integer NOT NULL,
    libelle character varying(255) NOT NULL,
    mdp character varying(255)
);


ALTER TABLE salon OWNER TO nabilb;

--
-- Name: salon_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE salon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE salon_id_seq OWNER TO nabilb;

--
-- Name: salon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE salon_id_seq OWNED BY salon.id;


--
-- Name: test; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE test (
    id integer NOT NULL,
    nom character varying(30) NOT NULL,
    msg character varying(100) NOT NULL
);


ALTER TABLE test OWNER TO nabilb;

--
-- Name: test_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_id_seq OWNER TO nabilb;

--
-- Name: test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE test_id_seq OWNED BY test.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    nom character varying(60) NOT NULL,
    prenom character varying(60) NOT NULL,
    email email NOT NULL,
    page_perso character varying(60),
    compte character varying(60) NOT NULL,
    mdp character varying(60) NOT NULL,
    description character varying(1000)
);


ALTER TABLE utilisateur OWNER TO nabilb;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO nabilb;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY salon ALTER COLUMN id SET DEFAULT nextval('salon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY test ALTER COLUMN id SET DEFAULT nextval('test_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY message (id, date, contenu, id_utilisateur, id_salon) FROM stdin;
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('message_id_seq', 1, false);


--
-- Data for Name: salon; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY salon (id, libelle, mdp) FROM stdin;
\.


--
-- Name: salon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('salon_id_seq', 1, false);


--
-- Data for Name: test; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY test (id, nom, msg) FROM stdin;
\.


--
-- Name: test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('test_id_seq', 3, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY utilisateur (id, nom, prenom, email, page_perso, compte, mdp, description) FROM stdin;
3	acs	dds	dds@acs.fr	www.test.com	test	sdsqdqsd	azeazeaze
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('utilisateur_id_seq', 3, true);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: salon_pkey; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY salon
    ADD CONSTRAINT salon_pkey PRIMARY KEY (id);


--
-- Name: test_pkey; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY test
    ADD CONSTRAINT test_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: message_id_salon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_salon_fkey FOREIGN KEY (id_salon) REFERENCES salon(id);


--
-- Name: message_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

