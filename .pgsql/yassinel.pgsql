--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: apprenant; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE apprenant (
    id integer NOT NULL,
    nom character varying(255),
    prenom character varying(255),
    ddn character varying(255),
    date date
);


ALTER TABLE apprenant OWNER TO yassinel;

--
-- Name: apprenant_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE apprenant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE apprenant_id_seq OWNER TO yassinel;

--
-- Name: apprenant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE apprenant_id_seq OWNED BY apprenant.id;


--
-- Name: formateur; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE formateur (
    id integer NOT NULL,
    nom character varying(255),
    prenom character varying(255)
);


ALTER TABLE formateur OWNER TO yassinel;

--
-- Name: formateur_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE formateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE formateur_id_seq OWNER TO yassinel;

--
-- Name: formateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE formateur_id_seq OWNED BY formateur.id;


--
-- Name: lieu; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE lieu (
    id integer NOT NULL,
    ville character varying(255)
);


ALTER TABLE lieu OWNER TO yassinel;

--
-- Name: lieu_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE lieu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE lieu_id_seq OWNER TO yassinel;

--
-- Name: lieu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE lieu_id_seq OWNED BY lieu.id;


--
-- Name: promo; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE promo (
    id integer NOT NULL,
    promotion character varying(255)
);


ALTER TABLE promo OWNER TO yassinel;

--
-- Name: promo_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE promo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE promo_id_seq OWNER TO yassinel;

--
-- Name: promo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE promo_id_seq OWNED BY promo.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY apprenant ALTER COLUMN id SET DEFAULT nextval('apprenant_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY formateur ALTER COLUMN id SET DEFAULT nextval('formateur_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY lieu ALTER COLUMN id SET DEFAULT nextval('lieu_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY promo ALTER COLUMN id SET DEFAULT nextval('promo_id_seq'::regclass);


--
-- Data for Name: apprenant; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY apprenant (id, nom, prenom, ddn, date) FROM stdin;
\.


--
-- Name: apprenant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('apprenant_id_seq', 1, false);


--
-- Data for Name: formateur; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY formateur (id, nom, prenom) FROM stdin;
\.


--
-- Name: formateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('formateur_id_seq', 1, false);


--
-- Data for Name: lieu; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY lieu (id, ville) FROM stdin;
\.


--
-- Name: lieu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('lieu_id_seq', 1, false);


--
-- Data for Name: promo; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY promo (id, promotion) FROM stdin;
\.


--
-- Name: promo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('promo_id_seq', 1, false);


--
-- Name: apprenant_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY apprenant
    ADD CONSTRAINT apprenant_pkey PRIMARY KEY (id);


--
-- Name: formateur_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY formateur
    ADD CONSTRAINT formateur_pkey PRIMARY KEY (id);


--
-- Name: lieu_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY lieu
    ADD CONSTRAINT lieu_pkey PRIMARY KEY (id);


--
-- Name: promo_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY promo
    ADD CONSTRAINT promo_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

