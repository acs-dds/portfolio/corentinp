--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: achievo_ci_walidb; Type: COMMENT; Schema: -; Owner: walidb
--

COMMENT ON DATABASE achievo_ci_walidb IS 'Base de donnés objectif_ci';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: succes; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE succes (
    id integer NOT NULL,
    titre character varying(200) NOT NULL,
    intitule character varying(255) NOT NULL,
    objectif integer NOT NULL,
    progression integer NOT NULL,
    idutilisateur integer NOT NULL
);


ALTER TABLE succes OWNER TO walidb;

--
-- Name: succes_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE succes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE succes_id_seq OWNER TO walidb;

--
-- Name: succes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE succes_id_seq OWNED BY succes.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    entreprise character varying(70) NOT NULL,
    mdp character(128) NOT NULL
);


ALTER TABLE utilisateur OWNER TO walidb;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO walidb;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY succes ALTER COLUMN id SET DEFAULT nextval('succes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: succes; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY succes (id, titre, intitule, objectif, progression, idutilisateur) FROM stdin;
4	TEST1	TEST2	100	80	1
32	Stage	Avoir	100	20	40
30	Nabil	Test	100	20	36
22	TEST	TEST	100	50	31
7	TEST2	TEST2	100	50	2
\.


--
-- Name: succes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('succes_id_seq', 33, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY utilisateur (id, entreprise, mdp) FROM stdin;
1	testt	123                                                                                                                             
2	nodex	123456                                                                                                                          
7	nodex_1	123456                                                                                                                          
10	nodex_2	123456                                                                                                                          
13	test5	123456                                                                                                                          
15	test56	123456                                                                                                                          
18	testtt	1234                                                                                                                            
22	nouveau	bonjour                                                                                                                         
25	olivier	1234                                                                                                                            
26	tes	147                                                                                                                             
28	corentin	124                                                                                                                             
31	yassine 	1234                                                                                                                            
4	Yas	1235                                                                                                                            
35	yassine	1234                                                                                                                            
36	nabil	1234                                                                                                                            
40	boutet	1234                                                                                                                            
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('utilisateur_id_seq', 37, true);


--
-- Name: succes_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_pkey PRIMARY KEY (id);


--
-- Name: succes_titre_unique; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_titre_unique UNIQUE (titre, idutilisateur);


--
-- Name: utilisateur_entreprise_key; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_entreprise_key UNIQUE (entreprise);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: succes_idutilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_idutilisateur_fkey FOREIGN KEY (idutilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

