--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE client (
    id integer NOT NULL,
    prenom character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    login character varying(255) NOT NULL,
    pass character varying(255) NOT NULL,
    adresse character varying(255) NOT NULL,
    idtype integer NOT NULL
);


ALTER TABLE client OWNER TO yassinel;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO yassinel;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- Name: epaisseur; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE epaisseur (
    id integer NOT NULL,
    hauteur integer NOT NULL,
    idproduit integer NOT NULL
);


ALTER TABLE epaisseur OWNER TO yassinel;

--
-- Name: epaisseur_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE epaisseur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE epaisseur_id_seq OWNER TO yassinel;

--
-- Name: epaisseur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE epaisseur_id_seq OWNED BY epaisseur.id;


--
-- Name: essence; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE essence (
    id integer NOT NULL,
    matiere character varying(50) NOT NULL
);


ALTER TABLE essence OWNER TO nabilb;

--
-- Name: essence_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE essence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE essence_id_seq OWNER TO nabilb;

--
-- Name: essence_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE essence_id_seq OWNED BY essence.id;


--
-- Name: produit; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE produit (
    id integer NOT NULL,
    reference character varying(30) NOT NULL,
    longueur integer NOT NULL,
    largeur integer NOT NULL,
    prixgache integer NOT NULL,
    prixutile integer NOT NULL,
    essence integer DEFAULT 1 NOT NULL,
    matiere character varying(255)
);


ALTER TABLE produit OWNER TO yassinel;

--
-- Name: produit_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE produit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE produit_id_seq OWNER TO yassinel;

--
-- Name: produit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE produit_id_seq OWNED BY produit.id;


--
-- Name: type; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE type (
    id integer NOT NULL,
    type character varying NOT NULL
);


ALTER TABLE type OWNER TO yassinel;

--
-- Name: type_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE type_id_seq OWNER TO yassinel;

--
-- Name: type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE type_id_seq OWNED BY type.id;


--
-- Name: type_produit; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE type_produit (
    id integer NOT NULL,
    idtype integer NOT NULL,
    idproduit integer NOT NULL
);


ALTER TABLE type_produit OWNER TO nabilb;

--
-- Name: type_produit_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE type_produit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE type_produit_id_seq OWNER TO nabilb;

--
-- Name: type_produit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE type_produit_id_seq OWNED BY type_produit.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY epaisseur ALTER COLUMN id SET DEFAULT nextval('epaisseur_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY essence ALTER COLUMN id SET DEFAULT nextval('essence_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY produit ALTER COLUMN id SET DEFAULT nextval('produit_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY type ALTER COLUMN id SET DEFAULT nextval('type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY type_produit ALTER COLUMN id SET DEFAULT nextval('type_produit_id_seq'::regclass);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY client (id, prenom, nom, login, pass, adresse, idtype) FROM stdin;
1	Albert	Dupont	a.dupont	beethoven	11 rue des buttes 70000 Vesoul	1
4	Alexandre	Romeix	stylbois	beethoven	1 place de la Paix 59710 Merignies	1
5	Rose	Hreidmarr	anonervo	beethoven	8 rue de la décadence 87000 Limoges	2
2	Simon	Bullard	decomex	beethoven	2 boulevard Gordon Freeman 89000	3
3	Marjorie	Demesrand	supersport25	beethoven	117bis avenue Corentin Perrot 21000 Dijon	2
\.


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('client_id_seq', 1, false);


--
-- Data for Name: epaisseur; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY epaisseur (id, hauteur, idproduit) FROM stdin;
1	15	6
2	15	7
3	15	8
4	15	9
5	15	10
6	20	16
7	20	17
8	20	18
10	25	2
11	25	3
12	25	4
13	25	5
14	30	16
15	30	17
16	30	18
17	30	6
18	30	7
19	30	8
20	30	9
22	40	2
23	40	3
24	40	4
25	40	5
26	40	11
27	40	12
28	40	13
29	40	14
30	40	15
31	40	16
32	40	17
33	40	18
34	50	11
35	50	12
36	50	13
37	50	14
38	50	15
39	50	16
40	50	17
41	50	18
42	60	1
43	60	2
44	60	3
45	60	4
46	60	5
47	65	11
48	65	12
49	65	13
50	65	14
51	65	15
52	80	11
53	80	12
54	80	13
55	80	14
56	80	15
9	25	1
21	40	1
57	20	6
58	20	7
59	20	8
60	20	9
61	20	10
62	30	10
\.


--
-- Name: epaisseur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('epaisseur_id_seq', 1, false);


--
-- Data for Name: essence; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY essence (id, matiere) FROM stdin;
1	chêne
2	merisier
3	sapin
4	frêne
\.


--
-- Name: essence_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('essence_id_seq', 4, true);


--
-- Data for Name: produit; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY produit (id, reference, longueur, largeur, prixgache, prixutile, essence, matiere) FROM stdin;
6	nodexch1pc	700	1000	14	43	2	merisier
7	nodexch1pl	1000	1000	14	43	2	merisier
8	nodexch2pl	1000	1400	14	43	2	merisier
9	nodexmr1gc	2000	2000	14	43	2	merisier
10	nodexmr2gc	2000	2800	14	43	2	merisier
11	nodexsa1pf	1500	1500	2	6	3	sapin
12	nodexsa2pf	1500	2000	2	6	3	sapin
14	nodexsa1mf	3000	3000	2	6	3	sapin
13	nodexsa3pf	1500	2500	2	6	3	sapin
15	nodexsa2mf	3000	5000	2	6	3	sapin
16	nodexfr1pv	1200	1200	7	13	4	frêne
17	nodexfr1mv	1800	1800	7	13	4	frêne
18	nodexfr2mv	1800	3600	7	13	4	frêne
1	nodexch1pa	1000	1000	7	11	1	chêne
2	nodexch2pa	1000	1500	7	11	1	chêne
3	nodexch1pf	1500	2000	7	11	1	chêne
4	nodexch2pf	1500	3000	7	11	1	chêne
5	nodexch1ga	5000	5000	7	11	1	chêne
\.


--
-- Name: produit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('produit_id_seq', 3, true);


--
-- Data for Name: type; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY type (id, type) FROM stdin;
1	gros
2	standard
3	luxe
\.


--
-- Name: type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('type_id_seq', 3, true);


--
-- Data for Name: type_produit; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY type_produit (id, idtype, idproduit) FROM stdin;
1	1	1
2	2	1
3	3	1
4	1	2
5	2	2
6	3	2
7	1	3
8	2	3
9	3	3
10	1	4
11	2	4
12	1	5
13	3	6
14	3	7
15	3	8
16	3	9
17	3	10
18	1	11
19	2	11
20	1	12
21	2	12
22	1	13
23	2	13
24	1	14
25	1	15
26	1	16
27	2	16
28	1	17
29	2	17
30	1	18
\.


--
-- Name: type_produit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('type_produit_id_seq', 30, true);


--
-- Name: client_key; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_key PRIMARY KEY (id);


--
-- Name: epaisseur_key; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY epaisseur
    ADD CONSTRAINT epaisseur_key PRIMARY KEY (id);


--
-- Name: essence_key; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY essence
    ADD CONSTRAINT essence_key PRIMARY KEY (id);


--
-- Name: produit_key; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY produit
    ADD CONSTRAINT produit_key PRIMARY KEY (id);


--
-- Name: ref_con; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY produit
    ADD CONSTRAINT ref_con UNIQUE (reference);


--
-- Name: type_key; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY type
    ADD CONSTRAINT type_key PRIMARY KEY (id);


--
-- Name: type_produit_key; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY type_produit
    ADD CONSTRAINT type_produit_key PRIMARY KEY (id);


--
-- Name: client_idtype_fkey; Type: FK CONSTRAINT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_idtype_fkey FOREIGN KEY (idtype) REFERENCES type(id);


--
-- Name: epaisseur_idproduit_fkey; Type: FK CONSTRAINT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY epaisseur
    ADD CONSTRAINT epaisseur_idproduit_fkey FOREIGN KEY (idproduit) REFERENCES produit(id);


--
-- Name: produit_essence_fkey; Type: FK CONSTRAINT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY produit
    ADD CONSTRAINT produit_essence_fkey FOREIGN KEY (essence) REFERENCES essence(id);


--
-- Name: type_produit_idproduit_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY type_produit
    ADD CONSTRAINT type_produit_idproduit_fkey FOREIGN KEY (idproduit) REFERENCES produit(id);


--
-- Name: type_produit_idtype_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY type_produit
    ADD CONSTRAINT type_produit_idtype_fkey FOREIGN KEY (idtype) REFERENCES type(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: client; Type: ACL; Schema: public; Owner: yassinel
--

REVOKE ALL ON TABLE client FROM PUBLIC;
REVOKE ALL ON TABLE client FROM yassinel;
GRANT ALL ON TABLE client TO yassinel;
GRANT ALL ON TABLE client TO nabilb;


--
-- Name: client_id_seq; Type: ACL; Schema: public; Owner: yassinel
--

REVOKE ALL ON SEQUENCE client_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE client_id_seq FROM yassinel;
GRANT ALL ON SEQUENCE client_id_seq TO yassinel;
GRANT ALL ON SEQUENCE client_id_seq TO nabilb;


--
-- Name: epaisseur; Type: ACL; Schema: public; Owner: yassinel
--

REVOKE ALL ON TABLE epaisseur FROM PUBLIC;
REVOKE ALL ON TABLE epaisseur FROM yassinel;
GRANT ALL ON TABLE epaisseur TO yassinel;
GRANT ALL ON TABLE epaisseur TO nabilb;


--
-- Name: epaisseur_id_seq; Type: ACL; Schema: public; Owner: yassinel
--

REVOKE ALL ON SEQUENCE epaisseur_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE epaisseur_id_seq FROM yassinel;
GRANT ALL ON SEQUENCE epaisseur_id_seq TO yassinel;
GRANT ALL ON SEQUENCE epaisseur_id_seq TO nabilb;


--
-- Name: essence; Type: ACL; Schema: public; Owner: nabilb
--

REVOKE ALL ON TABLE essence FROM PUBLIC;
REVOKE ALL ON TABLE essence FROM nabilb;
GRANT ALL ON TABLE essence TO nabilb;
GRANT ALL ON TABLE essence TO yassinel;


--
-- Name: produit; Type: ACL; Schema: public; Owner: yassinel
--

REVOKE ALL ON TABLE produit FROM PUBLIC;
REVOKE ALL ON TABLE produit FROM yassinel;
GRANT ALL ON TABLE produit TO yassinel;
GRANT ALL ON TABLE produit TO nabilb;


--
-- Name: produit_id_seq; Type: ACL; Schema: public; Owner: yassinel
--

REVOKE ALL ON SEQUENCE produit_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE produit_id_seq FROM yassinel;
GRANT ALL ON SEQUENCE produit_id_seq TO yassinel;
GRANT ALL ON SEQUENCE produit_id_seq TO nabilb;


--
-- Name: type; Type: ACL; Schema: public; Owner: yassinel
--

REVOKE ALL ON TABLE type FROM PUBLIC;
REVOKE ALL ON TABLE type FROM yassinel;
GRANT ALL ON TABLE type TO yassinel;
GRANT ALL ON TABLE type TO nabilb;


--
-- Name: type_id_seq; Type: ACL; Schema: public; Owner: yassinel
--

REVOKE ALL ON SEQUENCE type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE type_id_seq FROM yassinel;
GRANT ALL ON SEQUENCE type_id_seq TO yassinel;
GRANT ALL ON SEQUENCE type_id_seq TO nabilb;


--
-- Name: type_produit; Type: ACL; Schema: public; Owner: nabilb
--

REVOKE ALL ON TABLE type_produit FROM PUBLIC;
REVOKE ALL ON TABLE type_produit FROM nabilb;
GRANT ALL ON TABLE type_produit TO nabilb;
GRANT ALL ON TABLE type_produit TO yassinel;


--
-- Name: type_produit_id_seq; Type: ACL; Schema: public; Owner: nabilb
--

REVOKE ALL ON SEQUENCE type_produit_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE type_produit_id_seq FROM nabilb;
GRANT ALL ON SEQUENCE type_produit_id_seq TO nabilb;
GRANT ALL ON SEQUENCE type_produit_id_seq TO yassinel;


--
-- PostgreSQL database dump complete
--

