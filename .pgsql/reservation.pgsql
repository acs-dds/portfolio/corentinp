--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: dates_ok(integer, timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: nabilb
--

CREATE FUNCTION dates_ok(integer, timestamp without time zone, timestamp without time zone) RETURNS boolean
    LANGUAGE sql
    AS $_$SELECT COUNT(*) = 0 FROM reservation
WHERE reservation.idsalle = $1
AND (
    $2 >= debut AND $2 < fin
OR  $3 > debut AND $3 <= fin
OR  $2 < debut AND $3 > fin
);$_$;


ALTER FUNCTION public.dates_ok(integer, timestamp without time zone, timestamp without time zone) OWNER TO nabilb;

--
-- Name: heures_ok(integer, timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: nabilb
--

CREATE FUNCTION heures_ok(integer, timestamp without time zone, timestamp without time zone) RETURNS boolean
    LANGUAGE sql
    AS $_$SELECT COUNT(*) = 1 FROM salle
WHERE salle.id = $1
AND $2::time >= ouverture
AND $3::time <= fermeture;$_$;


ALTER FUNCTION public.heures_ok(integer, timestamp without time zone, timestamp without time zone) OWNER TO nabilb;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: reservation; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE reservation (
    id integer NOT NULL,
    client character varying(60) NOT NULL,
    debut timestamp without time zone NOT NULL,
    fin timestamp without time zone NOT NULL,
    idsalle integer DEFAULT 1 NOT NULL,
    CONSTRAINT dates_valides_check CHECK (dates_ok(idsalle, debut, fin)),
    CONSTRAINT debut_fin_check CHECK ((debut < fin)),
    CONSTRAINT heures_valides_check CHECK (heures_ok(idsalle, debut, fin))
);


ALTER TABLE reservation OWNER TO nabilb;

--
-- Name: reservation_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE reservation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reservation_id_seq OWNER TO nabilb;

--
-- Name: reservation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE reservation_id_seq OWNED BY reservation.id;


--
-- Name: salle; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE salle (
    id integer NOT NULL,
    libelle character varying(60) NOT NULL,
    ouverture time without time zone NOT NULL,
    fermeture time without time zone NOT NULL,
    CONSTRAINT debut_fin_check CHECK ((ouverture < fermeture))
);


ALTER TABLE salle OWNER TO nabilb;

--
-- Name: salle_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE salle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE salle_id_seq OWNER TO nabilb;

--
-- Name: salle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE salle_id_seq OWNED BY salle.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY reservation ALTER COLUMN id SET DEFAULT nextval('reservation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY salle ALTER COLUMN id SET DEFAULT nextval('salle_id_seq'::regclass);


--
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY reservation (id, client, debut, fin, idsalle) FROM stdin;
1	Nabil	2017-02-22 14:00:00	2017-02-22 15:00:00	1
3	Marie	2017-02-28 10:03:00	2017-02-28 18:00:00	1
4	nabil	2017-03-15 10:00:00	2017-03-15 11:00:00	2
\.


--
-- Name: reservation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('reservation_id_seq', 4, true);


--
-- Data for Name: salle; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY salle (id, libelle, ouverture, fermeture) FROM stdin;
1	Ignis	07:00:00	21:00:00
2	Prompto	07:00:00	21:00:00
3	Gladiolus	07:00:00	21:00:00
\.


--
-- Name: salle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('salle_id_seq', 1, false);


--
-- Name: reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id);


--
-- Name: salle_pkey; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY salle
    ADD CONSTRAINT salle_pkey PRIMARY KEY (id);


--
-- Name: reservation_idsalle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY reservation
    ADD CONSTRAINT reservation_idsalle_fkey FOREIGN KEY (idsalle) REFERENCES salle(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

