--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: email; Type: DOMAIN; Schema: public; Owner: tchat_edouard_camille
--

CREATE DOMAIN email AS text
	CONSTRAINT email_check CHECK ((VALUE ~ '^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,}$'::text));


ALTER DOMAIN email OWNER TO tchat_edouard_camille;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: discussion; Type: TABLE; Schema: public; Owner: tchat_edouard_camille; Tablespace: 
--

CREATE TABLE discussion (
    id integer NOT NULL,
    nom_discussion character varying(50) NOT NULL,
    mdp_discussion character(32)
);


ALTER TABLE discussion OWNER TO tchat_edouard_camille;

--
-- Name: discussion_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_edouard_camille
--

CREATE SEQUENCE discussion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE discussion_id_seq OWNER TO tchat_edouard_camille;

--
-- Name: discussion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_edouard_camille
--

ALTER SEQUENCE discussion_id_seq OWNED BY discussion.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: tchat_edouard_camille; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    id_pseudo integer,
    date timestamp without time zone DEFAULT now() NOT NULL,
    contenu text NOT NULL,
    id_discussion integer
);


ALTER TABLE message OWNER TO tchat_edouard_camille;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_edouard_camille
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO tchat_edouard_camille;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_edouard_camille
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: tchat_edouard_camille; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    nom character varying(50) NOT NULL,
    prenom character varying(50) NOT NULL,
    pseudo text NOT NULL,
    email email NOT NULL,
    mot_de_passe character(32) NOT NULL,
    pong timestamp without time zone
);


ALTER TABLE utilisateur OWNER TO tchat_edouard_camille;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_edouard_camille
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO tchat_edouard_camille;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_edouard_camille
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_edouard_camille
--

ALTER TABLE ONLY discussion ALTER COLUMN id SET DEFAULT nextval('discussion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_edouard_camille
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_edouard_camille
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: discussion; Type: TABLE DATA; Schema: public; Owner: tchat_edouard_camille
--

COPY discussion (id, nom_discussion, mdp_discussion) FROM stdin;
1	Walking	\N
2	Dead	\N
3	Battlestar	\N
4	Galactica	\N
5	Shameless	\N
6	DrHouse	\N
7	One Punch Man	\N
8	GTO	\N
9	Yassine	\N
11	General	\N
12	zrba	\N
13	ea	\N
\.


--
-- Name: discussion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_edouard_camille
--

SELECT pg_catalog.setval('discussion_id_seq', 13, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: tchat_edouard_camille
--

COPY message (id, id_pseudo, date, contenu, id_discussion) FROM stdin;
10	13	2017-03-06 22:09:17.550069	saitama !!!!	7
11	13	2017-03-06 22:11:18.712088	yaaaaaaaaaaaaah	4
12	13	2017-03-06 22:11:31.724796	ohhhhh	4
13	13	2017-03-06 22:11:56.180085	test	11
14	12	2017-03-06 22:13:49.903383	ihhhhhhhaaaa	4
15	12	2017-03-06 23:14:32.530893	puuuuunnnch !!	7
16	13	2017-03-06 23:19:10.355375	azeaze	7
17	12	2017-03-06 23:19:36.838093	boroooosoososososo	7
18	13	2017-03-06 23:26:51.522545	ok	4
19	12	2017-03-06 23:35:53.864635	okokok	7
20	12	2017-03-07 08:42:22.197413	yo	4
21	12	2017-03-07 08:42:47.621765	QuentinP	4
22	13	2017-03-07 08:43:00.644949	Faridas	4
23	13	2017-03-07 08:43:50.732001	kljiuoiu	4
24	\N	2017-03-07 10:08:22.058621	ghf	11
25	\N	2017-03-07 10:08:31.597845	ghf	1
26	13	2017-03-07 10:43:05.165352	Mega Punch	7
27	12	2017-03-07 10:43:51.51446	Prout	8
28	12	2017-03-07 10:53:48.128296	test	7
29	12	2017-03-07 10:59:46.635397	azeaze	5
30	12	2017-03-07 11:00:40.836991	er	11
31	12	2017-03-07 11:01:23.871359	zae	11
32	12	2017-03-07 11:01:30.974159	lol	5
33	12	2017-03-07 11:03:23.386092	Rick	1
34	13	2017-03-07 11:03:42.909804	Daryl	1
35	13	2017-03-07 11:07:40.079092	winter is coming	1
36	12	2017-03-07 11:07:51.119359	hahaha	11
37	12	2017-03-07 11:07:57.563372	sdqdazea	1
38	12	2017-03-07 11:09:38.148356	T'es viré	6
39	13	2017-03-07 11:09:38.273046	tu peux pas test	6
40	16	2017-03-07 11:41:44.034748	dsdsd	11
41	16	2017-03-07 11:42:33.25331	hello boyz	1
42	12	2017-03-07 11:54:19.88104	gjgjh	9
43	12	2017-03-07 11:55:08.48841	qsdqsdaze	2
44	12	2017-03-07 11:55:56.060945	test	12
45	12	2017-03-07 11:59:26.900593	wesh MP	1
46	12	2017-03-07 15:52:00.623602	salut les gars	3
47	17	2017-03-07 15:52:11.848397	yo	3
48	12	2017-03-08 09:53:11.635652	bouh	3
49	12	2017-03-08 10:08:15.834935	yass	2
50	12	2017-03-08 11:13:16.508757	azeaeazeaze	11
51	13	2017-03-08 11:15:41.987202	okok	6
52	13	2017-03-08 11:28:09.35729	yo	6
53	13	2017-03-08 11:28:15.77928	ahahahah	9
54	19	2017-03-08 13:45:50.345123	qlkdsqaze	11
55	20	2017-03-08 13:59:30.709822	test	11
56	21	2017-03-08 15:52:45.61688	test	11
57	20	2017-03-08 15:53:51.142216	Fafa je t'aime	11
58	13	2017-03-08 16:24:04.486351	boz	3
59	13	2017-03-08 16:24:15.206573	toz	3
60	19	2017-03-10 11:43:07.471017	BRAHHHHHHHHH	7
61	12	2017-03-15 15:06:52.649199	jaime\n	11
62	19	2017-03-16 16:41:30.837393	Oualead	11
63	19	2017-03-16 16:42:53.845728	Wesh Walid	11
64	19	2017-03-16 16:46:23.829994	Dégage Edouard	11
65	19	2017-03-16 16:52:02.93872	azeaze	11
66	13	2017-03-17 08:32:58.3293	ert	11
67	13	2017-03-17 08:38:30.32008	tttttttttttttttttt	11
68	13	2017-03-17 08:40:01.140383	yoyoyoyoyoyo	11
69	13	2017-03-17 08:41:52.475442	azearegf fdg hyer ert rzer 	11
70	13	2017-03-17 08:42:38.064143	azfd rehtrty''"é	11
71	13	2017-03-17 09:21:20.677669	sdf fhg	11
72	13	2017-03-17 09:25:00.770784	ahhh	11
73	13	2017-03-17 09:28:22.151135	scrooooolllll	11
74	13	2017-03-18 19:03:07.683548	Choupito	11
75	13	2017-03-20 09:25:27.635944	lksqjdaze	3
76	13	2017-03-20 09:25:29.96009	klmio	3
77	13	2017-03-20 09:25:32.086089	uiouo	3
78	13	2017-03-20 09:25:36.321447	uiotyutrrty	3
79	13	2017-03-20 09:25:55.386809	kjhkj	11
80	23	2017-03-23 08:56:54.375215	hello world	3
81	24	2017-03-23 08:59:57.068761	brava !!!	11
82	24	2017-03-23 09:00:09.933503	il va faire nuit Nabil 	11
83	24	2017-03-23 09:00:28.912349	#pikachu #grolem\n	11
84	24	2017-03-23 09:02:00.121538	Attention ratata va évoluer\n	11
85	24	2017-03-23 09:02:10.998623	tatatatattatata\n	11
86	24	2017-03-23 09:02:28.082009	ton ratata a évoluè en Ratatack	11
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_edouard_camille
--

SELECT pg_catalog.setval('message_id_seq', 86, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: tchat_edouard_camille
--

COPY utilisateur (id, nom, prenom, pseudo, email, mot_de_passe, pong) FROM stdin;
23	Lods	Marie Pierre	mp	mariepierre@lods.lol	ea087c1799d18a217c8746c67fbf8f27	2017-03-23 09:02:11.086958
14	Bibi	Raton	Bibi7	bibi@free.fr	3d09baddc21a365b7da5ae4d0aa5cb95	\N
16	lulu	lili	Lilu	lili@free.fr	45723a2af3788c4ff17f8d1114760e62	\N
18	Gump	Booba	b2o	45@scient.ifique	03f9ed9304b7c881bb33164928c3203f	\N
21	pruliere	jean	jean	jeanpruliere@gmail.com	5d718afd9c05c2b3e243f92f548d0326	2017-03-16 15:13:40.218597
17	corentin		coco	coco@coco.coco	d2104a400c7f629a197f33bb33fe80c0	2017-03-10 09:12:07.623767
24	grhe	Tarataa	ratata	f@gmail.com	c20ad4d76fe97759aa27a0c99bff6710	2017-03-23 13:41:34.564128
12	Boz	Yohko	yhk	yhk@toz.boz	f8dc47fbc142c46cdae0b7e1dd986144	2017-03-15 16:56:12.042212
20	w<x	dqs	kmi	camillecoquart@hotmail.fr	82a26dc06f6ad9773bf914eacb02a434	2017-04-13 10:45:46.388943
19	azeaze	azeae	yohko	yoyohko@gmail.com	073686530f3e00bea016d1f5d5e05f15	2017-03-16 16:57:52.554318
13	EA	Sports	ea	ea@sports.com	5b344ac52a0192941b46a8bf252c859c	2017-03-20 09:50:32.593124
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_edouard_camille
--

SELECT pg_catalog.setval('utilisateur_id_seq', 24, true);


--
-- Name: discussion_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_edouard_camille; Tablespace: 
--

ALTER TABLE ONLY discussion
    ADD CONSTRAINT discussion_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_edouard_camille; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_email_key; Type: CONSTRAINT; Schema: public; Owner: tchat_edouard_camille; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_email_key UNIQUE (email);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_edouard_camille; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_pseudo_key; Type: CONSTRAINT; Schema: public; Owner: tchat_edouard_camille; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pseudo_key UNIQUE (pseudo);


--
-- Name: message_id_discussion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tchat_edouard_camille
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_discussion_fkey FOREIGN KEY (id_discussion) REFERENCES discussion(id);


--
-- Name: message_id_pseudo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tchat_edouard_camille
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_pseudo_fkey FOREIGN KEY (id_pseudo) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

