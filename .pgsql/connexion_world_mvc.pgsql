--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: connexion_world_mvc; Type: COMMENT; Schema: -; Owner: walidb
--

COMMENT ON DATABASE connexion_world_mvc IS 'Chat World v0';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: messages; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE messages (
    id integer NOT NULL,
    pseudo character varying(30) NOT NULL,
    messages character varying(155) NOT NULL
);


ALTER TABLE messages OWNER TO walidb;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE messages_id_seq OWNER TO walidb;

--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE messages_id_seq OWNED BY messages.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY messages ALTER COLUMN id SET DEFAULT nextval('messages_id_seq'::regclass);


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY messages (id, pseudo, messages) FROM stdin;
1	Waloup	Hello back
5	Nico	Hello
6	olivier	salut
7	2017	2017
8	farid	boulet
9	alvin	Bienvenue à Access Code school Dijon 
10	alvin	Bienvenue à Access Code school Dijon 
11	alvin	Bienvenue à Access Code school Dijon 
12	Walid	Bonjour alvin
13	alvin	Comment ça va ? 
14	test	bonjour
15	oussama 	bonjour 
\.


--
-- Name: messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('messages_id_seq', 15, true);


--
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

