--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: message; Type: TABLE; Schema: public; Owner: b2o; Tablespace: 
--

CREATE TABLE message (
    id bigint NOT NULL,
    contenu character(255) NOT NULL,
    id_utilisateur integer NOT NULL,
    id_salon integer NOT NULL,
    date integer NOT NULL
);


ALTER TABLE message OWNER TO b2o;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: b2o
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO b2o;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: b2o
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: salon; Type: TABLE; Schema: public; Owner: b2o; Tablespace: 
--

CREATE TABLE salon (
    id bigint NOT NULL,
    libelle character varying(255) NOT NULL,
    mdp character varying(255)
);


ALTER TABLE salon OWNER TO b2o;

--
-- Name: salon_id_seq; Type: SEQUENCE; Schema: public; Owner: b2o
--

CREATE SEQUENCE salon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE salon_id_seq OWNER TO b2o;

--
-- Name: salon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: b2o
--

ALTER SEQUENCE salon_id_seq OWNED BY salon.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: b2o; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    nom character varying(255) DEFAULT ''::character varying NOT NULL,
    prenom character varying(255) DEFAULT ''::character varying NOT NULL,
    username character varying(255) DEFAULT ''::character varying NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    password character varying(255) DEFAULT ''::character varying NOT NULL,
    avatar character varying(255) DEFAULT 'default.jpg'::character varying,
    created_at date NOT NULL,
    updated_at date,
    updated_by integer,
    rights integer DEFAULT 0 NOT NULL,
    is_confirmed integer DEFAULT 0 NOT NULL,
    status character varying(255) DEFAULT 'logged-out'::character varying NOT NULL
);


ALTER TABLE users OWNER TO b2o;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: b2o
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO b2o;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: b2o
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: b2o
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: b2o
--

ALTER TABLE ONLY salon ALTER COLUMN id SET DEFAULT nextval('salon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: b2o
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: b2o
--

COPY message (id, contenu, id_utilisateur, id_salon, date) FROM stdin;
25	general                                                                                                                                                                                                                                                        	1	1	1489087185
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: b2o
--

SELECT pg_catalog.setval('message_id_seq', 25, true);


--
-- Data for Name: salon; Type: TABLE DATA; Schema: public; Owner: b2o
--

COPY salon (id, libelle, mdp) FROM stdin;
1	general	\N
\.


--
-- Name: salon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: b2o
--

SELECT pg_catalog.setval('salon_id_seq', 3, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: b2o
--

COPY users (id, nom, prenom, username, email, password, avatar, created_at, updated_at, updated_by, rights, is_confirmed, status) FROM stdin;
3			yassine	yassine@gmail.com	$2y$10$OTOhBkMncSAKufO.gaSjbuU9kZIG4t.4AvL6vK2f7UfcMjMc.OeLy	yassine.png	2017-03-07	2017-03-07	1	1	0	logged
2			Alex	aaaa.aaa@aaaa.fr	$2y$10$/SmjlXuq1eHs8D9z8LmLiuE503s3JMr3v//ZHkYO/ugPTY8j0sJe2	default.jpg	2017-03-06	2017-03-07	3	0	1	logged
4			TheLegend27	toto@toto.com	$2y$10$Zj6eWOp3mE0Qma9zxWLuieI0.s42ct6bGraS.igC.MdiCseZ6u2xC	TheLegend27.png	2017-03-09	\N	\N	0	0	logged
1			SysAdmin	sysadmin@tchat.dev	$2y$10$WZzww5UBlBTigmD51IvE4ekzAkajMp3AQ1X3vJ/gs2L6cDwgrJq8G	SysAdmin.png	2017-03-01	\N	\N	4	1	logged
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: b2o
--

SELECT pg_catalog.setval('users_id_seq', 4, true);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: b2o; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: salon_pkey; Type: CONSTRAINT; Schema: public; Owner: b2o; Tablespace: 
--

ALTER TABLE ONLY salon
    ADD CONSTRAINT salon_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: b2o; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: message_id_salon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: b2o
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_salon_fkey FOREIGN KEY (id_salon) REFERENCES salon(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

