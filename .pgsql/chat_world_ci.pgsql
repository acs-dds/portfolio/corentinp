--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: chat_world_ci; Type: COMMENT; Schema: -; Owner: walidb
--

COMMENT ON DATABASE chat_world_ci IS 'chat_world_ci';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: email; Type: DOMAIN; Schema: public; Owner: walidb
--

CREATE DOMAIN email AS text
	CONSTRAINT email_check CHECK ((VALUE ~ '^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,}$'::text));


ALTER DOMAIN email OWNER TO walidb;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: message; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    message character varying(255) NOT NULL,
    temps_message timestamp without time zone DEFAULT now(),
    utilisateur_id integer,
    salon_id integer
);


ALTER TABLE message OWNER TO walidb;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO walidb;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: register; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE register (
    id integer NOT NULL,
    pseudo character varying(30) NOT NULL,
    mdp character varying(20) NOT NULL,
    email text NOT NULL,
    message_time timestamp without time zone DEFAULT now()
);


ALTER TABLE register OWNER TO walidb;

--
-- Name: register_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE register_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE register_id_seq OWNER TO walidb;

--
-- Name: register_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE register_id_seq OWNED BY register.id;


--
-- Name: salon; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE salon (
    id integer NOT NULL,
    salon_name character varying(50) NOT NULL,
    pass character varying(30)
);


ALTER TABLE salon OWNER TO walidb;

--
-- Name: salon_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE salon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE salon_id_seq OWNER TO walidb;

--
-- Name: salon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE salon_id_seq OWNED BY salon.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY register ALTER COLUMN id SET DEFAULT nextval('register_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY salon ALTER COLUMN id SET DEFAULT nextval('salon_id_seq'::regclass);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY message (id, message, temps_message, utilisateur_id, salon_id) FROM stdin;
54	Test	2017-03-09 15:54:19.986613	19	1
55	test	2017-03-09 16:18:40.374395	19	\N
56	bonjour	2017-03-09 17:19:49.330841	22	\N
57	Coucîu	2017-03-09 21:14:33.453974	17	\N
58	test	2017-03-09 21:15:45.257673	24	\N
59	test	2017-03-09 22:23:27.000115	24	\N
60	raze	2017-03-10 11:49:44.241245	25	\N
61	test	2017-03-15 13:59:40.243615	1	\N
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('message_id_seq', 61, true);


--
-- Data for Name: register; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY register (id, pseudo, mdp, email, message_time) FROM stdin;
1	walidb	CZj86b4k	test@gmail.com	2017-03-06 10:56:31.92709
2	walidb	CZj86b4k	test@gmail.com	2017-03-06 10:56:34.903627
6	walidb	CZj86b4k		2017-03-07 12:04:25.04074
7	walidb	CZj86b4k		2017-03-07 12:11:01.639646
11	walidb	CZj86b4k	test@gmail.com	2017-03-07 13:56:57.910888
12	walidb	CZj86b4k	test@gmail.com	2017-03-07 13:58:11.320204
13	walidb	CZj86b4k	test@gmail.com	2017-03-07 14:10:24.10396
14	walidb	CZj86b4k	test@gmail.com	2017-03-07 14:10:42.746358
15	walidb	CZj86b4k	tetet@gmail.Com	2017-03-07 14:11:20.129806
16	walidb	CZj86b4k	test@gmail.com	2017-03-08 08:37:44.166391
17	test	123456	test@gmail.com	2017-03-08 11:21:50.747862
18	revo	123456	test@gmail.com	2017-03-08 11:26:11.31056
19	revo.b	123456	test@gmail.com	2017-03-08 11:50:30.182493
20	yassinl	12345	test@gmail.com	2017-03-08 16:44:43.071943
21	corentin	12345	test@gmail.com	2017-03-09 11:38:00.040699
22	bravo_farid	123456	test@gmail.com	2017-03-09 17:19:28.22476
23	test	test	test@gmail.com	2017-03-09 21:13:32.803889
24	quentin	12345	test@gmail.com	2017-03-09 21:14:37.147627
25	yhk	test	test@test.test	2017-03-10 11:49:08.140093
26	walidb	CZj86b4k	test@gmail.com	2017-03-15 13:59:28.15114
\.


--
-- Name: register_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('register_id_seq', 26, true);


--
-- Data for Name: salon; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY salon (id, salon_name, pass) FROM stdin;
1	General	\N
\.


--
-- Name: salon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('salon_id_seq', 1, true);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: register_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY register
    ADD CONSTRAINT register_pkey PRIMARY KEY (id);


--
-- Name: salon_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY salon
    ADD CONSTRAINT salon_pkey PRIMARY KEY (id);


--
-- Name: message_salon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_salon_id_fkey FOREIGN KEY (salon_id) REFERENCES salon(id);


--
-- Name: message_utilisateur_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_utilisateur_id_fkey FOREIGN KEY (utilisateur_id) REFERENCES register(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

