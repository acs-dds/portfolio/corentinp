--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: produit; Type: TABLE; Schema: public; Owner: nabilb; Tablespace: 
--

CREATE TABLE produit (
    id integer NOT NULL,
    reference character varying(6) NOT NULL,
    matiere character varying(255) NOT NULL,
    longueur integer NOT NULL,
    largeur integer NOT NULL,
    prixgache numeric(4,3) NOT NULL,
    prixutile numeric(4,3) NOT NULL
);


ALTER TABLE produit OWNER TO nabilb;

--
-- Name: produit_id_seq; Type: SEQUENCE; Schema: public; Owner: nabilb
--

CREATE SEQUENCE produit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE produit_id_seq OWNER TO nabilb;

--
-- Name: produit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nabilb
--

ALTER SEQUENCE produit_id_seq OWNED BY produit.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nabilb
--

ALTER TABLE ONLY produit ALTER COLUMN id SET DEFAULT nextval('produit_id_seq'::regclass);


--
-- Data for Name: produit; Type: TABLE DATA; Schema: public; Owner: nabilb
--

COPY produit (id, reference, matiere, longueur, largeur, prixgache, prixutile) FROM stdin;
\.


--
-- Name: produit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nabilb
--

SELECT pg_catalog.setval('produit_id_seq', 1, false);


--
-- Name: produit_key; Type: CONSTRAINT; Schema: public; Owner: nabilb; Tablespace: 
--

ALTER TABLE ONLY produit
    ADD CONSTRAINT produit_key PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

