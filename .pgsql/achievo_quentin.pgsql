--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: succes_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE succes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE succes_id_seq OWNER TO quentinp;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: succes; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE succes (
    id integer DEFAULT nextval('succes_id_seq'::regclass) NOT NULL,
    titre character varying(200) NOT NULL,
    intitule character varying(600) NOT NULL,
    objectif integer NOT NULL,
    progression integer NOT NULL,
    idutilisateur integer NOT NULL,
    CONSTRAINT succes_check CHECK ((progression <= objectif)),
    CONSTRAINT succes_objectif_check CHECK ((objectif > 0))
);


ALTER TABLE succes OWNER TO quentinp;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO quentinp;

--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer DEFAULT nextval('utilisateur_id_seq'::regclass) NOT NULL,
    entreprise character varying(70) NOT NULL,
    mdp character(128) NOT NULL
);


ALTER TABLE utilisateur OWNER TO quentinp;

--
-- Name: vuesucces; Type: VIEW; Schema: public; Owner: quentinp
--

CREATE VIEW vuesucces AS
 SELECT succes.id,
    succes.titre,
    succes.intitule,
    succes.objectif,
    succes.progression,
    succes.idutilisateur,
    (succes.progression = succes.objectif) AS fini
   FROM succes
UNION
 SELECT 0 AS id,
    'C''est qui le patron ?'::character varying AS titre,
    'Vous avez atteint tous les objectifs que vous vous êtes fixés ! N''hésitez pas à en définir de nouveaux ;-)'::character varying AS intitule,
    tous.c AS objectif,
    COALESCE(finis.c, (0)::bigint) AS progression,
    tous.i AS idutilisateur,
    COALESCE((tous.c = finis.c), false) AS fini
   FROM (( SELECT count(*) AS c,
            succes.idutilisateur AS i
           FROM succes
          GROUP BY succes.idutilisateur) tous
     LEFT JOIN ( SELECT count(*) AS c,
            succes.idutilisateur AS i
           FROM succes
          WHERE (succes.progression = succes.objectif)
          GROUP BY succes.idutilisateur) finis ON ((tous.i = finis.i)));


ALTER TABLE vuesucces OWNER TO quentinp;

--
-- Data for Name: succes; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY succes (id, titre, intitule, objectif, progression, idutilisateur) FROM stdin;
6	La voie est libre	Vous vous êtes échappés 5 nuits d'affilée de votre cage	5	2	3
7	La persévérance paiera	Vous vous êtes échappés 50 fois de votre cage	50	50	3
8	Ca y est, Minus ! Nous avons conquis le monde	Vous avez conquis le monde ;-) Bravo, maître !	1	1	3
9	Errare Minusum est	Votre collaborateur a fait capoter 50 de vos plans les plus ingénieux	50	50	3
2	Origamme	Vous avez complété votre première gamme de produits, soit un total de 20 références distinctes	20	20	1
3	MVP	Vous avez assez de modèles différents pour vous lancer, alors qu'attendez-vous ?	60	60	1
\.


--
-- Name: succes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('succes_id_seq', 28, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY utilisateur (id, entreprise, mdp) FROM stdin;
1	fope	ab54fd43bf4cbdf123d392bb17e56a1fabfbec11b6657aa61c9d58969e989b440f39bb54d42157768a4c4b7d4b83a543c74a9462a592bb2519896f15454ce949
3	cortex	758aad7d91f0a80359bad3a0fea93c7a1d2bb50ebe2f3e33a0e53fe2c3fb3f94e75fe8726422d65f29937c246e732920bbef5e7eaa481e30e886f6b8c9040398
4	quentinp	405df06bad7b0c6b94f250d23fbc052294ab45c307953515c65465371ecf3ba078d25db8ee388894c24064969478c0c169c8743637b732277bd5eef3d0350cf3
5	teub	fea0b8ad9a958095e91f05b47a47288fa3b4207c01d80ceab5d9c2c5b9003bbe7dada7fea473790908df7f14646d52820788c66a5350cb2e9f501b85604a3794
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('utilisateur_id_seq', 5, true);


--
-- Name: succes_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_pkey PRIMARY KEY (id);


--
-- Name: succes_titre_unique; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_titre_unique UNIQUE (titre, idutilisateur);


--
-- Name: utilisateur_entreprise_key; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_entreprise_key UNIQUE (entreprise);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: succes_idutilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_idutilisateur_fkey FOREIGN KEY (idutilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

