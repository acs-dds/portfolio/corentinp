--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: discussion; Type: TABLE; Schema: public; Owner: tchat_jean; Tablespace: 
--

CREATE TABLE discussion (
    id integer NOT NULL,
    libelle character varying(50) NOT NULL,
    motdepasse character(32)
);


ALTER TABLE discussion OWNER TO tchat_jean;

--
-- Name: discussion_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_jean
--

CREATE SEQUENCE discussion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE discussion_id_seq OWNER TO tchat_jean;

--
-- Name: discussion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_jean
--

ALTER SEQUENCE discussion_id_seq OWNED BY discussion.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: tchat_jean; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    contenu text NOT NULL,
    idutilisateur integer NOT NULL,
    iddiscussion integer NOT NULL
);


ALTER TABLE message OWNER TO tchat_jean;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_jean
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO tchat_jean;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_jean
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: tchat_jean; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    nom character varying(100) NOT NULL,
    prenom character varying(100) NOT NULL,
    pseudo character varying(30) NOT NULL,
    motdepasse character(32) NOT NULL,
    ping timestamp without time zone
);


ALTER TABLE utilisateur OWNER TO tchat_jean;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_jean
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO tchat_jean;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_jean
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: vuemessage; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vuemessage AS
 SELECT message.id,
    message.contenu,
    utilisateur.pseudo AS auteur,
    message.stamp AS date,
    utilisateur.id AS idauteur,
    message.iddiscussion
   FROM (message
     JOIN utilisateur ON ((utilisateur.id = message.idutilisateur)));


ALTER TABLE vuemessage OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_jean
--

ALTER TABLE ONLY discussion ALTER COLUMN id SET DEFAULT nextval('discussion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_jean
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_jean
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: discussion; Type: TABLE DATA; Schema: public; Owner: tchat_jean
--

COPY discussion (id, libelle, motdepasse) FROM stdin;
1	general	\N
2	prive	48b5a28a1365ed06fbc020b24c9529ff
\.


--
-- Name: discussion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_jean
--

SELECT pg_catalog.setval('discussion_id_seq', 2, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: tchat_jean
--

COPY message (id, stamp, contenu, idutilisateur, iddiscussion) FROM stdin;
1	2017-03-07 14:56:47.076121	Yo !	1	1
2	2017-03-07 14:57:05.030661	Wesh gros ! Je suis tout seul ou bien ?	1	1
3	2017-03-07 14:57:36.838844	(commence une partie de roulette russe)	1	1
19	2017-03-07 16:44:41.699472	farid	1	1
20	2017-03-07 16:51:23.756379	tu peux pas test	1	1
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_jean
--

SELECT pg_catalog.setval('message_id_seq', 20, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: tchat_jean
--

COPY utilisateur (id, nom, prenom, pseudo, motdepasse, ping) FROM stdin;
1	Pruliere	Jean	verdandi	098f6bcd4621d373cade4e832627b4f6	2017-03-07 16:58:31.764471
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_jean
--

SELECT pg_catalog.setval('utilisateur_id_seq', 1, true);


--
-- Name: discussion_libelle_key; Type: CONSTRAINT; Schema: public; Owner: tchat_jean; Tablespace: 
--

ALTER TABLE ONLY discussion
    ADD CONSTRAINT discussion_libelle_key UNIQUE (libelle);


--
-- Name: discussion_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_jean; Tablespace: 
--

ALTER TABLE ONLY discussion
    ADD CONSTRAINT discussion_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_jean; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_jean; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_pseudo_key; Type: CONSTRAINT; Schema: public; Owner: tchat_jean; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pseudo_key UNIQUE (pseudo);


--
-- Name: message_iddiscussion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tchat_jean
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_iddiscussion_fkey FOREIGN KEY (iddiscussion) REFERENCES discussion(id);


--
-- Name: message_idutilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tchat_jean
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_idutilisateur_fkey FOREIGN KEY (idutilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: vuemessage; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE vuemessage FROM PUBLIC;
REVOKE ALL ON TABLE vuemessage FROM postgres;
GRANT ALL ON TABLE vuemessage TO postgres;
GRANT SELECT ON TABLE vuemessage TO tchat_jean;


--
-- PostgreSQL database dump complete
--

