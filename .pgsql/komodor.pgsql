--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acteurs; Type: TABLE; Schema: public; Owner: faridl; Tablespace: 
--

CREATE TABLE acteurs (
    nom character varying(40) NOT NULL,
    prenom character varying(40) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE acteurs OWNER TO faridl;

--
-- Name: films; Type: TABLE; Schema: public; Owner: faridl; Tablespace: 
--

CREATE TABLE films (
    nom character varying(40) NOT NULL,
    realisateur character varying(40) NOT NULL,
    annee integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE films OWNER TO faridl;

--
-- Data for Name: acteurs; Type: TABLE DATA; Schema: public; Owner: faridl
--

COPY acteurs (nom, prenom, id) FROM stdin;
LKHALDOUNI	Farid	1
DI CAPRIO	Leonardo	2
\.


--
-- Data for Name: films; Type: TABLE DATA; Schema: public; Owner: faridl
--

COPY films (nom, realisateur, annee, id) FROM stdin;
Inception	Chistopher NOLAN	2010	1
\.


--
-- Name: acteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY acteurs
    ADD CONSTRAINT acteurs_pkey PRIMARY KEY (id);


--
-- Name: films_pkey; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY films
    ADD CONSTRAINT films_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

