--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: tchat_cp_yl; Type: COMMENT; Schema: -; Owner: tchat_cp_yl
--

COMMENT ON DATABASE tchat_cp_yl IS 'Coucou \(>.<)/

Pas touche à notre DB stp ;D';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: email; Type: DOMAIN; Schema: public; Owner: tchat_cp_yl
--

CREATE DOMAIN email AS text
	CONSTRAINT email_check CHECK ((VALUE ~ '^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,}$'::text));


ALTER DOMAIN email OWNER TO tchat_cp_yl;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: forum; Type: TABLE; Schema: public; Owner: tchat_cp_yl; Tablespace: 
--

CREATE TABLE forum (
    id integer NOT NULL,
    libelle character varying(50) NOT NULL,
    mdp character(12)
);


ALTER TABLE forum OWNER TO tchat_cp_yl;

--
-- Name: forum_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_cp_yl
--

CREATE SEQUENCE forum_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forum_id_seq OWNER TO tchat_cp_yl;

--
-- Name: forum_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_cp_yl
--

ALTER SEQUENCE forum_id_seq OWNED BY forum.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: tchat_cp_yl; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    contenu text NOT NULL,
    idusers integer NOT NULL,
    idforum integer NOT NULL
);


ALTER TABLE message OWNER TO tchat_cp_yl;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_cp_yl
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO tchat_cp_yl;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_cp_yl
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: tchat_cp_yl; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    nom character varying(80) NOT NULL,
    prenom character varying(80) NOT NULL,
    pseudo character varying(40) NOT NULL,
    mdp character(10) NOT NULL,
    ping timestamp without time zone
);


ALTER TABLE users OWNER TO tchat_cp_yl;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: tchat_cp_yl
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO tchat_cp_yl;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tchat_cp_yl
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_cp_yl
--

ALTER TABLE ONLY forum ALTER COLUMN id SET DEFAULT nextval('forum_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_cp_yl
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tchat_cp_yl
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: forum; Type: TABLE DATA; Schema: public; Owner: tchat_cp_yl
--

COPY forum (id, libelle, mdp) FROM stdin;
1	hello acs dijon	online      
2	coucou ! comment vas-tu ?	access      
\.


--
-- Name: forum_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_cp_yl
--

SELECT pg_catalog.setval('forum_id_seq', 2, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: tchat_cp_yl
--

COPY message (id, stamp, contenu, idusers, idforum) FROM stdin;
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_cp_yl
--

SELECT pg_catalog.setval('message_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: tchat_cp_yl
--

COPY users (id, nom, prenom, pseudo, mdp, ping) FROM stdin;
1	smith	laura	belle	slbmae    	\N
2	paolo	leonard	rebel	coucou    	2017-03-07 00:00:00
3	lili	lala	coucou	hello     	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tchat_cp_yl
--

SELECT pg_catalog.setval('users_id_seq', 3, true);


--
-- Name: forum_libelle_key; Type: CONSTRAINT; Schema: public; Owner: tchat_cp_yl; Tablespace: 
--

ALTER TABLE ONLY forum
    ADD CONSTRAINT forum_libelle_key UNIQUE (libelle);


--
-- Name: forum_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_cp_yl; Tablespace: 
--

ALTER TABLE ONLY forum
    ADD CONSTRAINT forum_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_cp_yl; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: tchat_cp_yl; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_pseudo_key; Type: CONSTRAINT; Schema: public; Owner: tchat_cp_yl; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pseudo_key UNIQUE (pseudo);


--
-- Name: message_idforum_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tchat_cp_yl
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_idforum_fkey FOREIGN KEY (idforum) REFERENCES forum(id);


--
-- Name: message_idusers_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tchat_cp_yl
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_idusers_fkey FOREIGN KEY (idusers) REFERENCES users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

