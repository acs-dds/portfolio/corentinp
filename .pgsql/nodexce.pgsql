--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client; Type: TABLE; Schema: public; Owner: edouardb; Tablespace: 
--

CREATE TABLE client (
    id integer NOT NULL,
    nom text NOT NULL,
    prenom text NOT NULL,
    id_type integer
);


ALTER TABLE client OWNER TO edouardb;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: edouardb
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO edouardb;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edouardb
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- Name: declinaison; Type: TABLE; Schema: public; Owner: edouardb; Tablespace: 
--

CREATE TABLE declinaison (
    id integer NOT NULL,
    epaisseur integer NOT NULL,
    id_planche integer NOT NULL
);


ALTER TABLE declinaison OWNER TO edouardb;

--
-- Name: declinaison_id_seq; Type: SEQUENCE; Schema: public; Owner: edouardb
--

CREATE SEQUENCE declinaison_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE declinaison_id_seq OWNER TO edouardb;

--
-- Name: declinaison_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edouardb
--

ALTER SEQUENCE declinaison_id_seq OWNED BY declinaison.id;


--
-- Name: planche; Type: TABLE; Schema: public; Owner: edouardb; Tablespace: 
--

CREATE TABLE planche (
    id integer NOT NULL,
    refer text NOT NULL,
    prix integer NOT NULL,
    essence text NOT NULL,
    longueur integer,
    largeur integer,
    CONSTRAINT prix CHECK ((0 < prix))
);


ALTER TABLE planche OWNER TO edouardb;

--
-- Name: planche_id_seq; Type: SEQUENCE; Schema: public; Owner: edouardb
--

CREATE SEQUENCE planche_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE planche_id_seq OWNER TO edouardb;

--
-- Name: planche_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edouardb
--

ALTER SEQUENCE planche_id_seq OWNED BY planche.id;


--
-- Name: planche_type; Type: TABLE; Schema: public; Owner: edouardb; Tablespace: 
--

CREATE TABLE planche_type (
    id integer NOT NULL,
    id_type integer NOT NULL,
    id_planche integer NOT NULL
);


ALTER TABLE planche_type OWNER TO edouardb;

--
-- Name: planche_type_id_seq; Type: SEQUENCE; Schema: public; Owner: edouardb
--

CREATE SEQUENCE planche_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE planche_type_id_seq OWNER TO edouardb;

--
-- Name: planche_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edouardb
--

ALTER SEQUENCE planche_type_id_seq OWNED BY planche_type.id;


--
-- Name: type; Type: TABLE; Schema: public; Owner: edouardb; Tablespace: 
--

CREATE TABLE type (
    id integer NOT NULL,
    nom text NOT NULL
);


ALTER TABLE type OWNER TO edouardb;

--
-- Name: type_id_seq; Type: SEQUENCE; Schema: public; Owner: edouardb
--

CREATE SEQUENCE type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE type_id_seq OWNER TO edouardb;

--
-- Name: type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edouardb
--

ALTER SEQUENCE type_id_seq OWNED BY type.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: edouardb
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: edouardb
--

ALTER TABLE ONLY declinaison ALTER COLUMN id SET DEFAULT nextval('declinaison_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: edouardb
--

ALTER TABLE ONLY planche ALTER COLUMN id SET DEFAULT nextval('planche_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: edouardb
--

ALTER TABLE ONLY planche_type ALTER COLUMN id SET DEFAULT nextval('planche_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: edouardb
--

ALTER TABLE ONLY type ALTER COLUMN id SET DEFAULT nextval('type_id_seq'::regclass);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: edouardb
--

COPY client (id, nom, prenom, id_type) FROM stdin;
3	Pruliere	Jean	1
4	Chu	Pika	5
\.


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edouardb
--

SELECT pg_catalog.setval('client_id_seq', 4, true);


--
-- Data for Name: declinaison; Type: TABLE DATA; Schema: public; Owner: edouardb
--

COPY declinaison (id, epaisseur, id_planche) FROM stdin;
12	15	9
13	20	10
14	25	11
15	30	12
16	30	13
\.


--
-- Name: declinaison_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edouardb
--

SELECT pg_catalog.setval('declinaison_id_seq', 16, true);


--
-- Data for Name: planche; Type: TABLE DATA; Schema: public; Owner: edouardb
--

COPY planche (id, refer, prix, essence, longueur, largeur) FROM stdin;
9	nodex21	700	merisier	2000	1000
10	nodex33	600	merisier	1000	1500
11	nodex44	600	sapin	1000	1000
12	nodex55	600	ébène	1000	1000
13	nodex66	600	merisier	1000	2000
\.


--
-- Name: planche_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edouardb
--

SELECT pg_catalog.setval('planche_id_seq', 13, true);


--
-- Data for Name: planche_type; Type: TABLE DATA; Schema: public; Owner: edouardb
--

COPY planche_type (id, id_type, id_planche) FROM stdin;
3	1	9
5	1	10
6	2	9
7	2	11
9	2	12
10	5	13
8	5	9
\.


--
-- Name: planche_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edouardb
--

SELECT pg_catalog.setval('planche_type_id_seq', 10, true);


--
-- Data for Name: type; Type: TABLE DATA; Schema: public; Owner: edouardb
--

COPY type (id, nom) FROM stdin;
1	gros
2	standard
5	pokemon
\.


--
-- Name: type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edouardb
--

SELECT pg_catalog.setval('type_id_seq', 5, true);


--
-- Name: client_pkey; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- Name: declinaison_pkey; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY declinaison
    ADD CONSTRAINT declinaison_pkey PRIMARY KEY (id);


--
-- Name: declinaison_unique; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY declinaison
    ADD CONSTRAINT declinaison_unique UNIQUE (epaisseur, id_planche);


--
-- Name: groupe_unique; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY planche
    ADD CONSTRAINT groupe_unique UNIQUE (longueur, largeur, essence);


--
-- Name: planche_pkey; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY planche
    ADD CONSTRAINT planche_pkey PRIMARY KEY (id);


--
-- Name: planche_type_pkey; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY planche_type
    ADD CONSTRAINT planche_type_pkey PRIMARY KEY (id);


--
-- Name: planche_unique; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY planche
    ADD CONSTRAINT planche_unique UNIQUE (refer);


--
-- Name: type_pkey; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY type
    ADD CONSTRAINT type_pkey PRIMARY KEY (id);


--
-- Name: client_id_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: edouardb
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_id_type_fkey FOREIGN KEY (id_type) REFERENCES type(id);


--
-- Name: declinaison_id_planche_fkey; Type: FK CONSTRAINT; Schema: public; Owner: edouardb
--

ALTER TABLE ONLY declinaison
    ADD CONSTRAINT declinaison_id_planche_fkey FOREIGN KEY (id_planche) REFERENCES planche(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: client; Type: ACL; Schema: public; Owner: edouardb
--

REVOKE ALL ON TABLE client FROM PUBLIC;
REVOKE ALL ON TABLE client FROM edouardb;
GRANT ALL ON TABLE client TO edouardb;
GRANT ALL ON TABLE client TO camillec;


--
-- Name: client_id_seq; Type: ACL; Schema: public; Owner: edouardb
--

REVOKE ALL ON SEQUENCE client_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE client_id_seq FROM edouardb;
GRANT ALL ON SEQUENCE client_id_seq TO edouardb;
GRANT ALL ON SEQUENCE client_id_seq TO camillec;


--
-- Name: declinaison; Type: ACL; Schema: public; Owner: edouardb
--

REVOKE ALL ON TABLE declinaison FROM PUBLIC;
REVOKE ALL ON TABLE declinaison FROM edouardb;
GRANT ALL ON TABLE declinaison TO edouardb;
GRANT ALL ON TABLE declinaison TO camillec;


--
-- Name: declinaison_id_seq; Type: ACL; Schema: public; Owner: edouardb
--

REVOKE ALL ON SEQUENCE declinaison_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE declinaison_id_seq FROM edouardb;
GRANT ALL ON SEQUENCE declinaison_id_seq TO edouardb;
GRANT ALL ON SEQUENCE declinaison_id_seq TO camillec;


--
-- Name: planche; Type: ACL; Schema: public; Owner: edouardb
--

REVOKE ALL ON TABLE planche FROM PUBLIC;
REVOKE ALL ON TABLE planche FROM edouardb;
GRANT ALL ON TABLE planche TO edouardb;
GRANT ALL ON TABLE planche TO camillec;


--
-- Name: planche_id_seq; Type: ACL; Schema: public; Owner: edouardb
--

REVOKE ALL ON SEQUENCE planche_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE planche_id_seq FROM edouardb;
GRANT ALL ON SEQUENCE planche_id_seq TO edouardb;
GRANT ALL ON SEQUENCE planche_id_seq TO camillec;


--
-- Name: type; Type: ACL; Schema: public; Owner: edouardb
--

REVOKE ALL ON TABLE type FROM PUBLIC;
REVOKE ALL ON TABLE type FROM edouardb;
GRANT ALL ON TABLE type TO edouardb;
GRANT ALL ON TABLE type TO camillec;


--
-- Name: type_id_seq; Type: ACL; Schema: public; Owner: edouardb
--

REVOKE ALL ON SEQUENCE type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE type_id_seq FROM edouardb;
GRANT ALL ON SEQUENCE type_id_seq TO edouardb;
GRANT ALL ON SEQUENCE type_id_seq TO camillec;


--
-- PostgreSQL database dump complete
--

