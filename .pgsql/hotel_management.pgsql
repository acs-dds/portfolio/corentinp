--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: hotel; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE hotel (
    id integer NOT NULL,
    buisness character varying NOT NULL,
    city character varying NOT NULL,
    category character varying NOT NULL
);


ALTER TABLE hotel OWNER TO yassinel;

--
-- Name: hotel_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE hotel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotel_id_seq OWNER TO yassinel;

--
-- Name: hotel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE hotel_id_seq OWNED BY hotel.id;


--
-- Name: utilisateurs; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE utilisateurs (
    id integer NOT NULL,
    client character varying(255),
    email character varying(255),
    pass character varying(255)
);


ALTER TABLE utilisateurs OWNER TO yassinel;

--
-- Name: utilisateurs_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE utilisateurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateurs_id_seq OWNER TO yassinel;

--
-- Name: utilisateurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE utilisateurs_id_seq OWNED BY utilisateurs.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY hotel ALTER COLUMN id SET DEFAULT nextval('hotel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY utilisateurs ALTER COLUMN id SET DEFAULT nextval('utilisateurs_id_seq'::regclass);


--
-- Data for Name: hotel; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY hotel (id, buisness, city, category) FROM stdin;
3	Chjhcgh	Vvvbn	Cvcjj
\.


--
-- Name: hotel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('hotel_id_seq', 3, true);


--
-- Data for Name: utilisateurs; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY utilisateurs (id, client, email, pass) FROM stdin;
\.


--
-- Name: utilisateurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('utilisateurs_id_seq', 1, false);


--
-- Name: hotel_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY hotel
    ADD CONSTRAINT hotel_pkey PRIMARY KEY (id);


--
-- Name: utilisateurs_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY utilisateurs
    ADD CONSTRAINT utilisateurs_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

