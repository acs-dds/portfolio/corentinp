--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE client (
    id integer NOT NULL,
    refclient character varying(100) NOT NULL,
    prenom character varying(50) NOT NULL,
    nom character varying(60) NOT NULL,
    rue character varying(200) NOT NULL,
    cp character varying(100),
    ville character varying(80) NOT NULL
);


ALTER TABLE client OWNER TO quentinp;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO quentinp;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quentinp
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- Name: epaisseur; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE epaisseur (
    id integer NOT NULL,
    epaisseur integer NOT NULL,
    refplanche character varying(100)
);


ALTER TABLE epaisseur OWNER TO quentinp;

--
-- Name: epaisseur_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE epaisseur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE epaisseur_id_seq OWNER TO quentinp;

--
-- Name: epaisseur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quentinp
--

ALTER SEQUENCE epaisseur_id_seq OWNED BY epaisseur.id;


--
-- Name: planche; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE planche (
    id integer NOT NULL,
    refplanche character varying(100) NOT NULL,
    matiere character varying(100) NOT NULL,
    longueur integer NOT NULL,
    largeur integer NOT NULL,
    prix_decoupe numeric NOT NULL,
    prix_gache numeric
);


ALTER TABLE planche OWNER TO quentinp;

--
-- Name: planche_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE planche_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE planche_id_seq OWNER TO quentinp;

--
-- Name: planche_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quentinp
--

ALTER SEQUENCE planche_id_seq OWNED BY planche.id;


--
-- Name: typs; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE typs (
    id integer NOT NULL,
    reftyps character varying(100) NOT NULL
);


ALTER TABLE typs OWNER TO quentinp;

--
-- Name: typs_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE typs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE typs_id_seq OWNER TO quentinp;

--
-- Name: typs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quentinp
--

ALTER SEQUENCE typs_id_seq OWNED BY typs.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY epaisseur ALTER COLUMN id SET DEFAULT nextval('epaisseur_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY planche ALTER COLUMN id SET DEFAULT nextval('planche_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY typs ALTER COLUMN id SET DEFAULT nextval('typs_id_seq'::regclass);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY client (id, refclient, prenom, nom, rue, cp, ville) FROM stdin;
1	1	Albert	Dupont	11 rue des buttes	70000	Vesoul
2	2	Simon	Bullard	2 Boulevard Gordon Freeman	89000	Auxerre
3	3	Marjorie	Demesrand	117 bis avenue Corentin Perrot	21000	Dijon
5	4	Rose	Hreidmarr	8 rue des Décadence	87000	Limoges
7	5	Alexandre	Romeix	1 place de la Paix	59710	Mairignies
\.


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('client_id_seq', 7, true);


--
-- Data for Name: epaisseur; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY epaisseur (id, epaisseur, refplanche) FROM stdin;
1	25	\N
2	40	\N
3	60	\N
4	15	\N
5	20	\N
6	50	\N
7	65	\N
\.


--
-- Name: epaisseur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('epaisseur_id_seq', 5, true);


--
-- Data for Name: planche; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY planche (id, refplanche, matiere, longueur, largeur, prix_decoupe, prix_gache) FROM stdin;
1	nodexch1pa	chene	1000	1000	0.011	0.007
2	nodexch2pa	chene	1000	1500	0.011	0.007
3	nodexch1pf	chene	1500	2000	0.011	0.007
4	nodexch2pf	chene	1500	3000	0.011	0.007
5	nodexch1ga	chene	5000	5000	0.011	0.007
8	nodexmr2pl	merisier	1000	1400	0.043	0.014
7	nodexmr1pl	merisier	1000	1000	0.043	0.014
6	nodexmr1pc	merisier	700	1000	0.043	0.014
9	nodexmr1gc	merisier	2000	2000	0.043	0.014
10	nodexmr2gc	merisier	2000	2800	0.043	0.014
\.


--
-- Name: planche_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('planche_id_seq', 2, true);


--
-- Data for Name: typs; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY typs (id, reftyps) FROM stdin;
1	gros
2	standard
3	luxe
\.


--
-- Name: typs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('typs_id_seq', 1, false);


--
-- Name: client_refclient_key; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_refclient_key UNIQUE (refclient);


--
-- Name: clients_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: epaisseur_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY epaisseur
    ADD CONSTRAINT epaisseur_pkey PRIMARY KEY (id);


--
-- Name: planche_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY planche
    ADD CONSTRAINT planche_pkey PRIMARY KEY (id);


--
-- Name: typs_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY typs
    ADD CONSTRAINT typs_pkey PRIMARY KEY (id);


--
-- Name: epaisseur_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY epaisseur
    ADD CONSTRAINT epaisseur_id_fkey FOREIGN KEY (id) REFERENCES planche(id);


--
-- Name: typs_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY typs
    ADD CONSTRAINT typs_id_fkey FOREIGN KEY (id) REFERENCES client(id);


--
-- Name: typs_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY typs
    ADD CONSTRAINT typs_id_fkey1 FOREIGN KEY (id) REFERENCES client(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

