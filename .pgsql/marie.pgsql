--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: climat; Type: TABLE; Schema: public; Owner: mariel; Tablespace: 
--

CREATE TABLE climat (
    ville character varying(80),
    temp_mini integer,
    temp_max integer,
    prcp real,
    date date,
    id integer NOT NULL
);


ALTER TABLE climat OWNER TO mariel;

--
-- Name: climat_id_seq; Type: SEQUENCE; Schema: public; Owner: mariel
--

CREATE SEQUENCE climat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE climat_id_seq OWNER TO mariel;

--
-- Name: climat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mariel
--

ALTER SEQUENCE climat_id_seq OWNED BY climat.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mariel
--

ALTER TABLE ONLY climat ALTER COLUMN id SET DEFAULT nextval('climat_id_seq'::regclass);


--
-- Data for Name: climat; Type: TABLE DATA; Schema: public; Owner: mariel
--

COPY climat (ville, temp_mini, temp_max, prcp, date, id) FROM stdin;
dijon	-200	-50	15	2017-02-01	1
\.


--
-- Name: climat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mariel
--

SELECT pg_catalog.setval('climat_id_seq', 1, true);


--
-- Name: climat_pkey; Type: CONSTRAINT; Schema: public; Owner: mariel; Tablespace: 
--

ALTER TABLE ONLY climat
    ADD CONSTRAINT climat_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

