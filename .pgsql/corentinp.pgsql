--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: corentinp; Type: COMMENT; Schema: -; Owner: corentinp
--

COMMENT ON DATABASE corentinp IS 'Coucou \(>.<)/

Pas touche à ma DB stp ;D';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ci_sessions; Type: TABLE; Schema: public; Owner: corentinp; Tablespace: 
--

CREATE TABLE ci_sessions (
    id character varying(128) NOT NULL,
    ip_address character varying(45) NOT NULL,
    "timestamp" bigint DEFAULT 0 NOT NULL,
    data text DEFAULT ''::text NOT NULL
);


ALTER TABLE ci_sessions OWNER TO corentinp;

--
-- Name: forums; Type: TABLE; Schema: public; Owner: corentinp; Tablespace: 
--

CREATE TABLE forums (
    id integer NOT NULL,
    title character varying(255) DEFAULT ''::character varying NOT NULL,
    slug character varying(255) DEFAULT ''::character varying NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    created_at date NOT NULL,
    updated_at date
);


ALTER TABLE forums OWNER TO corentinp;

--
-- Name: forums_id_seq; Type: SEQUENCE; Schema: public; Owner: corentinp
--

CREATE SEQUENCE forums_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forums_id_seq OWNER TO corentinp;

--
-- Name: forums_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: corentinp
--

ALTER SEQUENCE forums_id_seq OWNED BY forums.id;


--
-- Name: options; Type: TABLE; Schema: public; Owner: corentinp; Tablespace: 
--

CREATE TABLE options (
    id integer NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    value character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE options OWNER TO corentinp;

--
-- Name: options_id_seq; Type: SEQUENCE; Schema: public; Owner: corentinp
--

CREATE SEQUENCE options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE options_id_seq OWNER TO corentinp;

--
-- Name: options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: corentinp
--

ALTER SEQUENCE options_id_seq OWNED BY options.id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: corentinp; Tablespace: 
--

CREATE TABLE posts (
    id integer NOT NULL,
    content text NOT NULL,
    user_id integer NOT NULL,
    topic_id integer NOT NULL,
    created_at date NOT NULL,
    updated_at date
);


ALTER TABLE posts OWNER TO corentinp;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: corentinp
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE posts_id_seq OWNER TO corentinp;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: corentinp
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- Name: topics; Type: TABLE; Schema: public; Owner: corentinp; Tablespace: 
--

CREATE TABLE topics (
    id integer NOT NULL,
    title character varying(255) DEFAULT ''::character varying NOT NULL,
    slug character varying(255) DEFAULT ''::character varying NOT NULL,
    created_at date NOT NULL,
    updated_at date,
    user_id integer NOT NULL,
    forum_id integer NOT NULL,
    is_sticky integer DEFAULT 0 NOT NULL
);


ALTER TABLE topics OWNER TO corentinp;

--
-- Name: topics_id_seq; Type: SEQUENCE; Schema: public; Owner: corentinp
--

CREATE SEQUENCE topics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE topics_id_seq OWNER TO corentinp;

--
-- Name: topics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: corentinp
--

ALTER SEQUENCE topics_id_seq OWNED BY topics.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: corentinp; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(255) DEFAULT ''::character varying NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    password character varying(255) DEFAULT ''::character varying NOT NULL,
    avatar character varying(255) DEFAULT 'default.jpg'::character varying,
    created_at date NOT NULL,
    updated_at date,
    updated_by integer,
    is_admin integer DEFAULT 0 NOT NULL,
    is_moderator integer DEFAULT 0 NOT NULL,
    is_confirmed integer DEFAULT 0 NOT NULL,
    is_deleted integer DEFAULT 0 NOT NULL
);


ALTER TABLE users OWNER TO corentinp;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: corentinp
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO corentinp;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: corentinp
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: corentinp
--

ALTER TABLE ONLY forums ALTER COLUMN id SET DEFAULT nextval('forums_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: corentinp
--

ALTER TABLE ONLY options ALTER COLUMN id SET DEFAULT nextval('options_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: corentinp
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: corentinp
--

ALTER TABLE ONLY topics ALTER COLUMN id SET DEFAULT nextval('topics_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: corentinp
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: ci_sessions; Type: TABLE DATA; Schema: public; Owner: corentinp
--

COPY ci_sessions (id, ip_address, "timestamp", data) FROM stdin;
\.


--
-- Data for Name: forums; Type: TABLE DATA; Schema: public; Owner: corentinp
--

COPY forums (id, title, slug, description, created_at, updated_at) FROM stdin;
1	ACS::DDS	acs-dds	Le Channel de L'ACS !	2017-02-09	\N
\.


--
-- Name: forums_id_seq; Type: SEQUENCE SET; Schema: public; Owner: corentinp
--

SELECT pg_catalog.setval('forums_id_seq', 1, true);


--
-- Data for Name: options; Type: TABLE DATA; Schema: public; Owner: corentinp
--

COPY options (id, name, value) FROM stdin;
\.


--
-- Name: options_id_seq; Type: SEQUENCE SET; Schema: public; Owner: corentinp
--

SELECT pg_catalog.setval('options_id_seq', 1, false);


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: corentinp
--

COPY posts (id, content, user_id, topic_id, created_at, updated_at) FROM stdin;
1	Hello Corentin ! Tu fais quoi  ?\r\n	5	1	2017-02-09	\N
2	Coucou Zohra ;D	4	1	2017-02-09	\N
3	;D\r\nÇa t'interesse d'avoir la recette des sablés ?	5	1	2017-02-09	\N
4	Bien sûr ;D	4	1	2017-02-09	\N
\.


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: corentinp
--

SELECT pg_catalog.setval('posts_id_seq', 4, false);


--
-- Data for Name: topics; Type: TABLE DATA; Schema: public; Owner: corentinp
--

COPY topics (id, title, slug, created_at, updated_at, user_id, forum_id, is_sticky) FROM stdin;
1	Les Sablés de Zohra	les-sables-de-zohra	2017-02-09	2017-02-09	5	1	0
\.


--
-- Name: topics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: corentinp
--

SELECT pg_catalog.setval('topics_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: corentinp
--

COPY users (id, username, email, password, avatar, created_at, updated_at, updated_by, is_admin, is_moderator, is_confirmed, is_deleted) FROM stdin;
1	SysAdmin	sysadmin@codeigniter.dev	$2y$10$Cv57rLwiU3Ce04o4EZcmxOYAlNQVjSdaxlC2uW8QA7JtINSKjNZlC	default.jpg	2017-10-01	\N	\N	1	1	1	0
2	Admin	admin@codeigniter.dev	$2y$10$Cv57rLwiU3Ce04o4EZcmxOYAlNQVjSdaxlC2uW8QA7JtINSKjNZlC	default.jpg	2017-10-01	\N	\N	1	1	1	0
3	Moderator	moderator@codeigniter.dev	$2y$10$Cv57rLwiU3Ce04o4EZcmxOYAlNQVjSdaxlC2uW8QA7JtINSKjNZlC	default.jpg	2017-10-01	\N	\N	0	1	1	0
4	User	user@codeigniter.dev	$2y$10$Cv57rLwiU3Ce04o4EZcmxOYAlNQVjSdaxlC2uW8QA7JtINSKjNZlC	default.jpg	2017-10-01	\N	\N	0	0	1	0
5	Corentin	corentin@codeigniter.dev	$2y$10$Cv57rLwiU3Ce04o4EZcmxOYAlNQVjSdaxlC2uW8QA7JtINSKjNZlC	default.jpg	2017-10-01	\N	\N	0	0	1	0
6	Lola	zohra@codeigniter.dev	$2y$10$Cv57rLwiU3Ce04o4EZcmxOYAlNQVjSdaxlC2uW8QA7JtINSKjNZlC	default.jpg	2017-10-01	\N	\N	0	0	1	0
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: corentinp
--

SELECT pg_catalog.setval('users_id_seq', 6, true);


--
-- Name: ci_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: corentinp; Tablespace: 
--

ALTER TABLE ONLY ci_sessions
    ADD CONSTRAINT ci_sessions_pkey PRIMARY KEY (id, ip_address);


--
-- Name: forums_pkey; Type: CONSTRAINT; Schema: public; Owner: corentinp; Tablespace: 
--

ALTER TABLE ONLY forums
    ADD CONSTRAINT forums_pkey PRIMARY KEY (id);


--
-- Name: options_pkey; Type: CONSTRAINT; Schema: public; Owner: corentinp; Tablespace: 
--

ALTER TABLE ONLY options
    ADD CONSTRAINT options_pkey PRIMARY KEY (id);


--
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: corentinp; Tablespace: 
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: topics_pkey; Type: CONSTRAINT; Schema: public; Owner: corentinp; Tablespace: 
--

ALTER TABLE ONLY topics
    ADD CONSTRAINT topics_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: corentinp; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: ci_sessions_timestamp; Type: INDEX; Schema: public; Owner: corentinp; Tablespace: 
--

CREATE INDEX ci_sessions_timestamp ON ci_sessions USING btree ("timestamp");


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

