--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE client (
    id integer NOT NULL,
    nom character varying(50) NOT NULL,
    prenom character varying(30) NOT NULL,
    adresse character varying(50) NOT NULL,
    codepostal character varying(5) NOT NULL,
    log integer,
    mdp character(30)
);


ALTER TABLE client OWNER TO zohral;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO zohral;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- Name: commande; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE commande (
    id integer NOT NULL,
    numcde integer NOT NULL,
    datecde date NOT NULL
);


ALTER TABLE commande OWNER TO zohral;

--
-- Name: commande_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE commande_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE commande_id_seq OWNER TO zohral;

--
-- Name: commande_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE commande_id_seq OWNED BY commande.id;


--
-- Name: declinaison; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE declinaison (
    id integer NOT NULL,
    epaisseur integer NOT NULL
);


ALTER TABLE declinaison OWNER TO zohral;

--
-- Name: declinaison_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE declinaison_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE declinaison_id_seq OWNER TO zohral;

--
-- Name: declinaison_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE declinaison_id_seq OWNED BY declinaison.id;


--
-- Name: essence; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE essence (
    id integer NOT NULL,
    essence character varying(30) NOT NULL
);


ALTER TABLE essence OWNER TO zohral;

--
-- Name: essence_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE essence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE essence_id_seq OWNER TO zohral;

--
-- Name: essence_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE essence_id_seq OWNED BY essence.id;


--
-- Name: planche; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE planche (
    id integer NOT NULL,
    essence character varying(30) NOT NULL,
    reference character varying(20) NOT NULL,
    largeur integer DEFAULT 1 NOT NULL,
    longueur integer DEFAULT 1 NOT NULL,
    prixgache integer,
    prixforme integer
);


ALTER TABLE planche OWNER TO zohral;

--
-- Name: planche_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE planche_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE planche_id_seq OWNER TO zohral;

--
-- Name: planche_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE planche_id_seq OWNED BY planche.id;


--
-- Name: typologie; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE typologie (
    id integer NOT NULL,
    qualite character varying(50) NOT NULL
);


ALTER TABLE typologie OWNER TO zohral;

--
-- Name: typologie_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE typologie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE typologie_id_seq OWNER TO zohral;

--
-- Name: typologie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE typologie_id_seq OWNED BY typologie.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY commande ALTER COLUMN id SET DEFAULT nextval('commande_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY declinaison ALTER COLUMN id SET DEFAULT nextval('declinaison_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY essence ALTER COLUMN id SET DEFAULT nextval('essence_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY planche ALTER COLUMN id SET DEFAULT nextval('planche_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY typologie ALTER COLUMN id SET DEFAULT nextval('typologie_id_seq'::regclass);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY client (id, nom, prenom, adresse, codepostal, log, mdp) FROM stdin;
1	DUPONT	ALBERT	5 RUE CHARBONE	23000	1	\N
2	BOURGEOIS	EDOUARD	8 BD CLEMENCEAU	16000	2	\N
3	CORNEIL	SYLVIE	9 RUE STALINGRAD	89004	3	\N
4	DJIDJ	YASSINE	1 ALLEE DU PARC	21003	4	\N
5	GABRIEL	EVELYNE	2 RUE DU JARDIN	50010	5	\N
\.


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('client_id_seq', 5, true);


--
-- Data for Name: commande; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY commande (id, numcde, datecde) FROM stdin;
1	1	2007-01-02
\.


--
-- Name: commande_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('commande_id_seq', 1, true);


--
-- Data for Name: declinaison; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY declinaison (id, epaisseur) FROM stdin;
\.


--
-- Name: declinaison_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('declinaison_id_seq', 1, false);


--
-- Data for Name: essence; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY essence (id, essence) FROM stdin;
\.


--
-- Name: essence_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('essence_id_seq', 1, false);


--
-- Data for Name: planche; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY planche (id, essence, reference, largeur, longueur, prixgache, prixforme) FROM stdin;
1	SAPIN	NODEXAS2MF	20	15	10	15
4	MERISIER	NODEX54GGG	56	856	12	13
3	CHENE	NODEX52GKG	820	304	11	14
2	EBENE	NODEXKLH6K	500	140	30	25
7	SAPIN	NODEXKH455	85	456	11	55
\.


--
-- Name: planche_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('planche_id_seq', 7, true);


--
-- Data for Name: typologie; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY typologie (id, qualite) FROM stdin;
\.


--
-- Name: typologie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('typologie_id_seq', 1, false);


--
-- Name: declinaison_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY typologie
    ADD CONSTRAINT declinaison_pkey PRIMARY KEY (id);


--
-- Name: essence_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY essence
    ADD CONSTRAINT essence_pkey PRIMARY KEY (id);


--
-- Name: id_client_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY commande
    ADD CONSTRAINT id_client_pkey PRIMARY KEY (id);


--
-- Name: planche_declinaison_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY declinaison
    ADD CONSTRAINT planche_declinaison_pkey PRIMARY KEY (id);


--
-- Name: planche_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY planche
    ADD CONSTRAINT planche_pkey PRIMARY KEY (id);


--
-- Name: typologie_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT typologie_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: client; Type: ACL; Schema: public; Owner: zohral
--

REVOKE ALL ON TABLE client FROM PUBLIC;
REVOKE ALL ON TABLE client FROM zohral;
GRANT ALL ON TABLE client TO zohral;
GRANT ALL ON TABLE client TO mariel;


--
-- Name: commande; Type: ACL; Schema: public; Owner: zohral
--

REVOKE ALL ON TABLE commande FROM PUBLIC;
REVOKE ALL ON TABLE commande FROM zohral;
GRANT ALL ON TABLE commande TO zohral;
GRANT ALL ON TABLE commande TO mariel;


--
-- Name: declinaison; Type: ACL; Schema: public; Owner: zohral
--

REVOKE ALL ON TABLE declinaison FROM PUBLIC;
REVOKE ALL ON TABLE declinaison FROM zohral;
GRANT ALL ON TABLE declinaison TO zohral;
GRANT ALL ON TABLE declinaison TO mariel;


--
-- Name: essence; Type: ACL; Schema: public; Owner: zohral
--

REVOKE ALL ON TABLE essence FROM PUBLIC;
REVOKE ALL ON TABLE essence FROM zohral;
GRANT ALL ON TABLE essence TO zohral;
GRANT ALL ON TABLE essence TO mariel;


--
-- Name: planche; Type: ACL; Schema: public; Owner: zohral
--

REVOKE ALL ON TABLE planche FROM PUBLIC;
REVOKE ALL ON TABLE planche FROM zohral;
GRANT ALL ON TABLE planche TO zohral;
GRANT ALL ON TABLE planche TO mariel;


--
-- Name: typologie; Type: ACL; Schema: public; Owner: zohral
--

REVOKE ALL ON TABLE typologie FROM PUBLIC;
REVOKE ALL ON TABLE typologie FROM zohral;
GRANT ALL ON TABLE typologie TO zohral;
GRANT ALL ON TABLE typologie TO mariel;


--
-- PostgreSQL database dump complete
--

