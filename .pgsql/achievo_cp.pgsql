--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: succes; Type: TABLE; Schema: public; Owner: achiever; Tablespace: 
--

CREATE TABLE succes (
    id integer NOT NULL,
    titre character varying(200) NOT NULL,
    intitule character varying(600) NOT NULL,
    objectif integer NOT NULL,
    progression integer NOT NULL,
    idutilisateur integer NOT NULL,
    CONSTRAINT succes_check CHECK ((progression <= objectif)),
    CONSTRAINT succes_objectif_check CHECK ((objectif > 0))
);


ALTER TABLE succes OWNER TO achiever;

--
-- Name: succes_id_seq; Type: SEQUENCE; Schema: public; Owner: achiever
--

CREATE SEQUENCE succes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE succes_id_seq OWNER TO achiever;

--
-- Name: succes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: achiever
--

ALTER SEQUENCE succes_id_seq OWNED BY succes.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: achiever; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    entreprise character varying(70) NOT NULL,
    mdp character(128) NOT NULL
);


ALTER TABLE utilisateur OWNER TO achiever;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: achiever
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO achiever;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: achiever
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: achiever
--

ALTER TABLE ONLY succes ALTER COLUMN id SET DEFAULT nextval('succes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: achiever
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: succes; Type: TABLE DATA; Schema: public; Owner: achiever
--

COPY succes (id, titre, intitule, objectif, progression, idutilisateur) FROM stdin;
1	Prototype	Vous avez réalisé le prototype de votre produit	1	1	1
2	Origamme	Vous avez complété votre première gamme de produits, soit un total de 20 références distinctes	20	20	1
4	Jamais à court	Vous avez sécurisé un contrat bien négocié avec au moins 2 fournisseurs	2	0	1
5	Adjugé, vendu !	Vous avez réalisé votre première vente (et peut-être même une deuxième pendant que vous lisez ce message !)	1	1	1
6	C'est un oiseau ? un avion ? non, c'est un origami !	Vous avez réalisé 250 ventes, vos origamis ont le vent en poupe !	250	0	1
7	Ma passion ? J'en vis ,-)	Vous venez de vous verser votre premier salaire !	1	0	1
8	L'espoir fait vivre, pour de vrai !	Vous vous versez votre salaire depuis 6 mois	6	0	1
10	Plus assez de doigts...	Vous avez signé 21 partenariats commerciaux avec des hopitaux	21	0	1
11	L'espoir ne fait pas vivre que vous !	Vous avez engagé votre premier salarié, tâchez de le garder et de le chouchouter	1	0	1
12	Origami humanum est	Vous avez envoyé bénévolement 2500 origamis à des organismes caritatifs	2500	0	1
13	Pivoter pour mieux régner	Vous avez opéré un pivot dans votre phase de développement	1	0	1
14	Un vrai serpent	Vous avez opéré 3 pivots dans votre phase de développement !	3	0	1
15	Une vraie girouette	Vous avez opéré 5 pivots dans votre phase de développement !!	5	5	1
16	Faire vivre l'espoir	Votre société compte 5 salariés en CDI	5	0	1
3	MVP	Vous avez assez de modèles différents pour vous lancer, alors qu'attendez-vous ?	60	15	1
9	Sur les doigts de la main	Vous avez signé 5 partenariats commerciaux avec des hopitaux	5	2	1
\.


--
-- Name: succes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: achiever
--

SELECT pg_catalog.setval('succes_id_seq', 4, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: achiever
--

COPY utilisateur (id, entreprise, mdp) FROM stdin;
1	fope	ab54fd43bf4cbdf123d392bb17e56a1fabfbec11b6657aa61c9d58969e989b440f39bb54d42157768a4c4b7d4b83a543c74a9462a592bb2519896f15454ce949
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: achiever
--

SELECT pg_catalog.setval('utilisateur_id_seq', 1, false);


--
-- Name: succes_pkey; Type: CONSTRAINT; Schema: public; Owner: achiever; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_pkey PRIMARY KEY (id);


--
-- Name: succes_titre_unique; Type: CONSTRAINT; Schema: public; Owner: achiever; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_titre_unique UNIQUE (titre, idutilisateur);


--
-- Name: utilisateur_entreprise_key; Type: CONSTRAINT; Schema: public; Owner: achiever; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_entreprise_key UNIQUE (entreprise);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: achiever; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: succes_idutilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: achiever
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_idutilisateur_fkey FOREIGN KEY (idutilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

