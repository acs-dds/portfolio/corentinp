--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: email; Type: DOMAIN; Schema: public; Owner: faridl
--

CREATE DOMAIN email AS text
	CONSTRAINT email_check CHECK ((VALUE ~ '^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,}$'::text));


ALTER DOMAIN email OWNER TO faridl;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: discussion; Type: TABLE; Schema: public; Owner: faridl; Tablespace: 
--

CREATE TABLE discussion (
    id integer NOT NULL,
    intitule character varying(100) NOT NULL,
    mdp character varying(100)
);


ALTER TABLE discussion OWNER TO faridl;

--
-- Name: discussion_id_seq; Type: SEQUENCE; Schema: public; Owner: faridl
--

CREATE SEQUENCE discussion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE discussion_id_seq OWNER TO faridl;

--
-- Name: discussion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: faridl
--

ALTER SEQUENCE discussion_id_seq OWNED BY discussion.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: faridl; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    content character varying(100) NOT NULL,
    date timestamp without time zone NOT NULL,
    id_discussion integer,
    id_utilisateur integer
);


ALTER TABLE message OWNER TO faridl;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: faridl
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO faridl;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: faridl
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: faridl; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    nom character varying(100) NOT NULL,
    prenom character varying(100) NOT NULL,
    pseudo character varying(20) NOT NULL,
    email email NOT NULL,
    mdp character varying(100),
    ping timestamp without time zone
);


ALTER TABLE utilisateur OWNER TO faridl;

--
-- Name: resume; Type: VIEW; Schema: public; Owner: faridl
--

CREATE VIEW resume AS
 SELECT utilisateur.pseudo,
    discussion.intitule,
    message.content,
    message.date
   FROM ((message
     JOIN utilisateur ON ((message.id_utilisateur = utilisateur.id)))
     JOIN discussion ON ((message.id_discussion = discussion.id)))
  WHERE ((discussion.intitule)::text = 'general'::text);


ALTER TABLE resume OWNER TO faridl;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: faridl
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO faridl;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: faridl
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: faridl
--

ALTER TABLE ONLY discussion ALTER COLUMN id SET DEFAULT nextval('discussion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: faridl
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: faridl
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: discussion; Type: TABLE DATA; Schema: public; Owner: faridl
--

COPY discussion (id, intitule, mdp) FROM stdin;
1	general	\N
2	newd	\N
3	heckton	\N
14	heckton	\N
15	general	\N
16	general	\N
17	salut	\N
18	general	\N
19	general	\N
20	general	\N
21	general	\N
22	general	\N
23	heckton	\N
24	newd	\N
25	newd	\N
26	salut	\N
27	newd	\N
28	newd	\N
29	newd	\N
30	newd	\N
31	newd	\N
32	newd	\N
33	newd	\N
34	newd	\N
35	newd	\N
36	general	\N
37	general	\N
38	general	\N
39	general	\N
40	general	\N
41	general	\N
42	general	\N
43	general	\N
44	general	\N
45	general	\N
46	general	\N
47	general	\N
48	general	\N
49	general	\N
50	general	\N
51	general	\N
52	general	\N
53	general	\N
54	general	\N
55	general	\N
56	general	\N
57	general	\N
58	general	\N
59	general	\N
60	general	\N
61	general	\N
62	general	\N
63	[object HTMLCollection]	\N
64	general	\N
65	general	\N
66	general	\N
67	general	\N
68	general	\N
69	general	\N
70	general	\N
71	general	\N
72	general	\N
73	general	\N
74	general	\N
75	general	\N
76	general	\N
77	general	\N
78	general	\N
79	general	\N
80	general	\N
81	general	\N
82	general	\N
83	general	\N
84	general	\N
85	general	\N
86	general	\N
87	general	\N
88	general	\N
89	general	\N
90	general	\N
91	general	\N
92	general	\N
93	general	\N
94	general	\N
95	general	\N
96	general	\N
97	general	\N
98	general	\N
99	general	\N
100	general	\N
101	general	\N
102	general	\N
103		\N
104	general	\N
105	general	\N
106	general	\N
107	general	\N
108	general	\N
109	general	\N
110	general	\N
111	general	\N
112	general	\N
113	general	\N
114	general	\N
115	general	\N
116	general	\N
117	general	\N
118		\N
119	general	\N
120	general	\N
121	general	\N
122	general	\N
123	general	\N
124	general	\N
125	general	\N
126	general	\N
127	general	\N
128	general	\N
129	general	\N
130	general	\N
131	general	\N
132	general	\N
133	general	\N
134	general	\N
135	general	\N
136	general	\N
137	general	\N
138	heckton	\N
139	heckton	\N
140	heckton	\N
141	general	\N
142	general	\N
143	general	\N
144	general	\N
145	general	\N
146	general	\N
147	general	\N
148	general	\N
149	general	\N
150	general	\N
151	salut	\N
152	salut	\N
153	salut	\N
154	salut	\N
155	salut	\N
156	salut	\N
157	general	\N
158	general	\N
159	general	\N
160	general	\N
161	general	\N
162	general	\N
163	general	\N
164	general	\N
165	general	\N
166	general	\N
167	general	\N
168	general	\N
169	general	\N
170	general	\N
171	general	\N
172	general	\N
173	general	\N
174	general	\N
175	general	\N
176	general	\N
177	general	\N
178	general	\N
179	general	\N
180	general	\N
181	general	\N
182	general	\N
183	general	\N
184	general	\N
185	general	\N
186	general	\N
187	general	\N
188	heckton	\N
189	heckton	\N
190	heckton	\N
191	heckton	\N
192	heckton	\N
193	heckton	\N
194	heckton	\N
195	heckton	\N
196	newd	\N
197	newd	\N
198	salut	\N
199	salut	\N
200	salut	\N
201	salut	\N
202	salut	\N
203	general	\N
204	general	\N
205	general	\N
206	general	\N
\.


--
-- Name: discussion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: faridl
--

SELECT pg_catalog.setval('discussion_id_seq', 206, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: faridl
--

COPY message (id, content, date, id_discussion, id_utilisateur) FROM stdin;
271	ici gerrard	2017-03-20 16:57:03	1	57
272	ici gerrard	2017-03-20 16:57:14	1	57
273	je suis la	2017-03-20 17:01:36	1	57
274	je suis la	2017-03-20 17:01:36	1	57
275	je suis la	2017-03-20 17:01:36	1	57
276	je suis la	2017-03-20 17:01:36	1	57
277	je suis la	2017-03-20 17:01:36	1	57
278	ici gerrard	2017-03-20 17:18:38	3	57
279	ici gerrard	2017-03-20 17:18:38	3	57
280	ici gerrard	2017-03-20 17:20:21	3	57
281	ici gerrard	2017-03-20 17:20:21	3	57
282	ici gerrard	2017-03-20 17:20:21	3	57
283	ici gerrard	2017-03-20 17:20:21	3	57
284	ici gerrard	2017-03-20 17:20:21	3	57
285	ici gerrard	2017-03-20 17:20:21	3	57
286	gyuguyguyg	2017-03-20 17:21:10	2	57
287	gyuguyguyg	2017-03-20 17:21:10	2	57
288	gyuguyguyg	2017-03-20 17:21:54	17	57
289	gyuguyguyg	2017-03-20 17:21:54	17	57
290	gyuguyguyg	2017-03-20 17:21:54	17	57
291	gyuguyguyg	2017-03-20 17:21:54	17	57
292	kikou	2017-03-20 17:22:28	17	57
293	ici gerrard	2017-03-20 17:24:25	1	57
294	ici gerrard	2017-03-20 17:24:25	1	57
295	ezfeae	2017-03-20 19:31:25	1	57
296	vfzvzrzververvgerver	2017-03-20 19:31:25	1	57
247	je test la fct insertmessage	2017-03-20 16:32:06	1	57
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: faridl
--

SELECT pg_catalog.setval('message_id_seq', 296, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: faridl
--

COPY utilisateur (id, nom, prenom, pseudo, email, mdp, ping) FROM stdin;
56	farid	dsc	walid	yoyohko@gmail.com	992054a407ee4c3978ace4088908acf4	2017-03-17 16:37:17.125094
57	farid	farid	farid	camillecoquart@hotmail.fr	a1d12da42d4302e53d510954344ad164	2017-03-20 19:31:54.979403
54	gerrard	farid	komodor21	yoyohko@gmail.com	5ab660c7809c6077351e6e6f6700d349	\N
55	gerrard	dsc	corentin	yoyohko@gmail.com	202cb962ac59075b964b07152d234b70	\N
60	Perrot	Corentin	Kasai.	yoyohko@gmail.com	e10adc3949ba59abbe56e057f20f883e	2017-03-08 14:36:54.399672
63	latti	yassine	crackj	yassine@gmail.com	e10adc3949ba59abbe56e057f20f883e	2017-03-08 16:06:11.935586
64	gerrard	farid	komodor	tag@gmail.com	e10adc3949ba59abbe56e057f20f883e	\N
65	gerrard	farid	komodor	tag@gmail.com	e10adc3949ba59abbe56e057f20f883e	\N
66	gerrard	farid	komodor	tag@gmail.com	e10adc3949ba59abbe56e057f20f883e	\N
68	farid	dsc	farid	g@hf.com	e10adc3949ba59abbe56e057f20f883e	2017-03-17 15:02:35.644803
67	farid	dsc	farid	g@hf.com	e10adc3949ba59abbe56e057f20f883e	2017-03-17 15:02:36.484503
69	farid	dsc	farid	g@hf.com	e10adc3949ba59abbe56e057f20f883e	2017-03-17 15:02:36.529264
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: faridl
--

SELECT pg_catalog.setval('utilisateur_id_seq', 69, true);


--
-- Name: discussion_pkey; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY discussion
    ADD CONSTRAINT discussion_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: message_discussion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: faridl
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_discussion_fkey FOREIGN KEY (id_discussion) REFERENCES discussion(id);


--
-- Name: message_utilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: faridl
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

