--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client_online; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE client_online (
    id integer NOT NULL,
    client_on character varying(255) NOT NULL,
    client_off character varying(255)
);


ALTER TABLE client_online OWNER TO yassinel;

--
-- Name: client_online_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE client_online_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_online_id_seq OWNER TO yassinel;

--
-- Name: client_online_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE client_online_id_seq OWNED BY client_online.id;


--
-- Name: liste_users; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE liste_users (
    id integer NOT NULL,
    client_user character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    pass character varying(255) NOT NULL,
    temps_reel timestamp without time zone DEFAULT now()
);


ALTER TABLE liste_users OWNER TO yassinel;

--
-- Name: liste_users_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE liste_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE liste_users_id_seq OWNER TO yassinel;

--
-- Name: liste_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE liste_users_id_seq OWNED BY liste_users.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    message character varying(255),
    temps_message timestamp without time zone DEFAULT now(),
    id_salon integer NOT NULL,
    id_client integer
);


ALTER TABLE message OWNER TO yassinel;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO yassinel;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: salon; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE salon (
    id integer NOT NULL,
    libelle character varying(255) NOT NULL,
    mdp character varying(255)
);


ALTER TABLE salon OWNER TO yassinel;

--
-- Name: salon_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE salon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE salon_id_seq OWNER TO yassinel;

--
-- Name: salon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE salon_id_seq OWNED BY salon.id;


--
-- Name: tchat; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE tchat (
    id integer NOT NULL,
    pseudo character varying(255),
    message character varying(255),
    temps_message timestamp without time zone DEFAULT now()
);


ALTER TABLE tchat OWNER TO yassinel;

--
-- Name: tchat_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE tchat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tchat_id_seq OWNER TO yassinel;

--
-- Name: tchat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE tchat_id_seq OWNED BY tchat.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY client_online ALTER COLUMN id SET DEFAULT nextval('client_online_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY liste_users ALTER COLUMN id SET DEFAULT nextval('liste_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY salon ALTER COLUMN id SET DEFAULT nextval('salon_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY tchat ALTER COLUMN id SET DEFAULT nextval('tchat_id_seq'::regclass);


--
-- Data for Name: client_online; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY client_online (id, client_on, client_off) FROM stdin;
\.


--
-- Name: client_online_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('client_online_id_seq', 129, true);


--
-- Data for Name: liste_users; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY liste_users (id, client_user, email, pass, temps_reel) FROM stdin;
957	yassine	yassine@gmail.com	$2y$10$2fJqNCrHgqxUrrbTH8iFB.4o19TK63DuOdR2zkvGxiQOe.QbJuBVW	2017-03-06 11:16:46.668695
958	corentin	corentin@gmail.com	$2y$10$tO.momJ1Md3iOwXPX2Y.VuLKyifd10Z5NRcBhWVVT6ivsgYMc2mVC	2017-03-06 11:17:30.263173
959	nabs	nabs@gmail.com	$2y$10$/1PgOEb8.HmSu930LCxfLe2s6eUV4doclISpoaPgWaaky2tBSHnHK	2017-03-07 10:11:26.95126
960	yhk	yhk@boz.com	$2y$10$HewXyIJnQjsLCJ10ka0uO.OdMJMAsV/oOoUR4w0vliUSUZA9hquem	2017-03-07 10:26:16.034435
961	boubou	boubou@gmail.com	$2y$10$klELbkmQyyUILxG0/d8gvONa/pOgIUDhw9GSYGPUUSktdtDdc05Tq	2017-03-07 16:05:38.390815
962	farid	hbjgvjgvj@j.fr	$2y$10$L2ZU11fmL457ipee0MDqle.ux4F9D.Oo4sCSzEwK3Ry6h6wf.6LjC	2017-03-07 16:25:18.812858
963	walidb	test@gmail.com	$2y$10$lZIlqziZnZxCUey.I/0QUOD/ZxNbwJdDBvSKZefRXlUyo3k/mp7HO	2017-03-07 16:29:51.724752
964	mims	mims@gmail.com	$2y$10$s/3B89mx4Plc.jPy60WvleCbKcik8q9ewHT8gd9CpmScHTZmj2USS	2017-03-08 09:11:37.897908
965	jean	jean@gmail.com	$2y$10$Yi.N3LuNAbLZEoq8tMP2neSB.XaisGeUlAXqz6eYBSQ9zF/aUTXmO	2017-03-08 10:36:52.839972
966	test	test@test.com	$2y$10$3YVWZtXtM1K1YffJkjHQQuq88.ws2cqQ4NewW8wITTnpu5zjWntha	2017-03-08 10:39:10.026911
967	zohra	zohra@gmail.gmail.com	$2y$10$LOuW6xD7xgMCTKHiAGsP/.CHZNdNnISimQ7yARiFxG.avk967JB1m	2017-03-08 10:40:58.13196
968	walid 	walid@gmail.com	$2y$10$UAkDREEw7ve9GknUoFjEWefzgX50FDwHzJ3XaamKF3c6CfI2FA6Hm	2017-03-08 12:09:59.991354
969	myriam	unknow@hmildk.f	$2y$10$LpkW/0/I4/2BaW.vcFO7q.ob7C9yKh.iTqqC7drIFW9/cIXDOoQnK	2017-03-18 19:21:04.262612
\.


--
-- Name: liste_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('liste_users_id_seq', 969, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY message (id, message, temps_message, id_salon, id_client) FROM stdin;
159	bonjour	2017-03-08 13:16:07.491749	1	957
160	TTRRT	2017-03-08 13:23:27.009133	1	957
161	t	2017-03-08 13:23:44.037785	1	957
162	zerzerzr	2017-03-08 13:42:17.319858	1	959
163	thtfrhtrrh	2017-03-08 13:42:27.482101	1	957
164	zerzerzr	2017-03-08 13:42:34.493679	1	959
165	eaze	2017-03-08 13:47:07.318916	1	959
166	oijhkjh	2017-03-08 13:47:17.971848	1	959
169	coucou	2017-03-08 16:11:50.442896	1	957
170	tttt	2017-03-08 16:21:56.844362	1	957
171	ggggggg	2017-03-08 16:22:02.929054	1	957
172	hbgggnghhnhghngn	2017-03-08 16:22:09.893407	1	957
173	vvvv	2017-03-08 16:22:15.938691	1	957
174	yassibe	2017-03-08 16:42:57.61116	1	957
175	Kik 	2017-03-08 17:58:00.947435	1	\N
176	Salut	2017-03-08 17:58:46.560123	1	957
177	Salut	2017-03-08 17:59:02.900734	1	957
178	Couc&icirc;u	2017-03-09 19:14:13.128199	1	957
179	Couc&icirc;u	2017-03-09 19:14:23.640834	1	957
180	Couc&icirc;u	2017-03-09 19:14:26.767362	1	957
181	kikou	2017-03-10 08:39:00.197607	1	957
182	sss	2017-03-11 14:22:37.476169	1	957
183	mmm	2017-03-12 01:56:59.288041	1	957
184	ll	2017-03-18 15:27:40.109663	1	957
185	gfojtzjgerg	2017-03-18 19:21:23.352967	1	969
186	uyhtxhuyr	2017-03-24 15:07:31.483553	1	957
187	ykkyy	2017-03-24 15:07:38.450663	1	957
188	Jdjzidbdkzkfbdlzbdldbeoxneio	2017-04-09 00:28:18.833815	1	957
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('message_id_seq', 188, true);


--
-- Data for Name: salon; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY salon (id, libelle, mdp) FROM stdin;
1	generale	\N
2	private	\N
\.


--
-- Name: salon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('salon_id_seq', 1, true);


--
-- Data for Name: tchat; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY tchat (id, pseudo, message, temps_message) FROM stdin;
9134	quentin	12356\r\n\r\n	2017-03-10 16:09:59.729569
\.


--
-- Name: tchat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('tchat_id_seq', 9134, true);


--
-- Name: client_online_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY client_online
    ADD CONSTRAINT client_online_pkey PRIMARY KEY (id);


--
-- Name: liste_users_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY liste_users
    ADD CONSTRAINT liste_users_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: salon_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY salon
    ADD CONSTRAINT salon_pkey PRIMARY KEY (id);


--
-- Name: message_id_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_client_fkey FOREIGN KEY (id_client) REFERENCES liste_users(id);


--
-- Name: message_id_salon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_salon_fkey FOREIGN KEY (id_salon) REFERENCES salon(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

