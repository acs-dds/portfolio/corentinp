--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: news; Type: TABLE; Schema: public; Owner: camillec; Tablespace: 
--

CREATE TABLE news (
    id integer NOT NULL,
    login text,
    nom text,
    prenom text,
    slug text
);


ALTER TABLE news OWNER TO camillec;

--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: camillec
--

CREATE SEQUENCE news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE news_id_seq OWNER TO camillec;

--
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: camillec
--

ALTER SEQUENCE news_id_seq OWNED BY news.id;


--
-- Name: planete; Type: TABLE; Schema: public; Owner: camillec; Tablespace: 
--

CREATE TABLE planete (
    nom text,
    distance integer,
    id integer NOT NULL
);


ALTER TABLE planete OWNER TO camillec;

--
-- Name: planete_id_seq; Type: SEQUENCE; Schema: public; Owner: camillec
--

CREATE SEQUENCE planete_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE planete_id_seq OWNER TO camillec;

--
-- Name: planete_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: camillec
--

ALTER SEQUENCE planete_id_seq OWNED BY planete.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: camillec
--

ALTER TABLE ONLY news ALTER COLUMN id SET DEFAULT nextval('news_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: camillec
--

ALTER TABLE ONLY planete ALTER COLUMN id SET DEFAULT nextval('planete_id_seq'::regclass);


--
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: camillec
--

COPY news (id, login, nom, prenom, slug) FROM stdin;
1	\N	\N	\N	\N
2	\N	\N	\N	\N
3	\N	\N	\N	\N
4	\N	c	\N	
5	\N	cam	\N	
6	\N	cam	cam	
7	mille	cam	cam	
8	mille	cam	cam	\N
\.


--
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: camillec
--

SELECT pg_catalog.setval('news_id_seq', 8, true);


--
-- Data for Name: planete; Type: TABLE DATA; Schema: public; Owner: camillec
--

COPY planete (nom, distance, id) FROM stdin;
Mercure	1	1
Venus	7	2
Terre	1	3
\.


--
-- Name: planete_id_seq; Type: SEQUENCE SET; Schema: public; Owner: camillec
--

SELECT pg_catalog.setval('planete_id_seq', 3, true);


--
-- Name: news_pkey; Type: CONSTRAINT; Schema: public; Owner: camillec; Tablespace: 
--

ALTER TABLE ONLY news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: planete_pkey; Type: CONSTRAINT; Schema: public; Owner: camillec; Tablespace: 
--

ALTER TABLE ONLY planete
    ADD CONSTRAINT planete_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

