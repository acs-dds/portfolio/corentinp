--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: succes; Type: TABLE; Schema: public; Owner: achiever; Tablespace: 
--

CREATE TABLE succes (
    id integer NOT NULL,
    titre character varying(200) NOT NULL,
    intitule character varying(600) NOT NULL,
    objectif integer NOT NULL,
    progression integer NOT NULL,
    idutilisateur integer NOT NULL,
    CONSTRAINT succes_check CHECK ((progression <= objectif)),
    CONSTRAINT succes_objectif_check CHECK ((objectif > 0))
);


ALTER TABLE succes OWNER TO achiever;

--
-- Name: succes_id_seq; Type: SEQUENCE; Schema: public; Owner: achiever
--

CREATE SEQUENCE succes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE succes_id_seq OWNER TO achiever;

--
-- Name: succes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: achiever
--

ALTER SEQUENCE succes_id_seq OWNED BY succes.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: achiever; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    entreprise character varying(70) NOT NULL,
    mdp character(128) NOT NULL
);


ALTER TABLE utilisateur OWNER TO achiever;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: achiever
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO achiever;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: achiever
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: vuesucces; Type: VIEW; Schema: public; Owner: achiever
--

CREATE VIEW vuesucces AS
 SELECT succes.id,
    succes.titre,
    succes.intitule,
    succes.objectif,
    succes.progression,
    succes.idutilisateur,
    (succes.progression = succes.objectif) AS fini
   FROM succes
UNION
 SELECT 0 AS id,
    'C''est qui le patron ?'::character varying AS titre,
    'Vous avez atteint tous les objectifs que vous vous êtes fixés ! N''hésitez pas à en définir de nouveaux ;-)'::character varying AS intitule,
    tous.c AS objectif,
    COALESCE(finis.c, (0)::bigint) AS progression,
    tous.i AS idutilisateur,
    COALESCE((tous.c = finis.c), false) AS fini
   FROM (( SELECT count(*) AS c,
            succes.idutilisateur AS i
           FROM succes
          GROUP BY succes.idutilisateur) tous
     LEFT JOIN ( SELECT count(*) AS c,
            succes.idutilisateur AS i
           FROM succes
          WHERE (succes.progression = succes.objectif)
          GROUP BY succes.idutilisateur) finis ON ((tous.i = finis.i)));


ALTER TABLE vuesucces OWNER TO achiever;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: achiever
--

ALTER TABLE ONLY succes ALTER COLUMN id SET DEFAULT nextval('succes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: achiever
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: succes; Type: TABLE DATA; Schema: public; Owner: achiever
--

COPY succes (id, titre, intitule, objectif, progression, idutilisateur) FROM stdin;
10	Plus assez de doigts...	Vous avez signé 21 partenariats commerciaux avec des hopitaux	21	21	1
6	C'est un oiseau ? un avion ? non, c'est un origami !	Vous avez réalisé 250 ventes, vos origamis ont le vent en poupe !	250	250	1
14	Un vrai serpent	Vous avez opéré 3 pivots dans votre phase de développement !	3	3	1
8	L'espoir fait vivre, pour de vrai !	Vous vous versez votre salaire depuis 6 mois	6	6	1
9	Sur les doigts de la main	Vous avez signé 5 partenariats commerciaux avec des hopitaux	5	2	1
18	La persévérance paiera	Vous vous êtes échappés 50 fois de votre cage	50	5	3
20	Ca y est, Minus ! Nous avons conquis le monde	Vous avez conquis le monde ;-) Bravo, maître !	1	1	3
21	Errare Minusum est	Votre collaborateur a fait capoter 50 de vos plans les plus ingénieux	50	14	3
1	Prototype	Vous avez réalisé le prototype de votre produit	1	1	1
12	Origami humanum est	Vous avez envoyé bénévolement 2500 origamis à des organismes caritatifs	2500	2500	1
3	MVP	Vous avez assez de modèles différents pour vous lancer, alors qu'attendez-vous ?	60	60	1
2	Origamme	Vous avez complété votre première gamme de produits, soit un total de 20 références distinctes	20	20	1
7	Ma passion ? J'en vis ;-)	Vous venez de vous verser votre premier salaire !	1	1	1
4	Jamais à court	Vous avez sécurisé un contrat bien négocié avec au moins 2 fournisseurs	2	1	1
15	Une vraie girouette	Vous avez opéré 5 pivots dans votre phase de développement !!	5	4	1
19	La même chose que chaque soir, Minus...	Vous vous êtes échappés 500 fois de votre cage et le monde n'est toujours pas conquis	500	100	3
5	Adjugé, vendu !	Vous avez réalisé votre première vente (et peut-être même une deuxième pendant que vous lisez ce message !)	1	1	1
16	Faire vivre l'espoir	Votre société compte 5 salariés en CDI	5	1	1
11	L'espoir ne fait pas vivre que vous !	Vous avez engagé votre premier salarié, tâchez de le garder et de le chouchouter	1	1	1
13	Pivoter pour mieux régner	Vous avez opéré un pivot dans votre phase de développement	1	1	1
17	La voie est libre	Vous vous êtes échappés 5 nuits d'affilée de votre cage	5	0	3
\.


--
-- Name: succes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: achiever
--

SELECT pg_catalog.setval('succes_id_seq', 21, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: achiever
--

COPY utilisateur (id, entreprise, mdp) FROM stdin;
1	fope	ab54fd43bf4cbdf123d392bb17e56a1fabfbec11b6657aa61c9d58969e989b440f39bb54d42157768a4c4b7d4b83a543c74a9462a592bb2519896f15454ce949
3	cortex	758aad7d91f0a80359bad3a0fea93c7a1d2bb50ebe2f3e33a0e53fe2c3fb3f94e75fe8726422d65f29937c246e732920bbef5e7eaa481e30e886f6b8c9040398
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: achiever
--

SELECT pg_catalog.setval('utilisateur_id_seq', 3, true);


--
-- Name: succes_pkey; Type: CONSTRAINT; Schema: public; Owner: achiever; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_pkey PRIMARY KEY (id);


--
-- Name: succes_titre_unique; Type: CONSTRAINT; Schema: public; Owner: achiever; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_titre_unique UNIQUE (titre, idutilisateur);


--
-- Name: utilisateur_entreprise_key; Type: CONSTRAINT; Schema: public; Owner: achiever; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_entreprise_key UNIQUE (entreprise);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: achiever; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: succes_idutilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: achiever
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_idutilisateur_fkey FOREIGN KEY (idutilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

