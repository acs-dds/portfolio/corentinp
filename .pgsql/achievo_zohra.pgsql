--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: succes; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE succes (
    id integer NOT NULL,
    titre character varying(200) NOT NULL,
    intitule character varying(600) NOT NULL,
    objectif integer NOT NULL,
    progression integer NOT NULL,
    idutilisateur integer NOT NULL,
    CONSTRAINT succes_check CHECK ((progression <= objectif)),
    CONSTRAINT succes_objectif_check CHECK ((objectif > 0))
);


ALTER TABLE succes OWNER TO zohral;

--
-- Name: succes_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE succes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE succes_id_seq OWNER TO zohral;

--
-- Name: succes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE succes_id_seq OWNED BY succes.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    entreprise character varying(70) NOT NULL,
    mdp character(128) NOT NULL
);


ALTER TABLE utilisateur OWNER TO zohral;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO zohral;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY succes ALTER COLUMN id SET DEFAULT nextval('succes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: succes; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY succes (id, titre, intitule, objectif, progression, idutilisateur) FROM stdin;
\.


--
-- Name: succes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('succes_id_seq', 1, false);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY utilisateur (id, entreprise, mdp) FROM stdin;
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('utilisateur_id_seq', 1, false);


--
-- Name: succes_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_pkey PRIMARY KEY (id);


--
-- Name: succes_titre_unique; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_titre_unique UNIQUE (titre, idutilisateur);


--
-- Name: utilisateur_entreprise_key; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_entreprise_key UNIQUE (entreprise);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: succes_idutilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY succes
    ADD CONSTRAINT succes_idutilisateur_fkey FOREIGN KEY (idutilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

