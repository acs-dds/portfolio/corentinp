--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client; Type: TABLE; Schema: public; Owner: faridl; Tablespace: 
--

CREATE TABLE client (
    id integer NOT NULL,
    token character varying(100) NOT NULL,
    ttl interval,
    idhotel integer
);


ALTER TABLE client OWNER TO faridl;

--
-- Name: generateur_id_seq; Type: SEQUENCE; Schema: public; Owner: faridl
--

CREATE SEQUENCE generateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE generateur_id_seq OWNER TO faridl;

--
-- Name: generateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: faridl
--

ALTER SEQUENCE generateur_id_seq OWNED BY client.id;


--
-- Name: hotel; Type: TABLE; Schema: public; Owner: faridl; Tablespace: 
--

CREATE TABLE hotel (
    id integer NOT NULL,
    nom character varying(100),
    code character(3)
);


ALTER TABLE hotel OWNER TO faridl;

--
-- Name: hotel_id_seq; Type: SEQUENCE; Schema: public; Owner: faridl
--

CREATE SEQUENCE hotel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotel_id_seq OWNER TO faridl;

--
-- Name: hotel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: faridl
--

ALTER SEQUENCE hotel_id_seq OWNED BY hotel.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: faridl
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('generateur_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: faridl
--

ALTER TABLE ONLY hotel ALTER COLUMN id SET DEFAULT nextval('hotel_id_seq'::regclass);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: faridl
--

COPY client (id, token, ttl, idhotel) FROM stdin;
\.


--
-- Name: generateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: faridl
--

SELECT pg_catalog.setval('generateur_id_seq', 1, false);


--
-- Data for Name: hotel; Type: TABLE DATA; Schema: public; Owner: faridl
--

COPY hotel (id, nom, code) FROM stdin;
1	Accor	ACC
2	Classe Premiere	CLP
3	Mercure	MER
4	B&B	BAB
\.


--
-- Name: hotel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: faridl
--

SELECT pg_catalog.setval('hotel_id_seq', 4, true);


--
-- Name: generateur_pkey; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT generateur_pkey PRIMARY KEY (id);


--
-- Name: generateur_token_key; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT generateur_token_key UNIQUE (token);


--
-- Name: hotel_code_key; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY hotel
    ADD CONSTRAINT hotel_code_key UNIQUE (code);


--
-- Name: hotel_nom_key; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY hotel
    ADD CONSTRAINT hotel_nom_key UNIQUE (nom);


--
-- Name: hotel_pkey; Type: CONSTRAINT; Schema: public; Owner: faridl; Tablespace: 
--

ALTER TABLE ONLY hotel
    ADD CONSTRAINT hotel_pkey PRIMARY KEY (id);


--
-- Name: idhotelfkey; Type: FK CONSTRAINT; Schema: public; Owner: faridl
--

ALTER TABLE ONLY client
    ADD CONSTRAINT idhotelfkey FOREIGN KEY (idhotel) REFERENCES hotel(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

