--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: email; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN email AS text
	CONSTRAINT email_check CHECK ((VALUE ~ '^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,}$'::text));


ALTER DOMAIN email OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: discussion; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE discussion (
    id integer NOT NULL,
    "intitulé" text NOT NULL,
    mdp character varying(255)
);


ALTER TABLE discussion OWNER TO quentinp;

--
-- Name: discussion_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE discussion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE discussion_id_seq OWNER TO quentinp;

--
-- Name: discussion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quentinp
--

ALTER SEQUENCE discussion_id_seq OWNED BY discussion.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    contenu text NOT NULL,
    date_message timestamp without time zone DEFAULT now() NOT NULL,
    id_discussion integer NOT NULL,
    id_utilisateur integer NOT NULL
);


ALTER TABLE message OWNER TO quentinp;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO quentinp;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quentinp
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: quentinp; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    pseudo character varying(20) NOT NULL,
    email email NOT NULL,
    mdp character varying(255) NOT NULL
);


ALTER TABLE utilisateur OWNER TO quentinp;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: quentinp
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO quentinp;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quentinp
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY discussion ALTER COLUMN id SET DEFAULT nextval('discussion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: discussion; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY discussion (id, "intitulé", mdp) FROM stdin;
1	General	\N
\.


--
-- Name: discussion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('discussion_id_seq', 1, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY message (id, contenu, date_message, id_discussion, id_utilisateur) FROM stdin;
117	test	2017-03-09 19:31:28.876489	1	55
118	salut	2017-03-09 22:02:29.312414	1	44
119	test	2017-03-10 08:32:14.168299	1	48
120	test	2017-03-10 08:32:36.107334	1	48
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('message_id_seq', 120, true);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: quentinp
--

COPY utilisateur (id, pseudo, email, mdp) FROM stdin;
44	Admin	Quentin.papp@outlook.fr	2218c2aafda6087ccc9d48c247f68c27
46	quentinb	yassine@ledechien.fr	ab4f63f9ac65152575886860dde480a1
47	Donut	azer.fr@yo.fr	7c37be7260f8cd7c1f5e4dbdd7bc5b23
48	Lorenzo	alex.monnier21@gmail.com	5ba59c03b234f96c63d5e26531bba3db
55	Gandalf	mordor@biteaucul.fr	ed735d55415bee976b771989be8f7005
56	efrfe	fjjgjirg.ff@dc.fr	74b87337454200d4d33f80c4663dc5e5
57	yassine	coucou@gmail.com	e10adc3949ba59abbe56e057f20f883e
58	test	joke@joke.fr	098f6bcd4621d373cade4e832627b4f6
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quentinp
--

SELECT pg_catalog.setval('utilisateur_id_seq', 59, true);


--
-- Name: discussion_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY discussion
    ADD CONSTRAINT discussion_pkey PRIMARY KEY (id);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_email_key; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_email_key UNIQUE (email);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_pseudo_key; Type: CONSTRAINT; Schema: public; Owner: quentinp; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pseudo_key UNIQUE (pseudo);


--
-- Name: message_id_discussion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_discussion_fkey FOREIGN KEY (id_discussion) REFERENCES discussion(id);


--
-- Name: message_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quentinp
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

