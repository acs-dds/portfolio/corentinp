--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: avis; Type: TABLE; Schema: public; Owner: api; Tablespace: 
--

CREATE TABLE avis (
    id integer NOT NULL,
    note_globale character varying(200),
    commentaire character varying(1000)
);


ALTER TABLE avis OWNER TO api;

--
-- Name: avis_id_seq; Type: SEQUENCE; Schema: public; Owner: api
--

CREATE SEQUENCE avis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE avis_id_seq OWNER TO api;

--
-- Name: avis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: api
--

ALTER SEQUENCE avis_id_seq OWNED BY avis.id;


--
-- Name: token; Type: TABLE; Schema: public; Owner: api; Tablespace: 
--

CREATE TABLE token (
    id integer NOT NULL,
    token character varying(50) NOT NULL,
    ttl interval NOT NULL
);


ALTER TABLE token OWNER TO api;

--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: api
--

CREATE SEQUENCE token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE token_id_seq OWNER TO api;

--
-- Name: token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: api
--

ALTER SEQUENCE token_id_seq OWNED BY token.id;


--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: api; Tablespace: 
--

CREATE TABLE utilisateur (
    id integer NOT NULL,
    entreprise character varying(70) NOT NULL,
    mdp character(128) NOT NULL
);


ALTER TABLE utilisateur OWNER TO api;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: api
--

CREATE SEQUENCE utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE utilisateur_id_seq OWNER TO api;

--
-- Name: utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: api
--

ALTER SEQUENCE utilisateur_id_seq OWNED BY utilisateur.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: api
--

ALTER TABLE ONLY avis ALTER COLUMN id SET DEFAULT nextval('avis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: api
--

ALTER TABLE ONLY token ALTER COLUMN id SET DEFAULT nextval('token_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: api
--

ALTER TABLE ONLY utilisateur ALTER COLUMN id SET DEFAULT nextval('utilisateur_id_seq'::regclass);


--
-- Data for Name: avis; Type: TABLE DATA; Schema: public; Owner: api
--

COPY avis (id, note_globale, commentaire) FROM stdin;
\.


--
-- Name: avis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api
--

SELECT pg_catalog.setval('avis_id_seq', 1, false);


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: api
--

COPY token (id, token, ttl) FROM stdin;
\.


--
-- Name: token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api
--

SELECT pg_catalog.setval('token_id_seq', 1, false);


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: api
--

COPY utilisateur (id, entreprise, mdp) FROM stdin;
\.


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api
--

SELECT pg_catalog.setval('utilisateur_id_seq', 1, false);


--
-- Name: id_client_key; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY avis
    ADD CONSTRAINT id_client_key PRIMARY KEY (id);


--
-- Name: token_pkey; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY token
    ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- Name: token_token_key; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY token
    ADD CONSTRAINT token_token_key UNIQUE (token);


--
-- Name: utilisateur_entreprise_key; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_entreprise_key UNIQUE (entreprise);


--
-- Name: utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

