--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client; Type: TABLE; Schema: public; Owner: api; Tablespace: 
--

CREATE TABLE client (
    id integer NOT NULL,
    num_client integer NOT NULL,
    nomclient character varying(50),
    datec date
);


ALTER TABLE client OWNER TO api;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: api
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO api;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: api
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- Name: hotels; Type: TABLE; Schema: public; Owner: api; Tablespace: 
--

CREATE TABLE hotels (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    status character varying(7) NOT NULL,
    offline_token character varying(3) NOT NULL,
    token character varying(3) NOT NULL
);


ALTER TABLE hotels OWNER TO api;

--
-- Name: hotels_id_seq; Type: SEQUENCE; Schema: public; Owner: api
--

CREATE SEQUENCE hotels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotels_id_seq OWNER TO api;

--
-- Name: hotels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: api
--

ALTER SEQUENCE hotels_id_seq OWNED BY hotels.id;


--
-- Name: token; Type: TABLE; Schema: public; Owner: api; Tablespace: 
--

CREATE TABLE token (
    id integer NOT NULL,
    token character varying(50) NOT NULL,
    ttl interval NOT NULL
);


ALTER TABLE token OWNER TO api;

--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: api
--

CREATE SEQUENCE token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE token_id_seq OWNER TO api;

--
-- Name: token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: api
--

ALTER SEQUENCE token_id_seq OWNED BY token.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: api
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: api
--

ALTER TABLE ONLY hotels ALTER COLUMN id SET DEFAULT nextval('hotels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: api
--

ALTER TABLE ONLY token ALTER COLUMN id SET DEFAULT nextval('token_id_seq'::regclass);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: api
--

COPY client (id, num_client, nomclient, datec) FROM stdin;
\.


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api
--

SELECT pg_catalog.setval('client_id_seq', 1, false);


--
-- Data for Name: hotels; Type: TABLE DATA; Schema: public; Owner: api
--

COPY hotels (id, name, status, offline_token, token) FROM stdin;
1	Hotel A	online	yRw	HVQ
2	Hotel B	offline	QOm	RNi
3	Hotel C	online	nKd	8UI
4	Hotel D	offline	baE	1G9
5	Hotel E	online	jhy	5fp
\.


--
-- Name: hotels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api
--

SELECT pg_catalog.setval('hotels_id_seq', 5, true);


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: api
--

COPY token (id, token, ttl) FROM stdin;
1	HVQts9KHnv5DafV	413123:29:54
\.


--
-- Name: token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api
--

SELECT pg_catalog.setval('token_id_seq', 1, true);


--
-- Name: hotels_name_key; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY hotels
    ADD CONSTRAINT hotels_name_key UNIQUE (name);


--
-- Name: hotels_offline_token_key; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY hotels
    ADD CONSTRAINT hotels_offline_token_key UNIQUE (offline_token);


--
-- Name: hotels_pkey; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY hotels
    ADD CONSTRAINT hotels_pkey PRIMARY KEY (id);


--
-- Name: hotels_token_key; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY hotels
    ADD CONSTRAINT hotels_token_key UNIQUE (token);


--
-- Name: num_client_key; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT num_client_key PRIMARY KEY (id);


--
-- Name: token_pkey; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY token
    ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- Name: token_token_key; Type: CONSTRAINT; Schema: public; Owner: api; Tablespace: 
--

ALTER TABLE ONLY token
    ADD CONSTRAINT token_token_key UNIQUE (token);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

