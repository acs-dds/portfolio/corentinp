--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: rml; Type: COMMENT; Schema: -; Owner: walidb
--

COMMENT ON DATABASE rml IS 'Table pour le RML ';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: commande; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE commande (
    id integer NOT NULL,
    plat_choisi character varying(70)
);


ALTER TABLE commande OWNER TO zohral;

--
-- Name: commande_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE commande_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE commande_id_seq OWNER TO zohral;

--
-- Name: commande_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE commande_id_seq OWNED BY commande.id;


--
-- Name: ingredients; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE ingredients (
    id integer NOT NULL,
    nom_ingredient character varying(50) NOT NULL,
    quantite integer NOT NULL
);


ALTER TABLE ingredients OWNER TO zohral;

--
-- Name: ingredients_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE ingredients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ingredients_id_seq OWNER TO zohral;

--
-- Name: ingredients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE ingredients_id_seq OWNED BY ingredients.id;


--
-- Name: plat; Type: TABLE; Schema: public; Owner: zohral; Tablespace: 
--

CREATE TABLE plat (
    id integer NOT NULL,
    nom_plat character varying(70)
);


ALTER TABLE plat OWNER TO zohral;

--
-- Name: plat_id_seq; Type: SEQUENCE; Schema: public; Owner: zohral
--

CREATE SEQUENCE plat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plat_id_seq OWNER TO zohral;

--
-- Name: plat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: zohral
--

ALTER SEQUENCE plat_id_seq OWNED BY plat.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY commande ALTER COLUMN id SET DEFAULT nextval('commande_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY ingredients ALTER COLUMN id SET DEFAULT nextval('ingredients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: zohral
--

ALTER TABLE ONLY plat ALTER COLUMN id SET DEFAULT nextval('plat_id_seq'::regclass);


--
-- Data for Name: commande; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY commande (id, plat_choisi) FROM stdin;
\.


--
-- Name: commande_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('commande_id_seq', 1, false);


--
-- Data for Name: ingredients; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY ingredients (id, nom_ingredient, quantite) FROM stdin;
1	poisson	21
2	riz	50
3	pate	500
4	aricots vert	500
6	tomate	500
7	carotte	250
8	pomme de terre	500
9	oignons	200
12	ails	100
15	artichauds	500
16	petits pois	250
17	olives verte	100
18	olive noire	100
19	concombre	2
20	escalope de veau	3
21	ingrédient mystère	4
\.


--
-- Name: ingredients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('ingredients_id_seq', 21, true);


--
-- Data for Name: plat; Type: TABLE DATA; Schema: public; Owner: zohral
--

COPY plat (id, nom_plat) FROM stdin;
1	gratin dophinois
2	moussaka
3	couscous
4	poisson braisé
5	escalope de veau
8	choukroute
\.


--
-- Name: plat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: zohral
--

SELECT pg_catalog.setval('plat_id_seq', 8, true);


--
-- Name: ingredients_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_pkey PRIMARY KEY (id);


--
-- Name: nom_ingredient_pkey; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT nom_ingredient_pkey UNIQUE (nom_ingredient);


--
-- Name: nom_plat_key; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY plat
    ADD CONSTRAINT nom_plat_key UNIQUE (nom_plat);


--
-- Name: plat_choisi_key; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY commande
    ADD CONSTRAINT plat_choisi_key PRIMARY KEY (id);


--
-- Name: plat_key; Type: CONSTRAINT; Schema: public; Owner: zohral; Tablespace: 
--

ALTER TABLE ONLY plat
    ADD CONSTRAINT plat_key PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

