--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: news; Type: TABLE; Schema: public; Owner: edouardb; Tablespace: 
--

CREATE TABLE news (
    id integer NOT NULL,
    title character varying(128) NOT NULL,
    slug character varying(128) NOT NULL,
    text text NOT NULL
);


ALTER TABLE news OWNER TO edouardb;

--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: edouardb
--

CREATE SEQUENCE news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE news_id_seq OWNER TO edouardb;

--
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edouardb
--

ALTER SEQUENCE news_id_seq OWNED BY news.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: edouardb
--

ALTER TABLE ONLY news ALTER COLUMN id SET DEFAULT nextval('news_id_seq'::regclass);


--
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: edouardb
--

COPY news (id, title, slug, text) FROM stdin;
1	ede	ede	coucou
\.


--
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edouardb
--

SELECT pg_catalog.setval('news_id_seq', 1, true);


--
-- Name: news_pkey; Type: CONSTRAINT; Schema: public; Owner: edouardb; Tablespace: 
--

ALTER TABLE ONLY news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

