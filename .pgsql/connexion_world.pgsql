--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: connexion_world; Type: COMMENT; Schema: -; Owner: connexion_world
--

COMMENT ON DATABASE connexion_world IS 'Table pour les messages';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: messages; Type: TABLE; Schema: public; Owner: connexion_world; Tablespace: 
--

CREATE TABLE messages (
    id integer NOT NULL,
    pseudo character varying(30) NOT NULL,
    messages character varying(155) NOT NULL
);


ALTER TABLE messages OWNER TO connexion_world;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: connexion_world
--

CREATE SEQUENCE messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE messages_id_seq OWNER TO connexion_world;

--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: connexion_world
--

ALTER SEQUENCE messages_id_seq OWNED BY messages.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: connexion_world
--

ALTER TABLE ONLY messages ALTER COLUMN id SET DEFAULT nextval('messages_id_seq'::regclass);


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: connexion_world
--

COPY messages (id, pseudo, messages) FROM stdin;
1085	Guerrin	Bonsoir
1086	Waloup	Je vous souhaite la bienvenue dans votre nouvelle univers :)
1087	boussad	Bonsoir
1088	boussad	ça marche bien dites moi :D 
1089	Revo	Bonsoir , Ce n'es que la version 0 pour le moment :) 
1341	edouard	bonjour
1342	edouard	ça va ?
1308	Revo	Nouveau test
1311	<b>revo	Essai inject
1316	Test	Mobile
1343	test	test
1344	test	test
1345	yoyohko@gmail.com	raze
\.


--
-- Name: messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: connexion_world
--

SELECT pg_catalog.setval('messages_id_seq', 1345, true);


--
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: connexion_world; Tablespace: 
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: messages; Type: ACL; Schema: public; Owner: connexion_world
--

REVOKE ALL ON TABLE messages FROM PUBLIC;
REVOKE ALL ON TABLE messages FROM connexion_world;
GRANT ALL ON TABLE messages TO connexion_world;
GRANT ALL ON TABLE messages TO yassinel;


--
-- PostgreSQL database dump complete
--

