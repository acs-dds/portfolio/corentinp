--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: codetchat; Type: COMMENT; Schema: -; Owner: walidb
--

COMMENT ON DATABASE codetchat IS 'Base de donnés ( olivier ).';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: message; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE message (
    id integer NOT NULL,
    message character varying(255) NOT NULL,
    temps_message timestamp without time zone DEFAULT now()
);


ALTER TABLE message OWNER TO walidb;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO walidb;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- Name: register; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE register (
    id integer NOT NULL,
    pseudo character varying(30) NOT NULL,
    mdp character varying(20) NOT NULL,
    email text NOT NULL,
    message_time timestamp without time zone DEFAULT now()
);


ALTER TABLE register OWNER TO walidb;

--
-- Name: register_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE register_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE register_id_seq OWNER TO walidb;

--
-- Name: register_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE register_id_seq OWNED BY register.id;


--
-- Name: salon; Type: TABLE; Schema: public; Owner: walidb; Tablespace: 
--

CREATE TABLE salon (
    id integer NOT NULL,
    pass character varying(30) NOT NULL,
    salon_name character varying(50) NOT NULL
);


ALTER TABLE salon OWNER TO walidb;

--
-- Name: salon_id_seq; Type: SEQUENCE; Schema: public; Owner: walidb
--

CREATE SEQUENCE salon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE salon_id_seq OWNER TO walidb;

--
-- Name: salon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: walidb
--

ALTER SEQUENCE salon_id_seq OWNED BY salon.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY register ALTER COLUMN id SET DEFAULT nextval('register_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: walidb
--

ALTER TABLE ONLY salon ALTER COLUMN id SET DEFAULT nextval('salon_id_seq'::regclass);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY message (id, message, temps_message) FROM stdin;
1	Bienvenue à access Code school	2017-03-06 10:28:46.752115
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('message_id_seq', 1, true);


--
-- Data for Name: register; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY register (id, pseudo, mdp, email, message_time) FROM stdin;
1	Olivier	123456	test@gmail.com	2017-03-06 10:31:06.329951
\.


--
-- Name: register_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('register_id_seq', 1, true);


--
-- Data for Name: salon; Type: TABLE DATA; Schema: public; Owner: walidb
--

COPY salon (id, pass, salon_name) FROM stdin;
1	123	General
\.


--
-- Name: salon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: walidb
--

SELECT pg_catalog.setval('salon_id_seq', 1, true);


--
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: register_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY register
    ADD CONSTRAINT register_pkey PRIMARY KEY (id);


--
-- Name: salon_pkey; Type: CONSTRAINT; Schema: public; Owner: walidb; Tablespace: 
--

ALTER TABLE ONLY salon
    ADD CONSTRAINT salon_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

