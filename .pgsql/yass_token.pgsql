--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: token; Type: TABLE; Schema: public; Owner: yassinel; Tablespace: 
--

CREATE TABLE token (
    id integer NOT NULL,
    name_token character varying(255)
);


ALTER TABLE token OWNER TO yassinel;

--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: yassinel
--

CREATE SEQUENCE token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE token_id_seq OWNER TO yassinel;

--
-- Name: token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: yassinel
--

ALTER SEQUENCE token_id_seq OWNED BY token.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: yassinel
--

ALTER TABLE ONLY token ALTER COLUMN id SET DEFAULT nextval('token_id_seq'::regclass);


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: yassinel
--

COPY token (id, name_token) FROM stdin;
1	coucou
2	.$token.
3	9tcklee7tf0o0gscck4kcw48k
4	9asaw5cv52os884gk0s8g8ckg
5	bnqzrt7d49c8ks8440c0kgo0k
6	8qaggqcwno08kkkcg0o4ksows
7	7vg31lgrtz404g4o4kw80co8s
8	8cu40dozob48ks48c084kwcc
9	bmz2mckdpu04kog4w0sko0go0
10	a93h3ywszkg8w04o4w4goowco
11	cktemiqny3kkw8gkwwksco8ck
12	5s4flh1o40kc0scgwgsok0cgg
13	a3yb1fbzgo0g8844so0wgog0w
14	c927hen2c3s4sw4kcwc0ss84s
15	3qiommw3vjeokkkc84sowgwko
16	93bfo1u5d008w4kckcoo8o8ww
17	ei16kxd10wg8kgc4occcks0gc
18	477ijfxss6skcoks8cso4wo8k
19	2jifjtcpn2g4k4sowokgwkgw8
20	di8wg1075e88w0os0w0c40gw4
21	32j5o4scz3msgg80004ow4k40
22	5rzpu1befwo4kw404sowcgscc
\.


--
-- Name: token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: yassinel
--

SELECT pg_catalog.setval('token_id_seq', 22, true);


--
-- Name: token_pkey; Type: CONSTRAINT; Schema: public; Owner: yassinel; Tablespace: 
--

ALTER TABLE ONLY token
    ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

