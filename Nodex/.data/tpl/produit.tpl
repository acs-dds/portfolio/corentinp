<tr>
	<td><?=$this->ref;?></td>
	<td><?=$this->mat;?></td>
<?php if(isset($this->pl)) : ?>
	<td>
		<input type="number" name="longueur" style="width:45px" min="0" step="<?=$this->inc;?>" value="<?=$this->long;?>" /> x 
		<input type="number" name="largeur" style="width:45px" min="0" step="<?=$this->inc;?>" value="<?=$this->larg;?>" />
	</td>
<?php else : ?>
	<td><?=$this->long . " x " . $this->larg;?></td>
<?php endif;?>
	<td>
		<select name="epaisseur">
<?php foreach($this->ep as $epaisseur) : ?>
			<option value="<?=$epaisseur;?>"><?=$epaisseur;?></option>
<?php endforeach; ?>
		</select>
	</td>
	<td>
		<input type="hidden" name="reference" value="<?=$this->ref;?>" />
		<input type="hidden" name="matiere" value="<?=$this->mat;?>" />

		<input type="submit" value="Découper" />
	</td>
</tr>