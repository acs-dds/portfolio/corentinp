<!DOCTYPE html>
<html>
	<head>
		<title>Mon Simon</title>

		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito">

		<style type="text/css">
			::selection{
				color:gold;
				background-color:red;
				font-weight:bold;
			}
			html,
			button{
				font-family:"Nunito";
			}
			button {
				width: 250px;
				height: 250px;
				display: inline-block;
				padding: 0;
				background-color: #fff;
				outline: none;
				font-size: 20px;
			}
			#lv1{width:504px}
			#lv2,#lv3,#lv4{width:758px}
			#lv1,#lv2,#lv3,#lv4{margin:auto;padding:4% 1%;text-align:center;}
			span.pushed {
				/*background-color: #de5214;*/
	/*			display: none;*/
				width: 100%;
				height: 100%;
				opacity: .5;
			}
			button#btn1{
				background-color: #de5214;
			}
			button#btn2/* span.pushed */{
				background-color: #52de14;
			}
			button#btn3/* span.pushed */{
				background-color: #5214de;
			}
			button#btn4,
			button#btn5,
			button#btn6,
			button#btn7,
			button#btn8,
			button#btn9,
			button#btn10,
			button#btn11,
			button#btn12,
			button#btn13,
			button#btn14,
			button#btn15,
			button#btn16,
			button#btn17,
			button#btn18,
			button#btn19{
				background-color: #dede52;
			}

			button#btn20,
			button#btn21,
			button#btn22,
			button#btn23,
			button#btn24,
			button#btn25,
			button#btn26,
			button#btn27,
			button#btn28{
				background-color: red;
			}

			button#btn1 span.pushed,
			button#btn2 span.pushed,
			button#btn3 span.pushed,
			button#btn4 span.pushed,
			button#btn5 span.pushed,
			button#btn6 span.pushed,
			button#btn7 span.pushed,
			button#btn8 span.pushed,
			button#btn9 span.pushed,
			button#btn10 span.pushed,
			button#btn11 span.pushed,
			button#btn12 span.pushed,
			button#btn13 span.pushed,
			button#btn14 span.pushed,
			button#btn15 span.pushed,
			button#btn16 span.pushed,
			button#btn17 span.pushed,
			button#btn18 span.pushed,
			button#btn19 span.pushed,
			button#btn20 span.pushed,
			button#btn21 span.pushed,
			button#btn22 span.pushed,
			button#btn23 span.pushed,
			button#btn24 span.pushed,
			button#btn25 span.pushed,
			button#btn26 span.pushed,
			button#btn27 span.pushed,
			button#btn28 span.pushed{
				background-color: white;
				padding:20%;
			}
			button:active span.pushed, button.actif span.pushed {
				/*display: block;*/
				background-color:white;
				opacity:1;
			}
			output{
				position: fixed;
				background:rgba(0,0,0,0.65) 72%;
				width: 80%;
				height:20%;
				padding: 10px;
				left: 20%;
				bottom:0;
				text-align:center;
				/*z-index:2;*/
				border-top:4px solid black;
				line-height:200px;
				font-size:80px;
				color:white;
				text-shadow:4px 4px black;
			}
			#btn6,
			#btn6 span,
			#btn9,
			#btn9 span,
			#btn12,
			#btn12 span,
			#btn14,
			#btn14 span,
			#btn15,
			#btn15 span,
			#btn16,
			#btn16 span,
			#btn18,
			#btn18 span,
			#btn21,
			#btn21 span,
			#btn23,
			#btn23 span,
			#btn24,
			#btn24 span,
			#btn25,
			#btn25 span,
			#btn27,
			#btn27 span{
				border-radius:30%;
			}
			#btn1,#btn2,#btn3,#btn4{border:1px solid black;}
			#btn1,
			#btn5,
			#btn5 span,
			#btn1 span,
			#btn11,
			#btn11 span,
			#btn20,
			#btn20 span{
				border-radius:50% 10%;
			}
			#btn2,
			#btn2 span,
			#btn7,
			#btn7 span,
			#btn13,
			#btn13 span,
			#btn26,
			#btn26 span{
				border-radius:10% 50%;
			}
			#btn3,
			#btn3 span,
			#btn8,
			#btn8 span,
			#btn17,
			#btn17 span,
			#btn22,
			#btn22 span{
				border-radius:10% 50%;
			}
			#btn4,
			#btn4 span,
			#btn10,
			#btn10 span,
			#btn19,
			#btn19 span,
			#btn28,
			#btn28 span{
				border-radius:50% 10%;
			}
			#menu{
				background:grey;
				background:-webkit-linear-gradient(top, rgba(0,0,0,0) 0%, rgba(0,0,0,0.65) 72%);
				position:fixed;
				top:0;
				bottom:0;
				left:0;
				width:20%;
				border-right:4px solid black;
				/*z-index:1;*/
			}
			#levels{padding:4%;padding-top:90px;text-align:center;list-style:none}
			#start{padding:50% 4%;text-align:center;}
			#levels button,
			#start button{
				width:60%;
				height:initial;
				padding:5%;
				margin-bottom:5%;
				/*border:2px solid #9C27B0;*/
			}
			#levels button{
				border-radius:30% 100%;
			}
			#start button{
				border-radius:100% 30%;
			}
			#levels button:last-child{
				margin-bottom:0;
			}
			#levels2{
				background:grey;
				background:-webkit-linear-gradient(top, rgba(0,0,0,0) 0%, rgba(0,0,0,0.65) 72%);
				position:fixed;
				top:0;
				bottom:22%;
				left:20%;
				right:0;
				font-family:initial;
			}
			.highlight{
				position:absolute;
				left:86px;
				width:212px;
				height:64px;
				background:blue;
				z-index: 0;
				opacity: 0.4;
				display: none;
				border-radius:30% 100%;
			}
		</style>
	</head>
	<body onload="demo.init();">
		<script type="text/javascript">
			const SETTINGS = {
				rebound:{
					tension:11,
					friction:5
				},
				spinner:{
					id:"spinner",
					radius:90,
					sides:3,
					depth:10,
					colors:{
						/*background:"#231E76",
						stroke:"#231E76",*/
						background:"#f0f0f0",
						stroke:"#272633",
						base:null,
						child:"#272633"
					},
					alwaysForward:true, // When false the spring will reverse normally.
					restAt:0.5, // A number from 0.1 to 0.9 || null for full rotation
					renderBase:false
				}
			};
		</script>
		<section id="menu">
			<div class="highlight"></div>
			<section id="levels">
				<li><button id="lvl1">Level 1</button></li>
				<li><button id="lvl2">Level 2</button></li>
				<li><button id="lvl3">Level 3</button></li>
				<li><button id="lvl4">Level 4</button></li>
			</section>
			<section id="start">
				<button id="start2">Start</button>
			</section>
		</section>
		<section id="levels2">
			<section id="lv1" style="display:none">
				<button id="btn1"><span class="pushed"></span></button>
				<button id="btn2"><span class="pushed"></span></button>
				<button id="btn3"><span class="pushed"></span></button>
				<button id="btn4"><span class="pushed"></span></button>
			</section>
			<section id="lv2" style="display:none">
				<button id="btn5"><span class="pushed"></span></button>
				<button id="btn6"><span class="pushed"></span></button>
				<button id="btn7"><span class="pushed"></span></button>
				<button id="btn8"><span class="pushed"></span></button>
				<button id="btn9"><span class="pushed"></span></button>
				<button id="btn10"><span class="pushed"></span></button>
			</section>
			<section id="lv3" style="display:none">
				<button id="btn11"><span class="pushed"></span></button>
				<button id="btn12"><span class="pushed"></span></button>
				<button id="btn13"><span class="pushed"></span></button>
				<button id="btn14"><span class="pushed"></span></button>
				<button id="btn15"><span class="pushed"></span></button>
				<button id="btn16"><span class="pushed"></span></button>
				<button id="btn17"><span class="pushed"></span></button>
				<button id="btn18"><span class="pushed"></span></button>
				<button id="btn19"><span class="pushed"></span></button>
			</section>
			<section id="lv4" style="display:none">
				<button id="btn20"><span class="pushed"></span></button>
				<button id="btn21"><span class="pushed"></span></button>
				<button id="btn22"><span class="pushed"></span></button>
				<button id="btn23"><span class="pushed"></span></button>
				<button id="btn24"><span class="pushed"></span></button>
				<button id="btn25"><span class="pushed"></span></button>
				<button id="btn26"><span class="pushed"></span></button>
				<button id="btn27"><span class="pushed"></span></button>
				<button id="btn28"><span class="pushed"></span></button>
			</section>
		</section>
		<output id="output"></output>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/3.1.1/jquery.min.js"></script>
		<script type="text/javascript">
			var boutons = [document.getElementById("btn1"), document.getElementById("btn2"), document.getElementById("btn3"), document.getElementById("btn4")];
			var boutons2 = [document.getElementById("btn5"), document.getElementById("btn6"), document.getElementById("btn7"), document.getElementById("btn8"),document.getElementById("btn9"),document.getElementById("btn10")];
			var boutons3 = [document.getElementById("btn11"), document.getElementById("btn12"), document.getElementById("btn13"), document.getElementById("btn14"),document.getElementById("btn15"),document.getElementById("btn16"),document.getElementById("btn17"), document.getElementById("btn18"), document.getElementById("btn19")];
			var boutons4 = [document.getElementById("btn20"),document.getElementById("btn21"),document.getElementById("btn22"),document.getElementById("btn23"),document.getElementById("btn24"),document.getElementById("btn25"),document.getElementById("btn26"),document.getElementById("btn27"),document.getElementById("btn28")];
			var sequence = [];

			$(function(){
				$("li").click(function(){
					$(".highlight").animate({"top":(90 + $(this).index() * 66) + "px"},function(){
						$(".highlight").css({"display":"block"});
					});
				});
				$("#btn1").click(function(){
					if($("#btn1 span.pushed").css("padding") == "40%"){
						$("#btn1 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn1 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn1 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn1 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn2").click(function(){
					if($("#btn2 span.pushed").css("padding") == "40%"){
						$("#btn2 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn2 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn2 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn2 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn3").click(function(){
					if($("#btn3 span.pushed").css("padding") == "40%"){
						$("#btn3 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn3 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn3 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn3 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn4").click(function(){
					if($("#btn4 span.pushed").css("padding") == "40%"){
						$("#btn4 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn4 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn4 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn4 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn5").click(function(){
					if($("#btn5 span.pushed").css("padding") == "40%"){
						$("#btn5 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn5 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn5 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn5 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn6").click(function(){
					if($("#btn6 span.pushed").css("padding") == "40%"){
						$("#btn6 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn6 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn6 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn6 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn7").click(function(){
					if($("#btn7 span.pushed").css("padding") == "40%"){
						$("#btn7 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn7 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn7 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn7 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn8").click(function(){
					if($("#btn8 span.pushed").css("padding") == "40%"){
						$("#btn8 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn8 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn8 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn8 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn9").click(function(){
					if($("#btn9 span.pushed").css("padding") == "40%"){
						$("#btn9 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn9 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn9 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn9 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
				$("#btn10").click(function(){
					if($("#btn10 span.pushed").css("padding") == "40%"){
						$("#btn10 span.pushed").animate({"padding":20 + "%"},function(){
							$("#btn10 span.pushed").css({"padding":20 + "%"});
						});
					}
					else{
						$("#btn10 span.pushed").animate({"padding":40 + "%"},function(){
							$("#btn10 span.pushed").css({"padding":40 + "%"});
						});
					}
				});
			});

			function lv1(){
				document.getElementById("lv1").style.display = "block";
				document.getElementById("lv2").style.display = "none";
				document.getElementById("lv3").style.display = "none";
				document.getElementById("lv4").style.display = "none";
				// 0 : initialisation
				for (var i = 0; i < 4; i++) {
					sequence.push(boutons[Math.floor(Math.random() * 4)]);
				}
				
				console.log("Memorisez la sequence");
				document.getElementById("output").innerHTML = "Memorisez la sequence";
				// 1 : montrer au joueur les boutons choisis
				var courant = 0;
				function allumer() {

					function eteindre(bouton_a_eteindre) {
						bouton_a_eteindre.classList.remove('actif');
					}

					// allumer le bouton
					sequence[courant].classList.add('actif');

					// différer l'extinction de 600ms
					setTimeout(eteindre, 600, sequence[courant]);

					// différer l'allumage du bouton suivant dans 700ms
					courant++;
					if (courant < sequence.length) {
						setTimeout(allumer, 700);
					} else {
						// quand la séquence est finie, on laisse le joueur jouer
						tourJoueur();
					}
				}


				function tourJoueur() {
					console.log("A vous !");
					document.getElementById("output").innerHTML = "A vous !";
					courant = 0;

					// au bout de cinq secondes, gameover
					var to = setTimeout(gameover, 5000);

					// écoutons les clics
					for (var i = 0; i < boutons.length; i++) {
						boutons[i].addEventListener("click", verif);
					}

					function gameover() {
						// ne pas oublier de retirer les écouteurs de clic
						console.log('Temps écoulé');
						document.getElementById("output").innerHTML = 'Temps écoulé';
						for (var i = 0; i < boutons.length; i++) {
							boutons[i].removeEventListener("click", verif);
						}
					}

					// comparer avec la séquence
					function verif() {
						// dans tous les cas, on retire le timeout qui lance le gameover à 5s
						clearTimeout(to);
						// this désigne le bouton cliqué
						if (this != sequence[courant]) {
							// perdu
							sequence = [];
							courant = 0;
							console.log("Perdu !");
							document.getElementById("output").innerHTML = "Perdu !";
							location.reload();
							// on retire les listeners
							for (var i = 0; i < boutons.length; i++) {
								boutons[i].removeEventListener("click", verif);
							}
						} else {
							// bon bouton
							courant++;
							if (courant < sequence.length) {
								// s'il reste des boutons à cliquer, c'est tout ce qu'il y a à faire, les boutons continuent d'écouter, le tour du joueur continue
								to = setTimeout(gameover, 5000);
							} else {
								// round gagné
								// on ajoute un bouton à la séquence
								sequence.push(boutons[Math.floor(Math.random() * 4)]);
								courant = 0;
								setTimeout(allumer, 300);
								// on retire les listeners, c'est à l'ordinateur de montrer la séquence
								for (var i = 0; i < boutons.length; i++) {
									boutons[i].removeEventListener("click", verif);
								}
							}
						}
					}
				}

				document.getElementById("start2").addEventListener("click",allumer);

				document.getElementById("lvl2").removeEventListener("click",lv2);
				document.getElementById("lvl3").removeEventListener("click",lv3);
				document.getElementById("lvl4").removeEventListener("click",lv4);
			}

			function lv2(){
				document.getElementById("lv1").style.display = "none";
				document.getElementById("lv2").style.display = "block";
				document.getElementById("lv3").style.display = "none";
				document.getElementById("lv4").style.display = "none";
				// 0 : initialisation
				for (var i = 0; i < 4; i++) {
					sequence.push(boutons2[Math.floor(Math.random() * 4)]);
				}
				
				console.log("Memorisez la sequence");
				document.getElementById("output").innerHTML = "Memorisez la sequence";
				// 1 : montrer au joueur les boutons choisis
				var courant = 0;
				function allumer() {

					function eteindre(bouton_a_eteindre) {
						bouton_a_eteindre.classList.remove('actif');
					}

					// allumer le bouton
					sequence[courant].classList.add('actif');

					// différer l'extinction de 600ms
					setTimeout(eteindre, 600, sequence[courant]);

					// différer l'allumage du bouton suivant dans 700ms
					courant++;
					if (courant < sequence.length) {
						setTimeout(allumer, 700);
					} else {
						// quand la séquence est finie, on laisse le joueur jouer
						tourJoueur();
					}
				}


				function tourJoueur() {
					console.log("A vous !");
					document.getElementById("output").innerHTML = "A vous !";
					courant = 0;

					// au bout de cinq secondes, gameover
					var to = setTimeout(gameover, 5000);

					// écoutons les clics
					for (var i = 0; i < boutons.length; i++) {
						boutons2[i].addEventListener("click", verif);
					}

					function gameover() {
						// ne pas oublier de retirer les écouteurs de clic
						console.log('Temps écoulé');
						document.getElementById("output").innerHTML = 'Temps écoulé';
						for (var i = 0; i < boutons2.length; i++) {
							boutons2[i].removeEventListener("click", verif);
						}
					}

					// comparer avec la séquence
					function verif() {
						// dans tous les cas, on retire le timeout qui lance le gameover à 5s
						clearTimeout(to);
						// this désigne le bouton cliqué
						if (this != sequence[courant]) {
							// perdu
							sequence = [];
							courant = 0;
							console.log("Perdu !");
							document.getElementById("output").innerHTML = "Perdu !";
							location.reload();
							// on retire les listeners
							for (var i = 0; i < boutons2.length; i++) {
								boutons2[i].removeEventListener("click", verif);
							}
						} else {
							// bon bouton
							courant++;
							if (courant < sequence.length) {
								// s'il reste des boutons à cliquer, c'est tout ce qu'il y a à faire, les boutons continuent d'écouter, le tour du joueur continue
								to = setTimeout(gameover, 5000);
							} else {
								// round gagné
								// on ajoute un bouton à la séquence
								sequence.push(boutons[Math.floor(Math.random() * 4)]);
								courant = 0;
								setTimeout(allumer, 300);
								// on retire les listeners, c'est à l'ordinateur de montrer la séquence
								for (var i = 0; i < boutons2.length; i++) {
									boutons2[i].removeEventListener("click", verif);
								}
							}
						}
					}
				}

				document.getElementById("start2").addEventListener("click",allumer);

				document.getElementById("lvl1").removeEventListener("click",lv1);
				document.getElementById("lvl3").removeEventListener("click",lv3);
				document.getElementById("lvl4").removeEventListener("click",lv4);
			}

			function lv3(){
				document.getElementById("lv1").style.display = "none";
				document.getElementById("lv2").style.display = "none";
				document.getElementById("lv3").style.display = "block";
				document.getElementById("lv4").style.display = "none";
				// 0 : initialisation
				for (var i = 0; i < 4; i++) {
					sequence.push(boutons3[Math.floor(Math.random() * 4)]);
				}
				
				console.log("Memorisez la sequence");
				document.getElementById("output").innerHTML = "Memorisez la sequence";
				// 1 : montrer au joueur les boutons choisis
				var courant = 0;
				function allumer() {

					function eteindre(bouton_a_eteindre) {
						bouton_a_eteindre.classList.remove('actif');
					}

					// allumer le bouton
					sequence[courant].classList.add('actif');

					// différer l'extinction de 600ms
					setTimeout(eteindre, 600, sequence[courant]);

					// différer l'allumage du bouton suivant dans 700ms
					courant++;
					if (courant < sequence.length) {
						setTimeout(allumer, 700);
					} else {
						// quand la séquence est finie, on laisse le joueur jouer
						tourJoueur();
					}
				}


				function tourJoueur() {
					console.log("A vous !");
					document.getElementById("output").innerHTML = "A vous !";
					courant = 0;

					// au bout de cinq secondes, gameover
					var to = setTimeout(gameover, 5000);

					// écoutons les clics
					for (var i = 0; i < boutons3.length; i++) {
						boutons3[i].addEventListener("click", verif);
					}

					function gameover() {
						// ne pas oublier de retirer les écouteurs de clic
						console.log('Temps écoulé');
						document.getElementById("output").innerHTML = 'Temps écoulé';
						for (var i = 0; i < boutons3.length; i++) {
							boutons3[i].removeEventListener("click", verif);
						}
					}

					// comparer avec la séquence
					function verif() {
						// dans tous les cas, on retire le timeout qui lance le gameover à 5s
						clearTimeout(to);
						// this désigne le bouton cliqué
						if (this != sequence[courant]) {
							// perdu
							sequence = [];
							courant = 0;
							console.log("Perdu !");
							document.getElementById("output").innerHTML = "Perdu !";
							location.reload();
							// on retire les listeners
							for (var i = 0; i < boutons3.length; i++) {
								boutons3[i].removeEventListener("click", verif);
							}
						} else {
							// bon bouton
							courant++;
							if (courant < sequence.length) {
								// s'il reste des boutons à cliquer, c'est tout ce qu'il y a à faire, les boutons continuent d'écouter, le tour du joueur continue
								to = setTimeout(gameover, 5000);
							} else {
								// round gagné
								// on ajoute un bouton à la séquence
								sequence.push(boutons3[Math.floor(Math.random() * 4)]);
								courant = 0;
								setTimeout(allumer, 300);
								// on retire les listeners, c'est à l'ordinateur de montrer la séquence
								for (var i = 0; i < boutons3.length; i++) {
									boutons3[i].removeEventListener("click", verif);
								}
							}
						}
					}
				}

				document.getElementById("start2").addEventListener("click",allumer);

				document.getElementById("lvl1").removeEventListener("click",lv1);
				document.getElementById("lvl2").removeEventListener("click",lv2);
				document.getElementById("lvl4").removeEventListener("click",lv4);
			}

			function lv4(){
				document.getElementById("lv1").style.display = "none";
				document.getElementById("lv2").style.display = "none";
				document.getElementById("lv3").style.display = "none";
				document.getElementById("lv4").style.display = "block";
				// 0 : initialisation
				for (var i = 0; i < 4; i++) {
					sequence.push(boutons4[Math.floor(Math.random() * 4)]);
				}
				
				console.log("Memorisez la sequence");
				document.getElementById("output").innerHTML = "Memorisez la sequence";
				// 1 : montrer au joueur les boutons choisis
				var courant = 0;
				function allumer() {

					function eteindre(bouton_a_eteindre) {
						bouton_a_eteindre.classList.remove('actif');
					}

					// allumer le bouton
					sequence[courant].classList.add('actif');

					// différer l'extinction de 600ms
					setTimeout(eteindre, 600, sequence[courant]);

					// différer l'allumage du bouton suivant dans 700ms
					courant++;
					if (courant < sequence.length) {
						setTimeout(allumer, 700);
					} else {
						// quand la séquence est finie, on laisse le joueur jouer
						tourJoueur();
					}
				}


				function tourJoueur() {
					console.log("A vous !");
					document.getElementById("output").innerHTML = "A vous !";
					courant = 0;

					// au bout de cinq secondes, gameover
					var to = setTimeout(gameover, 5000);

					// écoutons les clics
					for (var i = 0; i < boutons4.length; i++) {
						boutons4[i].addEventListener("click", verif);
					}

					function gameover() {
						// ne pas oublier de retirer les écouteurs de clic
						console.log('Temps écoulé');
						document.getElementById("output").innerHTML = 'Temps écoulé';
						for (var i = 0; i < boutons4.length; i++) {
							boutons4[i].removeEventListener("click", verif);
						}
					}

					// comparer avec la séquence
					function verif() {
						// dans tous les cas, on retire le timeout qui lance le gameover à 5s
						clearTimeout(to);
						// this désigne le bouton cliqué
						if (this != sequence[courant]) {
							// perdu
							sequence = [];
							courant = 0;
							console.log("Perdu !");
							document.getElementById("output").innerHTML = "Perdu !";
							location.reload();
							// on retire les listeners
							for (var i = 0; i < boutons4.length; i++) {
								boutons4[i].removeEventListener("click", verif);
							}
						} else {
							// bon bouton
							courant++;
							if (courant < sequence.length) {
								// s'il reste des boutons à cliquer, c'est tout ce qu'il y a à faire, les boutons continuent d'écouter, le tour du joueur continue
								to = setTimeout(gameover, 5000);
							} else {
								// round gagné
								// on ajoute un bouton à la séquence
								sequence.push(boutons4[Math.floor(Math.random() * 4)]);
								courant = 0;
								setTimeout(allumer, 300);
								// on retire les listeners, c'est à l'ordinateur de montrer la séquence
								for (var i = 0; i < boutons4.length; i++) {
									boutons4[i].removeEventListener("click", verif);
								}
							}
						}
					}
				}

				document.getElementById("start2").addEventListener("click",allumer);

				document.getElementById("lvl1").removeEventListener("click",lv1);
				document.getElementById("lvl2").removeEventListener("click",lv2);
				document.getElementById("lvl3").removeEventListener("click",lv3);
			}

			document.getElementById("lvl1").addEventListener("click",lv1);
			document.getElementById("lvl2").addEventListener("click",lv2);
			document.getElementById("lvl3").addEventListener("click",lv3);
			document.getElementById("lvl4").addEventListener("click",lv4);
		</script>
		<script src="js/main.js"></script>
	</body>
</html>