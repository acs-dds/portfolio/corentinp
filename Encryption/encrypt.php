<script>
	// string to unicode
	function char(a){
		return a.charCodeAt(0) - 65;
	}

	// vigenere
	function vig(text,key,decode){
		var i = 0,b;
		key = key.toUpperCase().replace(/[^A-Z]/g,"");
		return text.toUpperCase().replace(/[^A-Z ]/g,"").replace(/[A-Z]/g,function(a){
			b = key[i++ % key.length];
			return String.fromCharCode((char(a) + (decode ? 26 - char(b) : char(b))) % 26 + 65);
		});
	}

	// caesar / block key >! 26 pour eviter pb
	function cae(text,key,decode){
		key %= 65;
		return text.toUpperCase().replace(/[^A-Z ]/g,"").replace(/[A-Z]/g,function(a){
			console.log(a);
			console.log(((char(a) + (decode ? - 1 * key : key)) % 26) + 65);
			return String.fromCharCode(((char(a) + (decode ? - 1 * key : key)) % 26) + 65);
		});
	}

	// rot
	function rot13(text){
		return text.replace(/([a-m])|([n-z])/ig,function(a,b,c){
			return String.fromCharCode((b ? b.charCodeAt(0) + 13 : c ? c.charCodeAt(0) - 13 : 0) || a);
		});
	}

	// test
	function test(arg){
		var text = "The quick brown fox Jumped over the lazy Dog the lazy dog lazy dog dog";
		var key = "This is the Key!";
		var key2 = 5;
		var a,b,c;

		if(arg == "vig"){
			var enc = vig(text,key);
			var dec = vig(enc,key,true);
			var a = true;
		}
		else if(arg == "cae"){
			var enc = cae(text,key2);
			var dec = cae(enc,key2,true);
			var b = true;
		}
		else if(arg == "rot13"){
			var enc = rot13(text,13);
			var dec = rot13(enc,13);
			var c = true;
		}

		console.clear();
		if(a)setTimeout(function(){console.clear();console.info("%cEncyption Method%c: %cVigenère","color:red","color:initial","font-weight:bold;text-decoration:underline")},300);
		if(b)setTimeout(function(){console.clear();console.info("%cEncyption Method%c: %cCaesar","color:red","color:initial","font-weight:bold;text-decoration:underline")},300);
		if(c)setTimeout(function(){console.clear();console.info("%cEncyption Method%c: %cROT13","color:red","color:initial","font-weight:bold;text-decoration:underline")},300);
		setTimeout(function(){console.log("")},600);
		setTimeout(function(){console.info("%cText%c: " + text,"color:red","color:initial")},900);
		if(a)setTimeout(function(){console.info("%cKey%c: " + key,"color:red","color:initial")},1200);
		if(b)setTimeout(function(){console.info("%cKey%c: " + key2,"color:red","color:initial")},1200);
		if(c)setTimeout(function(){console.info("%cKey%c: " + key3,"color:red","color:initial")},1200);
		setTimeout(function(){console.info("%cText Encrypted%c: " + enc,"color:red","color:initial")},1500);
		setTimeout(function(){console.info("%cText Decrypted%c: " + dec,"color:red","color:initial")},1800);
		setTimeout(function(){console.log("")},2500);
		setTimeout(function(){console.log("")},3400);

		if(a || b){
			setTimeout(function(){console.info("%cFunction Call%c: %c" + arg + "%c(%ctext%c,%ckey%c,%cdecode%c);","color:red","color:initial","font-style:italic;color:#A6E22E","font-style:italic;color:272822","font-style:italic;color:#FD971F","font-style:italic;color:272822","font-style:italic;color:#FD971F","font-style:italic;color:272822","font-style:italic;color:#FD971F","font-style:italic;color:272822")},2800);
			setTimeout(function(){console.log("                   %cstr  " + (a ? "str" : "int") + "   bool","font-style:italic")},3100);
		}
		if(c){
			setTimeout(function(){console.info("%cFunction Call%c: %crot13%c(%ctext%c);","color:red","color:initial","font-style:italic;color:#A6E22E","font-style:italic;color:272822","font-style:italic;color:#FD971F","font-style:italic;color:272822")},2800);
			setTimeout(function(){console.log("                     %cstr","font-style:italic")},3100);
		}
	}

	function init(){
		console.clear();
		setTimeout(function(){console.info("%cFunction Call%c: %ctest%c(%ctype%c);","color:red","color:initial","font-style:italic;color:#A6E22E","font-style:italic;color:272822","font-style:italic;color:#FD971F","font-style:italic;color:272822")},300);
		setTimeout(function(){console.log('                    %c"vig" %cfor %cVigenère',"font-style:italic","font-style:initial","font-weight:bold")},600);
		setTimeout(function(){console.log('                     or')},700);
		setTimeout(function(){console.log('                    %c"cae" %cfor %cCaesar',"font-style:italic","font-style:initial","font-weight:bold")},800);
		setTimeout(function(){console.log('                     or')},1100);
		setTimeout(function(){console.log('                    %c"rot13" %cfor %cROT13',"font-style:italic","font-style:initial","font-weight:bold")},1400);
		setTimeout(function(){console.log("")},1700);
	}

	init();
</script>