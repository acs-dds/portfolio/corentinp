<!doctype html>
<html>
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Dropdown</title>

		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.min.css">

		<!--[if lt IE 9]>
			<script src="https://raw.githubusercontent.com/aFarkas/html5shiv/master/dist/html5shiv.min.js"></script>
		<![endif]-->

		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/3.1.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		<style type="text/css">
			@import url(".static/css/css.css");

			@-webkit-viewport{width:device-width;}
			@-moz-viewport{width:device-width;}
			@-ms-viewport{width:device-width;}
			@-o-viewport{width:device-width;}
			@viewport{width:device-width;}

			@media print{
				.container{
					width:auto;
				}
			}
		</style>
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">ACS Dijon</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active">
						<a href="#">Accueil</a>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Le staff
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="#jean">Jean</a></li>
							<li><a href="#christophe">Christophe</a></li>
							<li><a href="#sylvain">Sylvain</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#dds">DDS
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="#projets-dds">Les projets</a></li>
							<li><a href="#aotw">L'apprenant of the week</a></li>
						</ul>
					</li>
					<li class="divider-vertical"></li>
					<li><a href="#">A propos</a></li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Contact
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="#contact-acs21">Contacter ACS Dijon</a></li>
							<li><a href="#contact-ofp">Contacter Onlineformapro</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>

		<section id="slider">
			<span id="sl_play" class="sl_command"></span>
			<span id="sl_pause" class="sl_command"></span>
			<span id="sl_i1" class="sl_command sl_i"></span>
			<span id="sl_i2" class="sl_command sl_i"></span>
			<span id="sl_i3" class="sl_command sl_i"></span>
			<span id="sl_i4" class="sl_command sl_i"></span>
			<section id="slideshow">
				<a class="play_commands pause" href="#sl_pause" title="Maintain paused">Pause</a>
				<a class="play_commands play" href="#sl_play" title="Play the animation">Play</a>
				<a class="commands prev commands1" href="#sl_i4" title="Aller à la dernière slide">&lt;</a>
				<a class="commands next commands1" href="#sl_i2" title="Aller à la 2ème slide">&gt;</a>
				<a class="commands prev commands2" href="#sl_i1" title="Aller à la 1ère slide">&lt;</a>
				<a class="commands next commands2" href="#sl_i3" title="Aller à la 3ème slide">&gt;</a>
				<a class="commands prev commands3" href="#sl_i2" title="Aller à la 2ème slide">&lt;</a>
				<a class="commands next commands3" href="#sl_i4" title="Aller à la 4ème slide">&gt;</a>
				<a class="commands prev commands4" href="#sl_i3" title="Aller à la 3ème slide">&lt;</a>
				<a class="commands next commands4" href="#sl_i1" title="Aller à la 1ère slide">&gt;</a>
				<div class="container">
					<div class="c_slider"></div>
					<div class="slider">
						<figure>
							<img src="https://upload.wikimedia.org/wikipedia/commons/4/40/NLA_Brigus_tango7174.jpg" width="1667.13" height="500" />
							<!-- <img src="white.png" /> -->
							<figcaption>Captions</figcaption>
						</figure>
						<figure>
							<img src="https://upload.wikimedia.org/wikipedia/commons/4/40/NLA_Brigus_tango7174.jpg" width="1667.13" height="500" />
							<!-- <img src="blue.png" /> -->
							<figcaption>Captions</figcaption>
						</figure>
						<figure>
							<img src="https://upload.wikimedia.org/wikipedia/commons/4/40/NLA_Brigus_tango7174.jpg" width="1667.13" height="500" />
							<!-- <img src="red.png" /> -->
							<figcaption>Captions</figcaption>
						</figure>
						<figure>
							<img src="https://upload.wikimedia.org/wikipedia/commons/4/40/NLA_Brigus_tango7174.jpg" width="1667.13" height="500" />
							<!-- <img src="yellow.png" /> -->
							<figcaption>Captions</figcaption>
						</figure>
					</div>
				</div>
				<span id="timeline"></span>
			</section>
		</section>

		<div id="main" class="container">
			<div class="jumbotron text-center">
				<h1>Fixed Width Layout</h1>
				<p class="lead">The Bootstrap 3 grid is fluid only. This example shows how to use a custom container to create a fixed width layout.</p>
				<p><a class="btn btn-large btn-success" href="http://bootply.com/tagged/bootstrap-3" target="ext">More Examples</a></p>
			</div>
			
			<div class="jumbotron">
				<h4>Mobile First</h4>
				<p>Responsive by default, and a clean "flat" design.</p>

				<h4>Powerful Grid</h4>
				<p>3 responsive grid sizes to control the grid layout on tiny, small and large displays.</p>

				<h4>New Elements</h4>
				<p>New panels, list groups, glyphicons and justified navigation.</p>

				<h4>Less Bloat</h4>
				<p>The optimized CSS is in a single file. Glyphicons are now separate.</p>
			</div>

			<div class="jumbotron text-center">
				<h2>Moving from Bootstrap 2.x to 3.0?</h2>
				<p class="lead">Everything has changed, so you can't just replace the 2.x with the 3.0 files. The folks at Bootply, the Bootstrap Playground have created a migration guide to help.</p>
				<p><a class="btn btn-large btn-success" href="http://www.bootply.com/bootstrap-3-migration-guide" target="ext">Migration Guide</a></p>
			</div>
		</div>

		<script type="text/javascript" src=".static/js/js.js"></script>
	</body>
</html>