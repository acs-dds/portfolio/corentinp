(function(){
	"use strict";

	if(navigator.userAgent.match(/IEMobile\/10\.0/)){
		var msViewportStyle = document.createElement("style");
		msViewportStyle.appendChild(
			document.createTextNode(
				"@-ms-viewport{width:auto!important}"
			)
		);
		document.querySelector("head").appendChild(msViewportStyle);
	}

	// Since "Trident" means "Hello i'm from Microsoft and my name is IE :D" and i don't like IE
	//  > go to Google and try to learn how to make a good naviagtor!
	if(navigator.userAgent.match(/Trident/)){
		$("html").css("display","none");
		document.location = "https://www.google.com";
	}

	var nua = navigator.userAgent;
	var isAndroid = (nua.indexOf("Mozilla/5.0") > -1 && nua.indexOf("Android ") > -1 && nua.indexOf("AppleWebKit") > -1 && nua.indexOf("Chrome") === -1);
	if(isAndroid){
		$("select.form-control").removeClass("form-control").css("width","100%");
	}
})();

$(function(){
	$(window).on("scroll",function(){
		if ($(window).scrollTop() > 506){
			$(".navbar, .navbar-default").addClass("fixed");
			$(".container").addClass("fixed2");
		}
		else{
			$(".navbar, .navbar-default").removeClass("fixed");
			$(".container").removeClass("fixed2");
		}
	});
});