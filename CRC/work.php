<script type="text/javascript">
	var text = "Ceci est mon texte";
	var z;

	// v2
	var v2 = function(text){
		var res = [];

		text.split("").forEach(function(letter){
			var bin = letter.charCodeAt(0).toString(2);
			if(letter.match(/[a-zA-Z]/)){
				var padding = 8 - bin.length;
				res.push(new Array(padding + 1).join("0") + bin);
			}
			else{
				var padding = bin.length;
				res.push(bin.substring(0,8));
				z = true;
			}
		});

		console.log(res);

		var l = res.length;
		var calc = "";

		// output
		// > ["01010100", "01000101", "01010011", "01010100"]

		// calc
		// var hello = "Hello world";
		var hello = res;
		var histogram = [[0,1]];

		for(var i = 0;i < hello.length - 1;i++){
			histogram.push(new Array(0,1));
		}

		for(var i = 0;i < res.length;i++){
			for(var j = 0;j < res[i].length;j++){
				var letter = hello[i][j];
				histogram[i][letter] = (histogram[i][letter] || 0) + 1;
			}
		}

		// output
		// [H: 1, e: 1, l: 3, o: 2, " ": 1, w: 1, r: 1, d: 1]
		if(z){
			for(var i = 0; i < histogram[i][1];i++){
				histogram[i][1] = histogram[i][1] - 1;
			}
		}

		function add(a,b){
			return a + b;
		}

		for(var i = 0;i < res.length;i++){
			histogram[i].forEach(function(a,b){
				var y = histogram[i].reduce(add,0);
				console.log(y);
			})
			//var y = histogram[i].reduce(add,0);
		}

		console.log(histogram);
		console.log("histogram[0]: " + histogram[0]);
		console.log("l: " + l);
		console.log("l / 2: " + l / 2);
		console.log("Math.floor(l / 2): " + Math.floor(l / 2));

		//console.log("y: " + y);

		// verif
		if(l > 1){
			if(histogram[0] > Math.floor(l / 2)){
				console.log("true");
			}
			else{
				console.log("false");
			}
		}
		else if(l == 1){
			if(histogram[0] > l){
				console.log("true");
			}
			else{
				console.log("false");
			}
		}
	}
</script>