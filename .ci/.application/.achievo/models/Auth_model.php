<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Auth_model class.
 * 
 * @extends CI_Model
 */
class Auth_model extends CI_Model{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->database();
	}

	/**
	 * login function.
	 * 
	 * @access public
	 * @param string $username
	 * @param string $password
	 * @return bool true on success, false on failure
	 */
	public function login($username,$password){
		$this->db->select("mdp");
		$this->db->from("utilisateur");
		$this->db->where("entreprise",$username);
		$this->db->where("mdp",hash("sha512","ok@/" . $password));

		return $this->db->get()->result();
	}

	/**
	 * logout function.
	 * 
	 * @access public
	 * @param int $user_id
	 * @return bool
	 */
	public function logout($user_id){
		$this->db->from("users");
		$this->db->where("id",$user_id);

		return true;
	}

	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param string $username
	 * @return int the user id
	 */
	public function get_user_id_from_username($username){
		$this->db->select("id");
		$this->db->from("utilisateur");
		$this->db->where("entreprise",$username);

		return $this->db->get()->row("id");
	}

	/**
	 * get_user function.
	 * 
	 * @access public
	 * @param int $user_id
	 * @return object the user object
	 */
	public function get_user($user_id){
		$this->db->from("utilisateur");
		$this->db->where("id",$user_id);

		return $this->db->get()->row();
	}

	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param string $username
	 * @param string $email
	 * @param string $password
	 * @return bool true on success, false on failure
	 */
	public function create_user($username,$email,$password){
		$data = array(
			"username"   => $username,
			"email"      => $email,
			"password"   => $this->hash_password($password),
			"created_at" => date("Y-m-j H:i:s"),
		);
		
		return $this->db->insert("users",$data);
	}

	/**
	 * create_admin_user function.
	 * 
	 * @access public
	 * @param string $username
	 * @param string $email
	 * @param string $password
	 * @return bool
	 */
	public function create_admin_user($username,$email,$password){
		$data = array(
			"username"     => $username,
			"email"        => $email,
			"password"     => $this->hash_password($password),
			"created_at"   => date("Y-m-j H:i:s"),
			"rights"       => "4",
			"is_confirmed" => "1",
		);

		return $this->db->insert("users",$data);
	}

	/**
	 * hash_password function.
	 * 
	 * @access private
	 * @param string $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password){
		return password_hash($password,PASSWORD_BCRYPT);
	}

	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param string $password
	 * @param string $hash
	 * @return bool
	 */
	private function verify_password_hash($password,$hash){
		return password_verify($password,$hash);
	}
}