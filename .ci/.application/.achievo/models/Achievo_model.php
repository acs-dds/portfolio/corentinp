<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Auth_model class.
 * 
 * @extends CI_Model
 */
class Achievo_model extends CI_Model{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->database();
	}

	public function get_data(){
		$this->db->from("succes");
		$this->db->order_by("id ASC");

		return $this->db->get()->result();
	}

	public function get_data_from_id($id){
		$this->db->from("succes");
		$this->db->where("id",$id);

		return $this->db->get()->result();
	}

	public function update_data_by_id($id,$progression){
		$this->db->from("succes");
		$this->db->where("id",$id);

		$data = array(
			"progression" => $progression
		);

		return $this->db->update("succes",$data);
	}
}