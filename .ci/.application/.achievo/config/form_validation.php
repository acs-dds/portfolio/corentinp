<?php defined("BASEPATH") OR exit("No direct script access allowed");

$config = array(
	"auth/login" => array(
		array(
			"field" => "username",
			"label" => "Username",
			"rules" => "required|alpha_numeric"
		),
		array(
			"field" => "password",
			"label" => "Password",
			"rules" => "required"
		)
	),
	"auth/register" => array(
		array(
			"field" => "username",
			"label" => "Username",
			"rules" => "trim|required|alpha_numeric|min_length[4]|max_length[20]|is_unique[users.username]"
		),
		array(
			"field" => "email",
			"label" => "Email",
			"rules" => "trim|required|valid_email|is_unique[users.email]"
		),
		array(
			"field" => "password",
			"label" => "Password",
			"rules" => "trim|required|min_length[6]"
		),
		array(
			"field" => "password_confirm",
			"label" => "Confirm Password",
			"rules" => "trim|required|min_length[6]|matches[password]"
		)
	)
);