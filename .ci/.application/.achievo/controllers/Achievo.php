<?php defined("BASEPATH") OR exit("No direct script access allowed");

class Achievo extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library(array("session","form_validation"));
		$this->load->helper(array("url"));
		$this->load->model(array("achievo_model"));

		$this->output->enable_profiler(false);
	}

	/**
	 * index function.
	 * 
	 * @access public
	 * @return bool
	 */
	public function index(){
		$data = new stdClass();

		$data->data = $this->achievo_model->get_data();

		$this->load->view("header");
		$this->load->view("achievo/index",$data);
		$this->load->view("footer");
	}

	/**
	 * view function.
	 * 
	 * @access public
	 * @return bool
	 */
	public function view($id){
		$data = new stdClass();

		$data->object = $this->achievo_model->get_data_from_id($id);

		$this->load->view("header");
		$this->load->view("achievo/view",$data);
		$this->load->view("footer");
	}

	/**
	 * update function.
	 * 
	 * @access public
	 * @return bool
	 */
	public function update($id){
		$number = $this->input->post("progression");

		if($number){
			$this->achievo_model->update_data_by_id($id,$number);
		}
		redirect("achievo/view/" . $id);
	}
}