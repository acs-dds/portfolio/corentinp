<?php defined("BASEPATH") OR exit("No direct script access allowed");?>
			<div class="container">
				<div class="row">
					<style>@import url("http://fonts.googleapis.com/css?family=Expletus+Sans");
					h2,h3,h5,p{margin:0;}.panel-body{margin-left:15em;max-height:65em;display:flex;flex-flow:column wrap;}.normal{background:white;opacity:.8;border-radius:5px;display:flex;padding:1em;margin:.5em;border:1px solid black;min-width:30em;max-width:29em;height:6em;}.fa{font-size:3.4em}.fa-trophy{color:#ffd400;}figure{margin:auto 0 0 0;width:0}a{text-decoration:none}.title{height:1.2em;width:45em;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;}.desc{width:55em;height:0;margin-top:2em;margin-left:-25em}.desc>h5{max-height:1em;font-family:"Expletus Sans";}progress{margin-bottom:-2em;width:100%;}html{background:url("fond.jpg");}.fini{font-size:1.4em}progress[value] {
	/* Get rid of the default appearance */
	-webkit-appearance: none;
	
	/* This unfortunately leaves a trail of border behind in Firefox and Opera. We can remove that by setting the border to none. */
	border: none;
	
	/* Add dimensions */
	height: 20px;
	
	/* Although firefox doesn't provide any additional pseudo class to style the progress element container, any style applied here works on the container. */
	  background-color: whiteSmoke;
	  border-radius: 3px;
	  box-shadow: 0 2px 3px rgba(0,0,0,.5) inset;
	
	/* Of all IE, only IE10 supports progress element that too partially. It only allows to change the background-color of the progress value using the 'color' attribute. */
	color: royalblue;
	
	position: relative;
}

/*
Webkit browsers provide two pseudo classes that can be use to style HTML5 progress element.
-webkit-progress-bar -> To style the progress element container
-webkit-progress-value -> To style the progress element value.
*/

progress[value]::-webkit-progress-bar {
	background-color: whiteSmoke;
	border-radius: 3px;
	box-shadow: 0 2px 3px rgba(0,0,0,.5) inset;
}

progress[value]::-webkit-progress-value {
	position: relative;
	
	background-size: 35px 20px, 100% 100%, 100% 100%;
	border-radius:3px;
	
	/* Let's animate this */
	
}
progress{animation: animate-stripes 5s linear infinite;}
@keyframes animate-stripes { 100% { background-position: -100px 0; } }

/* Let's spice up things little bit by using pseudo elements. */

progress[value]::-webkit-progress-value:after {
	/* Only webkit/blink browsers understand pseudo elements on pseudo classes. A rare phenomenon! */
	content: '';
	position: absolute;
	
	width:5px; height:5px;
	top:7px; right:7px;
	
	background-color: white;
	border-radius: 100%;
}

/* Firefox provides a single pseudo class to style the progress element value and not for container. -moz-progress-bar */

progress[value]::-moz-progress-bar {
	/* Gradient background with Stripes */
	background-image:
	-moz-linear-gradient( 135deg,
													 transparent,
													 transparent 33%,
													 rgba(0,0,0,.1) 33%,
													 rgba(0,0,0,.1) 66%,
													 transparent 66%),
    -moz-linear-gradient( top,
														rgba(255, 255, 255, .25),
														rgba(0,0,0,.2)),
     -moz-linear-gradient( left, #09c, #f44);
	
	background-size: 35px 20px, 100% 100%, 100% 100%;
	border-radius:3px;
	
	/* Firefox doesn't support CSS3 keyframe animations on progress element. Hence, we did not include animate-stripes in this code block */
}

/* Fallback technique styles */
.progress-bar {
	background-color: whiteSmoke;
	border-radius: 3px;
	box-shadow: 0 2px 3px rgba(0,0,0,.5) inset;

	/* Dimensions should be similar to the parent progress element. */
	width: 100%; height:20px;
}

.progress-bar span {
	background-color: royalblue;
	border-radius: 3px;
	
	display: block;
	text-indent: -9999px;
}


.html5::-webkit-progress-value{
	/* Gradient background with Stripes */
	background-image:
	-webkit-linear-gradient( 135deg,
													 transparent,
													 transparent 33%,
													 rgba(0,0,0,.1) 33%,
													 rgba(0,0,0,.1) 66%,
													 transparent 66%),
    -webkit-linear-gradient( top,
														rgba(255, 255, 255, .25),
														rgba(0,0,0,.2)),
     -webkit-linear-gradient( left, #09c, #f44);
}

.html5::-moz-progress-bar{
	/* Gradient background with Stripes */
	background-image:
	-moz-linear-gradient( 135deg,
													 transparent,
													 transparent 33%,
													 rgba(0,0,0,.1) 33%,
													 rgba(0,0,0,.1) 66%,
													 transparent 66%),
    -moz-linear-gradient( top,
														rgba(255, 255, 255, .25),
														rgba(0,0,0,.2)),
     -moz-linear-gradient( left, #09c, #f44);
}
</style>
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Achievo</h3>
							</div><!-- .panel-heading -->
							<div class="panel-body">
<?php foreach($data as $donnee) : ?>
								<article class="normal">
									<figure>
<?php if($donnee->progression === $donnee->objectif) : ?>
										<i class="fa fa-trophy" aria-hidden="true"></i>
<?php else : ?>
										<i class="fa fa-lock" aria-hidden="true"></i>
<?php endif;?>
									</figure>
									<h2 class="title"><a href="achievo/view/<?=url_title($donnee->titre,"-",true) . "-" . $donnee->id;?>"><?=$donnee->titre;?></a></h2>
									<div class="desc">
										<h5><?=$donnee->intitule;?></h5>
<?php if($donnee->progression === $donnee->objectif) : ?>
										<h4 class="fini">Bravo, tu as terminé !</h4>
<?php else : ?>
										<progress min="0" max="<?=$donnee->objectif?>" value="<?=$donnee->progression?>" class="html5">
											<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
											<div class="progress-bar">
												<span style="width:<?=$donnee->progression/$donnee->objectif*100?>%">80%</span>
											</div>
										</progress>
<?php endif;?>
									</div>
								</article>
<?php endforeach;?>
							</div><!-- .col-md-10 -->
						</div><!-- .row -->
					</div><!-- .container -->
				</div><!-- .col-md-10 -->
			</div><!-- .col-md-10 -->