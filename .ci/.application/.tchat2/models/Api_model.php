<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Api_model extends CI_Model{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->database();
	}

	/**
	 * get_messages function.
	 * 
	 * @access public
	 * @param int $channel_id
	 * @return object
	 */
	public function get_messages($channel_id,$user_id){
		$this->db->select("message.id,message.date,message.contenu,users.username,users.avatar");
		$this->db->from("message");
		$this->db->where("id_salon",$channel_id);
		$this->db->join("users","message.id_utilisateur = users.id");
		$this->db->order_by("message.date DESC");

		return $this->db->get()->result();
	}

	/**
	 * get_channel_id_from_name function.
	 * 
	 * @access public
	 * @param string $channel
	 * @return int the channel id
	 */
	public function get_channel_id_from_name($channel){
		$this->db->select("id");
		$this->db->from("salon");
		$this->db->where("libelle",$channel);

		return $this->db->get()->row("id");
	}

	/**
	 * get_user_id_from_name function.
	 * 
	 * @access public
	 * @param string $username
	 * @return int the user id
	 */
	public function get_user_id_from_name($username){
		$this->db->select("id");
		$this->db->from("users");
		$this->db->where("username",$username);

		return $this->db->get()->row("id");
	}

	/**
	 * reset function.
	 * 
	 * @access public
	 * @param int $channel_id
	 * @return bool
	 */
	public function reset($channel_id){
		$this->db->where("id_salon",$channel_id);

		return $this->db->delete("message");
	}

	/**
	 * get_users function.
	 * 
	 * @access public
	 * @return string
	 */
	public function get_users(){
		$this->db->select("id,username,status");
		$this->db->from("users");
		$this->db->order_by("id");

		return $this->db->get()->result();
	}

	/**
	 * get_channel function.
	 * 
	 * @access public
	 * @return object
	 */
	public function get_channels(){
		$this->db->from("salon");
		$this->db->order_by("id");

		return $this->db->get()->result();
	}

	/**
	 * verify_channel function.
	 * 
	 * @access public
	 * @param string $channel
	 * @param string $mdp
	 * @return bool
	 */
	public function verify_channel($channel,$mdp){
		$this->db->select("libelle,mdp");
		$this->db->from("salon");
		$this->db->where("libelle",$channel);
		$this->db->where("mdp",$mdp);

		if($this->db->get()->row("libelle") == NULL){
			return false;
		}
		else{
			return true;
		}
	}

	/**
	 * delete_channel function.
	 * 
	 * @access public
	 * @param int $channel_id
	 * @return bool
	 */
	public function delete_channel($channel_id){
		$this->db->where("id_salon",$channel_id);

		if($this->db->delete("message")){
			$this->db->where("id",$channel_id);

			return $this->db->delete("salon");
		}
		return false;
	}

	/**
	 * delete_message function.
	 * 
	 * @access public
	 * @param int $channel_id
	 * @param string $time
	 * @param string $message
	 */
	public function delete_message($channel_id,$time,$message,$user_id){
		$this->db->where("id_salon",$channel_id);
		$this->db->where("date",$time);
		$this->db->where("contenu",$message);

		return $this->db->delete("message");
	}

	/**
	 * create_channel function.
	 * 
	 * @access public
	 * @param string $channel_name
	 * @return bool
	 */
	public function create_channel($channel_name,$mdp){
		$data = array(
			"libelle" => $channel_name,
			"mdp"     => $mdp
		);

		return $this->db->insert("salon",$data);
	}

	/**
	 * send_message function.
	 * 
	 * @access public
	 * @param string $message
	 * @param int $id_utilisateur
	 * @param int $id_salon
	 * @return bool
	 */
	public function send_message($message,$utilisateur_id,$channel_id){
		$data = array(
			"date"           => microtime(true),
			"contenu"        => $message,
			"id_utilisateur" => $utilisateur_id,
			"id_salon"       => $channel_id
		);

		return $this->db->insert("message",$data);
	}
}