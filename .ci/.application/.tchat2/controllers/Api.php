<?php defined("BASEPATH") OR exit("No direct script access allowed");

class Api extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library(array("session"));
		$this->load->helper(array("url"));
		$this->load->model(array("api_model"));

		$this->output->enable_profiler(false);
	}

	/**
	 * get function.
	 * 
	 * @access public
	 * @return void
	 */
	public function get(){
		$messages = $this->input->post("channel");
		$user     = $this->input->post("users");
		$channels = $this->input->post("channels");

		$data = new stdClass();

		$channel_id = $this->api_model->get_channel_id_from_name($messages);
		$user_id    = $this->api_model->get_user_id_from_name($user);

		if($messages){
			$data->messages = $this->api_model->get_messages($channel_id,$_SESSION["user_id"]);

			$this->load->view("api/get/message",$data);
		}
		if($user){
			$data->users = $this->api_model->get_users();

			$this->load->view("api/get/user",$data);
		}
		if($channels){
			$data->channels = $this->api_model->get_channels();

			$this->load->view("api/get/channel",$data);
		}
	}

	/**
	 * send function.
	 * 
	 * @access public
	 * @return void
	 */
	public function send(){
		$message = $this->input->post("content");
		$author  = $this->input->post("author");
		$channel = $this->input->post("channel");

		$channel_id = $this->api_model->get_channel_id_from_name($channel);

		if($message && $author && $channel){
			$this->api_model->send_message($message,$_SESSION["user_id"],$channel_id);
		}
	}

	/**
	 * delete function.
	 * 
	 * @access public
	 * @return void
	 */
	public function delete(){
		$channel = $this->input->post("channel");
		$time = $this->input->post("time");
		$message = $this->input->post("message");
		$channel_id = $this->api_model->get_channel_id_from_name($channel);

		if($channel){
			if($channel != "general" && empty($time)){
				$this->api_model->delete_channel($channel_id);
			}
			if($time && $channel && $message){
				$this->api_model->delete_message($channel_id,$time,$message,$_SESSION["user_id"]);
			}
		}
	}

	/**
	 * reset function.
	 * 
	 * @access public
	 * @return void
	 */
	public function reset(){
		$channel = $this->input->post("channel");
		$channel_id = $this->api_model->get_channel_id_from_name($channel);

		if($channel){
			$this->api_model->reset($channel_id);
		}
	}

	/**
	 * create function.
	 * 
	 * @access public
	 * @return void
	 */
	public function create(){
		$channel = $this->input->post("channel");
		$mdp     = $this->input->post("mdp");

		if($channel){
			$this->api_model->create_channel($channel,$mdp);
		}
	}

	/**
	 * verify function.
	 * 
	 * @access public
	 * @return bool on true or 404 error page on false
	 */
	public function verify(){
		$this->output->enable_profiler(false);

		$channel = $this->input->post("channel");
		$mdp     = $this->input->post("mdp");

		if($channel && $mdp){
			if($this->api_model->verify_channel($channel,$mdp)){
				return true;
			}
			else{
				show_404();
			}
		}
	}
}