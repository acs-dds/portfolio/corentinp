<?php defined("BASEPATH") OR exit("No direct script access allowed");

class Auth extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library(array("session","form_validation"));
		$this->load->helper(array("url","form"));
		$this->load->model(array("auth_model"));

		$this->output->enable_profiler(false);
	}

	/**
	 * login function.
	 * 
	 * @access public
	 * @return bool
	 */
	public function login(){
		// create the data object
		$data = new stdClass();

		if($this->form_validation->run() == false){
			// validation not ok, send validation errors to the view
			$this->load->view("header");
			$this->load->view("auth/login");
			$this->load->view("footer");
		}
		else{
			// set variables from the form
			$username = $this->input->post("username");
			$password = $this->input->post("password");

			if($this->auth_model->login($username,$password)){
				$user_id = $this->auth_model->get_user_id_from_username($username);
				$user    = $this->auth_model->get_user($user_id);

				// set session user datas
				$_SESSION["user_id"]      = (int)$user->id;
				$_SESSION["username"]     = (string)$user->username;
				$_SESSION["logged_in"]    = (bool)true;
				$_SESSION["is_confirmed"] = (bool)$user->is_confirmed;
				$_SESSION["rights"]       = (int)$user->rights;

				// set the refresh to true
				$data->refresh = true;
				$data->logged = true;

				// user login ok
				$this->load->view("header",$data);
				$this->load->view("home",$data);
				$this->load->view("footer");
			}
			else{
				// login failed
				$data->error = "Wrong username or password.";

				// send error to the view
				$this->load->view("header",$data);
				$this->load->view("auth/login",$data);
				$this->load->view("footer");
			}
		}
	}

	/**
	 * email_validation function.
	 * 
	 * @access public
	 * @param string $username
	 * @param string $hash
	 * @return void
	 */
	public function email_validation($username,$hash){
		// create the data object
		$data = new stdClass();

		// avoid blank at the end of the url
		$hash = trim($hash);

		if($this->user_model->confirm_account($username,$hash)){
			// account validation ok
			$data->success = 'Congratulation, your email address has been confirmed and your account is now validated! Please <a href="' . base_url("login") . '">login</a>.';
			$this->load->view("header");
			$this->load->view("user/register/confirmation",$data);
			$this->load->view("footer");
		}
		else{
			// account validation failed
			$data->error = "An error has occurred, your email address cannot be validated. Please contact the website administrator.";
			$this->load->view("header");
			$this->load->view("user/register/confirmation",$data);
			$this->load->view("footer");
		}
	}

	/**
	 * register function.
	 * 
	 * @access public
	 * @return bool
	 */
	public function register(){
		// create the data object
		$data = new stdClass();

		// load form helper and validation library
		$this->load->helper("form");
		$this->load->library("form_validation");

		if($this->form_validation->run() === false){
			// validation not ok, send validation errors to the view
			$this->load->view("header");
			$this->load->view("auth/register");
			$this->load->view("footer");
		}
		else{
			// set variables from the form
			$username = $this->input->post("username");
			$email    = $this->input->post("email");
			$password = $this->input->post("password");

			if($this->auth_model->create_user($username,$email,$password)){
				// set the refresh to true
				$data->refresh = true;
				$data->register = true;

				// user creation ok
				$this->load->view("header",$data);
				$this->load->view("home",$data);
				$this->load->view("footer");
			}
			else{
				// user creation failed, this should never happen
				$data->error = "There was a problem creating your new account. Please try again.";

				// send error to the view
				$this->load->view("header");
				$this->load->view("auth/register",$data);
				$this->load->view("footer");
			}
		}
	}

	/**
	 * logout function.
	 * 
	 * @access public
	 * @return void
	 */
	public function logout(){
		// create the data object
		$data = new stdClass();

		if(isset($_SESSION["logged_in"]) && $_SESSION["logged_in"] === true){
			// remove session datas
			if($this->auth_model->logout($_SESSION["user_id"])){
				foreach($_SESSION as $key => $value){
					unset($_SESSION[$key]);
				}

				// set the logout to true
				$data->refresh = true;
				$data->logout = true;

				// user logout ok
				$this->load->view("header",$data);
				$this->load->view("home",$data);
				$this->load->view("footer");
			}
			else{
				// there user was not logged in, we cannot logged him out,
				// redirect him to site root
				redirect("/");
			}
		}
	}
}