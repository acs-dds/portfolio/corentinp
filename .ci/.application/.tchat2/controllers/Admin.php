<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Admin class.
 * 
 * @extends CI_Controller
 */
class Admin extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library(array("session"));
		$this->load->helper(array("url"));
		$this->load->model(array("user_model","admin_model"));

		$this->output->enable_profiler(false);
	}

	/**
	 * index function.
	 * 
	 * @access public
	 * @return void
	 */
	public function index(){
		// if the user is not admin, redirect to base url
		if(!isset($_SESSION["rights"]) === 3){
			redirect(base_url());
			return;
		}

		// create the data object
		$data = new stdClass();

		$this->load->view("header");
		$this->load->view("admin/home",$data);
		$this->load->view("footer");
	}

	/**
	 * users function.
	 * 
	 * @access public
	 * @return void
	 */
	public function users(){
		// if the user is not admin, redirect to base url
		if(!isset($_SESSION["rights"]) === 3){
			redirect(base_url());
			return;
		}

		// create the data object
		$data = new stdClass();

		$data->users = $this->admin_model->get_users();

		$this->load->view("header");
		$this->load->view("admin/users",$data);
		$this->load->view("footer");
	}

	/**
	 * users_edit function.
	 * 
	 * @access public
	 * @param mixed $username (default: false)
	 * @return void
	 */
	public function users_edit($username = false){
		// if the user is not admin, redirect to base url
		if(!isset($_SESSION["rights"]) === 3){
			redirect(base_url());
			return;
		}

		if($username === false){
			redirect(base_url("admin/users"));
			return;
		}

		// create the data object
		$data = new stdClass();

		// create the user object
		$user_id = $this->user_model->get_user_id_from_username($username);
		$user    = $this->user_model->get_user($user_id);

		// assign values to the data object
		$data->user = $user;
		if ($user->updated_by !== null) {
			$data->user->updated_by = $this->user_model->get_username_from_user_id($user->updated_by);
		}

		// load form helper and validation library
		$this->load->helper("form");
		$this->load->library("form_validation");

		// set form validations rules
		$this->form_validation->set_rules("user_rights","User Rights","required|in_list[sysadmin,admin,mod,vip,user]");

		if($this->form_validation->run() == false){
			$this->load->view("header");
			$this->load->view("admin/edit_user",$data);
			$this->load->view("footer");
		}
		else{
			// assign rights to variables
			if($this->input->post("user_rights") === "sysadmin"){
				$rights = "4";
			}
			elseif($this->input->post("user_rights") === "admin"){
				$rights = "3";
			}
			elseif($this->input->post("user_rights") === "mod"){
				$rights = "2";
			}
			elseif($this->input->post("user_rights") === "vip"){
				$rights = "1";
			}
			else{
				$rights = "0";
			}

			if($this->admin_model->update_user_rights($user_id,$rights)){
				// update user success
				$data->success = $user->username . " has successfully been updated.";
			}
			else{
				// error while updating user rights, this should never happen
				$data->error = "There was an error while trying to update this user. Please try again";
			}

			$this->load->view("header");
			$this->load->view("admin/edit_user",$data);
			$this->load->view("footer");
		}
	}

	/**
	 * options function.
	 * 
	 * @access public
	 * @return void
	 */
	public function options(){
		// if the user is not admin, redirect to base url
		if(!isset($_SESSION["rights"]) === 3){
			redirect(base_url());
			return;
		}

		// create the data object
		$data = new stdClass();

		$this->load->view("header");
		$this->load->view("admin/options",$data);
		$this->load->view("footer");
	}

	/**
	 * emails function.
	 * 
	 * @access public
	 * @return void
	 */
	public function emails(){
		// if the user is not admin, redirect to base url
		if(!isset($_SESSION["rights"]) === 3){
			redirect(base_url());
			return;
		}

		// create the data object
		$data = new stdClass();

		$this->load->view("header");
		$this->load->view("admin/emails",$data);
		$this->load->view("footer");
	}
}