<?php defined("BASEPATH") OR exit("No direct script access allowed");

class TChat extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library(array("session"));
		$this->load->helper(array("url"));

		$this->output->enable_profiler(false);
	}

	/**
	 * index function.
	 * 
	 * @access public
	 * @return bool
	 */
	public function index(){
		// if the user is not admin, redirect to base url
		if(!isset($_SESSION["user_id"])){
			redirect(base_url());
			return;
		}

		$this->load->view("header");
		$this->load->view("tchat/index");
		$this->load->view("footer");
	}
}