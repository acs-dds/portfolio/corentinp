<?php defined("BASEPATH") OR exit("No direct script access allowed");

class Home extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library(array("session","form_validation"));
		$this->load->helper(array("url"));

		$this->output->enable_profiler(false);
	}

	/**
	 * index function.
	 * 
	 * @access public
	 * @return bool
	 */
	public function index(){
		$this->load->view("header");
		$this->load->view("home");
		$this->load->view("footer");
	}
}