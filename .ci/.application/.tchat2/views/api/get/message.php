<?php foreach($messages as $id => $message) : ?>
<div id="message">
	<img src="uploads/avatars/<?=$message->avatar;?>" alt="Profile" height="80" width="80" style="border-radius:50%">

	<p class="author"><?=trim($message->username);?> <!--(?=$this->pseudo;?)--></p>
	<p class="content"><?=trim($message->contenu);?></p>
	<p class="time"><?=ucfirst(strftime("%A %e %B %Y à %k:%M:%S",$message->date));?></p>
<?php if($_SESSION["rights"] === 4) : ?>
	<input type="button" name="<?=trim($message->contenu);?>" date="<?=$message->date;?>" id="clear" value="✕" onclick="deleteMessage(this.name,this.attributes[2].value);">
<?php endif;?>
</div>
<?php endforeach;?>