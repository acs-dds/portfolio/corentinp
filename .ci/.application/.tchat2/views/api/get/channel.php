<?php foreach($channels as $id => $channel) : ?>
<?php if($channel->mdp != "") : ?>
<li>
	<a id="ch-<?=$channel->id;?>" name="<?=$channel->libelle;?>" href="#" onclick="verifyChMdp(this.name)"><?=$channel->libelle;?></a>
<?php if($_SESSION["rights"] === 4) : ?>
	<input type="button" id="suppr" name="<?=$channel->libelle;?>" onclick="deleteChannel(this.name)" value="✖">
<?php endif;?>
</li>
<?php else : ?>
<li>
	<a id="ch-<?=$channel->id;?>" name="<?=$channel->libelle;?>" href="#" onclick="channel=this.name;"><?=$channel->libelle;?></a>
<?php if($_SESSION["rights"] === 4) : ?>
	<input type="button" id="suppr" name="<?=$channel->libelle;?>" onclick="deleteChannel(this.name)" value="✖">
<?php endif;?>
</li>
<?php endif;?>
<?php endforeach;?>