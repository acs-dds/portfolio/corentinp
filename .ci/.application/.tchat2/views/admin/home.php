<?php defined("BASEPATH") OR exit("No direct script access allowed");?>
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<ul class="nav nav-pills nav-stacked">
							<li role="presentation" class="active"><a>Home</a></li>
							<li role="presentation"><a href="<?=base_url("admin/users")?>">Users</a></li>
							<li role="presentation"><a href="<?=base_url("admin/options")?>">Options</a></li>
							<li role="presentation"><a href="<?=base_url("admin/emails")?>">Emails</a></li>
						</ul><!-- .nav .nav-pills .nav-stacked -->
					</div><!-- .col-md-12 -->
					<div class="col-md-10">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Administration home</h3>
							</div><!-- .panel-heading -->
							<div class="panel-body">
								
							</div><!-- .panel-body -->
						</div><!-- .panel .panel-default -->
					</div><!-- .col-md-10 -->
				</div><!-- .row -->
			</div><!-- .container -->