<?php defined("BASEPATH") OR exit("No direct script access allowed");?>
<?php if(ENVIRONMENT === "development") : ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p>Utilisateur : <?=var_dump($user);?></p>
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
<?php endif;?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ol class="breadcrumb">
							<li><a href="<?=base_url();?>">Accueil</a></li>
							<li>Utilisateur</li>
							<li class="active"><?=$user->username;?></li>
						</ol><!-- .breadcrumb -->
					</div><!-- .col-md-12 -->
					<div class="col-md-12">
						<div class="page-header">
							<h1>Profile de <small><?=$user->username;?></small><?php if(isset($_SESSION["username"]) && $_SESSION["username"] === $user->username) : ?> <a href="<?=base_url("user/" . $user->username . "/edit");?>" class="btn btn-xs btn-success">Éditer votre profile</a><?php endif;?></h1>
						</div><!-- .page-header -->
					</div><!-- .col-md-12 -->
					<div class="col-md-12">
						<div class="row">
							<div class="col-sm-2 text-center">
								<img class="avatar" src="<?=base_url("uploads/avatars/" . $user->avatar);?>">
								<h2><?=$user->username;?></h2>
							</div><!-- .col-sm-2 .text-center -->
							<div class="col-sm-4 col-sm-offset-1">
								<p>Nom : <?=$user->nom;?></p>
								<p>Prénom : <?=$user->prenom;?></p>
								<p>Pseudo : <?=$user->username;?></p>
								<p>E-Mail : <?=$user->email;?></p>
							</div><!-- .col-sm-4 .col-sm-offset-1 -->
							<div class="col-sm-5">
								<p>Date d'inscription : <?=$user->created_at;?></p>
<?php if($user->rights == "0") : ?> 
								<p>Rang : Utilisateur</p>
<?php elseif($user->rights == "1") : ?>
								<p>Rang : VIP</p>
<?php elseif($user->rights == "2") : ?>
								<p>Rang : Modérateur</p>
<?php elseif($user->rights == "3") : ?>
								<p>Rang : Administrateur</p>
<?php elseif($user->rights == "4") : ?>
								<p>Rang : SysAdministrateur</p>
<?php endif; ?>
								<p>Status : <?=$user->status;?></p>
							</div><!-- .col-sm-5 -->
						</div><!-- .row -->
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container -->