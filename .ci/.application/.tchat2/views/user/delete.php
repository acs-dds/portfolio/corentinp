<?php defined("BASEPATH") OR exit("No direct script access allowed");?>
<?php if(ENVIRONMENT === "development") : ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p>Utilisateur : <?=var_dump($user);?></p>
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
<?php endif;?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ol class="breadcrumb">
							<li><a href="<?=base_url();?>">Accueil</a></li>
							<li>Utilisateur</li>
							<li><a href="<?=base_url("user/" . $user->username);?>"><?=$user->username;?></a></li>
							<li class="active">Supprimer</li>
						</ol><!-- .breadcrumb -->
					</div><!-- .col-md-12 -->
					<div class="col-md-12">
						<div class="page-header">
							<h1>Suppression du compte utilisateur <small><?=$user->username;?></small></h1>
						</div><!-- .page-header -->
					</div><!-- .col-md-12 -->
					<div class="col-md-12">
						<div class="alert alert-success" role="alert">
							<p>Votre compte d'utilisateur a été supprimé. Bye Bye :(</p>
						</div><!-- .alert .alert-success -->
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container --><?php session_destroy();?>