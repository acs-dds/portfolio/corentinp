<?php defined("BASEPATH") OR exit("No direct script access allowed");?>
<?php if(ENVIRONMENT === "development") : ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p>Utilisateur : <?=var_dump($user);?></p>
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
<?php endif;?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ol class="breadcrumb">
							<li><a href="<?=base_url();?>">Accueil</a></li>
							<li>Utilisateur</li>
							<li><a href="<?=base_url("user/" . $user->username);?>"><?=$user->username;?></a></li>
							<li class="active">Éditer</li>
						</ol><!-- .breadcrumb -->
					</div><!-- .col-md-12 -->
					<div class="col-md-12">
						<div class="page-header">
							<h1>Éditer votre profile <small><?=$user->username;?></small></h1>
						</div><!-- .page-header -->
					</div><!-- .col-md-12 -->
<?php if(validation_errors()) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<p><?=validation_errors();?></p>
						</div><!-- .alert .alert-danger -->
					</div><!-- .col-md-12 -->
<?php endif;?>
<?php if(isset($error)) : ?>
					<div class="col-md-12">
						<div class="alert alert-danger" role="alert">
							<p><?=$error;?></p>
						</div><!-- .alert .alert-danger -->
					</div><!-- .col-md-12 -->
<?php endif;?>
<?php if(isset($success)) : ?>
					<div class="col-md-12">
						<div class="alert alert-success" role="alert">
							<p><?=$success;?></p>
						</div><!-- .alert .alert-success -->
					</div><!-- .col-md-12 -->
<?php endif;?>
<?php if(isset($_SESSION["flash"])) : ?>
					<div class="col-md-12">
						<div class="alert alert-success" role="alert">
							<p><?=$_SESSION["flash"];?></p>
							<?php unset($_SESSION["flash"]);?>
						</div><!-- .alert .alert-success -->
					</div><!-- .col-md-12 -->
<?php endif;?>
					<div class="col-md-12">
						<div class="row">
							<?=form_open_multipart();?>
								<div class="col-md-8">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Gérer votre compte</h3>
										</div><!-- .panel-heading -->
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3 text-center">
													<img class="avatar" src="<?=base_url("uploads/avatars/" . $user->avatar);?>">
													<br><br>
													<div class="form-group">
														<label for="avatar">Change ta photo de profil</label>
														<input type="file" id="avatar" name="userfile" style="word-wrap:break-word;outline:none">
													</div><!-- .form-group -->
												</div><!-- .col-sm-3 .text-center -->
												<div class="col-sm-7 col-sm-offset-2">
													<div class="form-group">
														<label for="username">Votre nom d'utilisateur</label>
														<input type="text" class="form-control" id="username" name="username" placeholder="<?=$user->username;?>">
													</div><!-- .form-group -->
													<div class="form-group">
														<label for="email">Votre e-mail</label>
														<input type="email" class="form-control" id="email" name="email" placeholder="<?=$user->email;?>">
													</div><!-- .form-group -->
													<div class="form-group">
														<label for="current_password">Mot de passe</label>
														<input type="password" class="form-control" id="current_password" name="current_password" placeholder="Mot de passe">
													</div><!-- .form-group -->
													<div class="form-group">
														<label for="password">Nouveau mot de passe</label>
														<input type="password" class="form-control" id="password" name="password" placeholder="Nouveau mot de passe">
													</div><!-- .form-group -->
													<div class="form-group">
														<label for="password_confirm">Confirmer le nouveau mot de passe</label>
														<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirmer le nouveau mot de passe">
													</div><!-- .form-group -->
													<input type="submit" class="btn btn-primary" value="Mettre à jour votre profil">
												</div><!-- .col-sm-7 .col-sm-offset-2 -->
											</div><!-- .row -->
										</div><!-- .panel-body -->
									</div><!-- .panel .panel-default -->
								</div><!-- col-md-8 -->
								<div class="col-md-4">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Supprimer votre compte</h3>
										</div><!-- .panel-heading -->
										<div class="panel-body">
											<p>Si vous souhaitez supprimer votre compte, cliquez sur le bouton ci-dessous.</p>
											<p><strong>FAITES ATTENTION! Si vous cliquez sur le lien ci-dessous, votre compte sera immédiatement et définitivement supprimé. Aucun retour!</strong></p>
											<a href="<?=base_url("user/" . $user->username . "/delete");?>" class="btn btn-danger btn-block btn-sm" onclick="return confirm('Voulez-vous vraiment supprimer votre compte? Si vous cliquez sur OK, votre compte sera immédiatement et définitivement supprimé.')">Supprimer votre compte</a>
										</div><!-- .panel-body -->
									</div><!-- .panel .panel-default -->
								</div><!-- .col-md-4 -->
							</form>
						</div><!-- .row -->
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container -->