<?php defined("BASEPATH") OR exit("No direct script access allowed"); ?>
			<div class="container">
				<div class="row">
					<?php if (isset($error)) : ?>
						<div class="col-md-12">
							<div class="alert alert-danger" role="alert">
								<?= $error ?>
							</div>
						</div>
					<?php endif; ?>
					<?php if (isset($register)) : ?>
						<div class="col-md-12">
							<div class="alert alert-success" role="alert">
								You have been succesfully registered!
							</div>
						</div>
					<?php endif; ?>
					<?php if (isset($logged)) : ?>
						<div class="col-md-12">
							<div class="alert alert-success" role="alert">
								You have been succesfully logged!
							</div>
						</div>
					<?php endif; ?>
					<?php if (isset($logout)) : ?>
						<div class="col-md-12">
							<div class="alert alert-success" role="alert">
								You have been succesfully logged-out!
							</div>
						</div>
					<?php endif; ?>
					<div class="col-md-12">
						<div class="page-header">
							<h1>Home</h1>
						</div>
						{{ Contenu }}
					</div>
				</div><!-- .row -->
			</div><!-- .container -->