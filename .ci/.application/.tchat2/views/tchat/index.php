<?php defined("BASEPATH") OR exit("No direct script access allowed");?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>TChat - Index</title>

		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Sans+Caption|Ubuntu">
		<link rel="stylesheet" type="text/css" href="../TChat/.static/css/styles.css?<?=microtime(true);?>">

		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/3.1.1/jquery.min.js?<?=microtime(true);?>"></script>
	</head>
	<body>
		<div class="notif" id="success">
			<div class="succes">
				<p id="bienvenue">Bonjour et bienvenue sur TChat, <b><?=$_SESSION["username"];?> !</b></p>
			</div>
		</div>
		<div class="notif" id="soon">
			<div class="success">
				<p id="bienvenue">Fonctionalitée en cours de développement !</b></p>
			</div>
		</div>
		<div id="header">
			<h1 id="ch-name"></h1>
<?php if($_SESSION["rights"] === 4) : ?>
			<input type="button" id="reset" name="reset" value="Reset" />
<?php endif;?>
			<input type="text" id="search" name="search" placeholder="🔍Rechercher">
			<input type="button" id="logout" name="logout" value="🚪">
		</div>
		<div id="footer">
			<form id="form" method="post">
				<input type="text" id="content" name="content" placeholder="Ecrivez votre message...">
				<input type="button" id="send" name="submit" value="↩">
				<input type="hidden" id="author" name="author" value="<?=$_SESSION["username"];?>">
			</form>
		</div>
		<div id="messages"></div>
		<div id="sidebar">
			TChat
			<ul id="user-logged">
				<li class="logged"><?=$_SESSION["username"];?></li>
			</ul>
			<div id="channels">
				Channels
				<input type="button" id="createchannel" name="createchannel" value="+">
				<ul id="ch"></ul>
			</div>
			<div id="pm">
				Direct Messages
				<!-- <input type="button" id="directmessage" name="directmessage" value="+"> -->
				<ul id="logged"></ul>
			</div>
		</div>
		<div id="copyright">
			<p class="date"></p>
			<p>Design by <a href="http://alexm.dijon.codeur.online" target="_blank">@Alex</a> & <a href="http://corentinp.dijon.codeur.online" target="_blank">@Kasai.</a></p>
			<p>Assistant Technique: <a href="http://yassinl.dijon.codeur.online" target="_blank">@CrackJ</a></p>
		</div>
		<script type="text/javascript">
			var channel = "general",user,ch_name,a,c;
			$(document).ready(function(){
				window.location="#success";
				setTimeout(function(){window.location="#";},1500);
				$(".date").html("© Corentin PERROT | "+d.getFullYear());
				$("#ch-name").html(channel);
				//$(window).on("unload",logout);
				$("#form").submit(send);
				$("#send").click(send);
<?php if($_SESSION["rights"] === 4) : ?>
				$("#reset").click(resetChannel);
<?php endif;?>
				$("#createchannel").click(createChannel);
				$("#logout").click(logout);
				//login();
			});
			var d=new Date();
			var soon=function(){window.location="#soon";setTimeout(function(){window.location="#";},3000);};
			//var login=function(){$.ajax({url:"api/v1/",method:"POST",data:"login=<?=$_SESSION['username'];?>"})};
			//var logout=function(){$.ajax({url:"api/v1/",method:"POST",data:"logout=<?=$_SESSION['username'];?>",success:function(){window.location="./login";}})};
			var send=function(e){a=encodeURIComponent($("#author").val());c=encodeURIComponent($("#content").val());if(a!=""&&c!=""){$.ajax({url:"api/v1/send",method:"POST",data:"channel="+channel+"&author="+a+"&content="+c,success:function(){$("#content").val("")}})}e.preventDefault();};
<?php if($_SESSION["rights"] === 4) : ?>
			var deleteChannel=function(a){$.ajax({url:"api/v1/delete",method:"POST",data:"channel="+a})};
			var deleteMessage=function(a,b){$.ajax({url:"api/v1/delete",method:"POST",data:"channel="+channel+"&message="+a+"&time="+b})}
			var deleteUser=function(a){$.ajax({url:"api/v1/delete",method:"POST",data:"usersdelete="+a})};
			var resetChannel=function(){$.ajax({url:"api/v1/reset",method:"POST",data:"channel="+channel})};
<?php endif;?>
			var createChannel=function(){ch_name=prompt("Entrez le nom de votre Channel");ch_mdp=prompt("Entrez le nouveau Mot de passe de votre Channel\nLaissez vide pour Aucun mot de passe.");$.ajax({url:"api/v1/create",method:"POST",data:"channel="+ch_name+"&mdp="+ch_mdp,success:function(){channel=ch_name}})};
			var getMessages=function(){$.ajax({url:"api/v1/get",method:"POST",data:"channel="+channel,success:function(data){$("#messages").html(data);}})};
			var getUsers=function(){$.ajax({url:"api/v1/get",method:"POST",data:"users=true",success:function(data){$("#logged").html(data);}})};
			var getChannels=function(){$.ajax({url:"api/v1/get",method:"POST",data:"channels=true",success:function(data){$("#ch").html(data);}})};
			var verifyChMdp=function(a){mdp=prompt("Entrez le mot de passe de votre Channel");$.ajax({url:"api/v1/verify",method:"POST",data:"channel="+a+"&mdp="+mdp,success:function(d){channel=a;}})};
			var it_get = setInterval(getMessages,1000);
			var it_users = setInterval(getUsers,1000);
			var it_channels = setInterval(getChannels,1000);
		</script>
	</body>
</html>