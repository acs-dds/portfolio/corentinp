<?php defined("BASEPATH") OR exit("No direct script access allowed");?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1">
<?php if(isset($refresh)) : ?>

		<meta http-equiv="refresh" content="2, url='<?=base_url();?>'" />
<?php endif;?>

		<title>TChat - Home</title>

		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">

		<!-- CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
		<link rel="stylesheet" href="<?=base_url("assets/css/style.css");?>">

		<!--[if lt IE 9]>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<header id="site-header">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?=base_url();?>">Home</a>
<?php if(isset($_SESSION["username"]) && $_SESSION["logged_in"] === true) : ?>
						<a class="navbar-brand" href="<?=base_url("tchat");?>">TChat</a>
<?php endif; ?>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
<?php if(isset($_SESSION["username"]) && $_SESSION["logged_in"] === true) : ?>
<?php if($_SESSION["rights"] >= 3) : ?>
							<li><a href="<?=base_url("admin");?>">Admin</a></li>
<?php endif; ?>
							<li><a href="<?=base_url("user/" . $_SESSION["username"]);?>">Profile</a></li>
							<li><a href="<?=base_url("logout");?>">Logout</a></li>
<?php else : ?>
							<li><a href="<?=base_url("register");?>">Register</a></li>
							<li><a href="<?=base_url("login");?>">Login</a></li>
<?php endif; ?>
						</ul>
					</div><!-- .navbar-collapse -->
				</div><!-- .container-fluid -->
			</nav><!-- .navbar -->
		</header><!-- #site-header -->

		<main id="site-content" role="main">
<?php if(ENVIRONMENT === "development") : ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p>Session : <?=var_dump($_SESSION);?></p>
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
<?php endif;?>