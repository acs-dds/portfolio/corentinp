<?php defined("BASEPATH") OR exit("No direct script access allowed");?>

		</main><!-- #site-content -->

		<footer id="site-footer" role="contentinfo"></footer><!-- #site-footer -->

		<!-- JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
	</body>
</html>