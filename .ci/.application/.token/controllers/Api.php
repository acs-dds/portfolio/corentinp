<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Api class.
 * 
 * @extends CI_Controller
 */
class Api extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library(array("session"));
		$this->load->helper(array("url"));
		$this->load->model(array("api_model"));

		$this->output->enable_profiler(TRUE);
	}

	/**
	 * index function.
	 * 
	 * @access public
	 * @return void
	 */
	public function index(){
		$this->load->view("home");
	}

	/**
	 * create function.
	 * 
	 * @access public
	 * @return void
	 */
	public function create($hotel = false){
		if($hotel === false){
			redirect(base_url());
			return;
		}

		$data = new stdClass();

		$token = $this->api_model->generer_token($hotel);
		$token2 = $this->api_model->generer_token_static(3);

		$data->token  = $token;
		$data->token2 = $token2;

		$this->load->view("api/create/create",$data);
	}

	/**
	 * send function.
	 * 
	 * @access public
	 * @return void
	 */
	public function send(){
		$this->load->view("api/send/index");
	}
}