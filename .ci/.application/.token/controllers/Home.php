<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Home class.
 * 
 * @extends CI_Controller
 */
class Home extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->output->enable_profiler(TRUE);
	}

	/**
	 * index function.
	 * 
	 * @access public
	 * @return void
	 */
	public function index(){
		$this->load->view("home");
	}
}