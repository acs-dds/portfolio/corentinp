<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Qrcode_Image class.
 * 
 * @extends CI_Controller
 */
class Qrcode_Image extends CI_Controller{
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library(array("session","ciqrcode"));
		$this->load->helper(array("url"));

		$this->output->enable_profiler(false);
	}

	/**
	 * index function.
	 * 
	 * @access public
	 * @return void
	 */
	public function index($data){
		$params['data'] = $data;
		$params['level'] = 'H';
		$params['size'] = 10000;

		$data = new stdClass();

		$data->image = $this->ciqrcode->generate($params);
		$this->load->view("qrcode",$data);
	}
}