<?php defined("BASEPATH") OR exit("No direct script access allowed");

/**
 * Forum_model class.
 * 
 * @extends CI_Model
 */
class Api_model extends CI_Model{

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();

		$this->load->database();
		$this->load->helper(array("url"));
	}

	/**
	 * crypto_rand_secure function.
	 * 
	 * @access public
	 * @param int $min
	 * @param int $max
	 * @return bool
	 */
	public function crypto_rand_secure($min,$max){
		$range = $max - $min;

		if($range < 1)
			return $min; // not so random...

		$log = ceil(log($range, 2));
		$bytes = (int)($log / 8) + 1; // length in bytes
		$bits = (int)$log + 1; // length in bits
		$filter = (int)(1 << $bits) - 1; // set all lower bits to 1

		do{
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		}
		while($rnd > $range);

		return $min + $rnd;
	}

	/**
	 * generer_token_static function.
	 * 
	 * @access public
	 * @param int $number
	 * @return string
	 */
	public function generer_token_static($number){
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet); // edited

		for ($i=0; $i < $number; $i++) {
			$token .= $codeAlphabet[$this->crypto_rand_secure(0,$max-1)];
		}

		return $token;
	}

	/**
	 * is_online function.
	 * 
	 * @access public
	 * @param int $id
	 * @return string
	 */
	public function is_offline($id){
		$this->db->select("status");
		$this->db->from("hotels");
		$this->db->where("id",$id);

		if($this->db->get()->row("status") == "offline"){
			return true;
		}
	}

	/**
	 * offline_token function.
	 * 
	 * @access public
	 * @param int $number
	 * @return string
	 */
	public function offline_token($id){
		$this->db->select("offline_token");
		$this->db->from("hotels");
		$this->db->where("id",$id);

		return $this->db->get()->row("offline_token");
	}

	/**
	 * generer_token function.
	 * 
	 * @access public
	 * @param int $number
	 * @return string
	 */
	public function generer_token($number){
		$this->db->select("token");
		$this->db->from("hotels");
		$this->db->where("id",$number);

		$hotel_id = $this->db->get()->row("token");

		if($this->is_offline($number)){
			return $hotel_id . $this->offline_token($number) . $this->generer_token_static(9);
		}
		else{
			return $hotel_id . $this->generer_token_static(12);
		}
	}
}