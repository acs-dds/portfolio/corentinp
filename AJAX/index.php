<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>AJAX</title>

		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/3.1.1/jquery.min.js"></script>
	</head>
	<body>
		<h1>Test</h1>

		<input type="button" id="send" value="Natif">
		<input type="button" id="send2" value="jQuery">

		<div class="natif">
			<h1 id="n"></h1>
		</div>

		<div class="jquery">
			<h1 id="j"></h1>
		</div>

		<script type="text/javascript" src=".static/js/script.min.js"></script>
	</body>
</html>