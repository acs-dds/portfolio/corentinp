<!DOCTYPE html>
<html id="html">
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>Test Balises HTML</title>

		<link rel="stylesheet" href=".static/css/style.css">
	</head>
	<body id="body">
		<section id="section">
			<h1 id="h1">Titre H1</h1>
			<h2 id="h2">Titre H2</h2>
			<p id="p">Texte en "p"</p>
			<ol id="ol">
				<li id="ol_li">Liste ordonnée</li>
				<li id="ol_li">Liste ordonnée</li>
			</ol>
			<ul id="ul">
				<li id="ul_li">Liste non ordonnée</li>
				<li id="ul_li">Liste non ordonnée</li>
			</ul>
			<blockquote id="blockquote" cite="http://example.com/facts">
				<p id="blockquote_p">Citation venant de example.com/facts</p>
			</blockquote>
		</section>
	</body>
</html>