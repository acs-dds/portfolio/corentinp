<?php require(__DIR__ . "/../../jeanp/daily/2401.php");?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>2401</title>

		<link rel="stylesheet" href="">
	</head>
	<body>
		<nav class="navbar navbar-light bg-faded">
			<ul class="nav navbar-nav">
<?php foreach($menu as $titre => $lien) : ?>
				<li class="nav-item">
<?php if($titre != "nos services") : ?>
					<a class="nav-link" href="<?=$lien;?>"><?=$titre;?></a>
<?php else : ?>
					<a class="nav-link"><?=$titre;?></a>
					<ul class="nav navbar-nav">
<?php foreach($lien as $sous_titre => $sous_lien) : ?>
						<li class="nav-item active">
							<a class="nav-link" href="<?=$sous_lien;?>"><?=$sous_titre;?></a>
						</li>
<?php endforeach;?>
					</ul>
<?php endif;?>
				</li>
<?php endforeach;?>
			</ul>
			<form class="form-inline navbar-form pull-right">
				<input class="form-control" type="text" placeholder="Search">
				<button class="btn btn-success-outline" type="submit">Search</button>
			</form>
		</nav>
	</body>
</html>