<?php require(__DIR__ . "/../../jeanp/daily/2501.php");?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>2501</title>

		<link rel="stylesheet" href="">
	</head>
	<body>
		Caractère "A" encodé puis décodé : "<?=encode("A");?>"
		<pre>
			1. Caractère "A" encodé puis décodé : "&lt;?=encode("A");?&gt;"
		</pre>
		<pre>
			1. <i style="color:blue;">function</i> <a style="color:green">encode</a>($<i style="color:orange">character</i>) {
			2. 	$c = <a style="color:blue;">ord</a>($character); 
			3. 	<a style="color:red;">if</a> (<a style="color:red;">!</a>($c <a style="color:red;">%</a> <a style="color:purple;">3</a>)) {
			4. 		<a style="color:red;">return</a> <a style="color:blue;">chr</a>($c <a style="color:red;">*</a> <a style="color:purple;">2</a> <a style="color:red;">+</a> <a style="color:purple;">15</a>);
			5. 	} <a style="color:red;">else return</a> <a style="color:blue;">chr</a>($c);
			6. }
		</pre>
		<p>1. : création d'une fonction nommée "encode" qui prend 1 (le chiffre) argument : "$character"</p>
		<p>2. : assignation du code ASCII (ord) de l'argument "$character" à la variable : "$c"</p>
		<p>3. : si le caractère ASCII ne se trouve pas dans la table "normale"</p>
		<p>4. : retourne le caractère venant de la variable "$c" en tant que caractère "humain" (en le replaçant dans la bonne table ASCII)</p>
		<p>5. : sinon retourne le caractère venant de la variable "$c" en tant que caractère "humain"</p>
		<p>6. : fin de la fonction.</p>
		<p>C'est l'adaptation en PHP du code "encode" qu'on avait fait en JavaScript !</p>
	</body>
</html>