<?php

require_once(".classes/mapper.php");
require_once(".classes/thememapper.php");

class Controller{
	protected $mapper;
	protected $theme_mapper;

	public function __construct(){
		$this->mapper = new Mapper();
		$this->theme_mapper = new ThemeMapper();
	}

	public function getContent($count){
		return $this->mapper->getContent($count);
	}

	public function createTheme($name,$data){
		return $this->theme_mapper->createTheme($name,$data);
	}

	public function listTheme(){
		return $this->theme_mapper->listTheme();
	}
}