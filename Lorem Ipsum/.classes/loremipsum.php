<?php

class LoremIpsum{

	public function __construct(){}

	public function getContent($theme = "default"){
		$longParag = 150;
		$f = fopen(__DIR__ . "/../.data/theme/" . $theme . ".csv","r");
		$csv = fgetcsv($f,0);

		$longTab = count($csv)-1;
		for ($i=0; $i < $longParag ; $i++) { 
			$randMot = rand(0,$longTab);
			$tabParag[] = $csv[$randMot];
		}
		$parag = implode(" ",$tabParag);
		$parag = ucfirst($parag).".";
		return $parag;
	}
}