<?php

class ThemeMapper{
	protected $file;

	public function __construct(){}

	public function createTheme($name,$data){
		if(file_exists($this->file = ".data/theme/" . $name . ".csv"))
			return "ERROR!";
		else{
			$f = fopen($this->file,"w");
			fputcsv($f,explode(" ",$data));
			fclose($f);
		}
	}

	public function listTheme(){
		$data = "";
		foreach(str_replace(array(".csv"),"",array_diff(scandir(".data/theme"),array(".",".."))) as $value) : 
			$data .= '<option value="' . $value . '">' . $value . '</option>' . "\n";
		endforeach;

		return $data;
	}

	/*public function __toString(){
		ob_start();
		require(".data/tpl/list.tpl");
		return ob_get_clean();
	}*/
}