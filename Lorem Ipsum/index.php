<?php require_once(".classes/controller.php");$controller = new Controller();?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>Lorem Ipsum</title>

		<link rel="stylesheet" href="">
	</head>
	<body>
		<form action="." method="post" accept-charset="utf-8">
			<label>Number of words</label>
			<input type="number" min="0" name="char" /><br>
			<label>Text per Paragraphe</label>
			<input type="number" min="0" name="para" />
			<!-- <select name="choice[]" multiple> -->
			<select name="choice" multiple>
				<?=$controller->listTheme();?>
			</select>
			<input type="submit" value="Send" />
		</form>
		<a href="create">Create my own Lipsum</a>
		<div id="output">
<?php if(isset($_POST["char"]) && isset($_POST["para"])) : ?>
			<?=$controller->getContent($_POST["choice"]);?>
<?php endif; ?>
		</div>
	</body>
</html>