<?php require(".classes/controller.php");?>
<form action="" method="post">
	<input type="text" name="name" placeholder="Name"><br>
	<textarea name="data" placeholder="lorem, ipsum, etc..." rows="7" cols="80"></textarea><br>
	<input type="submit" value="Create my Lipsum!">
</form>

<?php
if(isset($_POST["name"]) && isset($_POST["data"])) : 
	$controller = new Controller();
	$controller->createTheme($_POST["name"],$_POST["data"]);
	header("Location: ./");
endif;