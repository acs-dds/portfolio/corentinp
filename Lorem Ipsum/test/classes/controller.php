<?php 
require_once "classes/mapper.php";

class Controller {
	private $mapper;
	
	public function __construct() {
		$this->mapper = new Mapper();
	}

	public function actionGenerateAllParagraphe () {
		if (isset($_GET['Mot']) && isset($_GET['Paragraphe']) && isset($_GET['theme'])) {
			return $this->mapper->generateAllParagraphe($_GET['Mot'],$_GET['Paragraphe'],$_GET['theme']);
		}else {
			return $this->mapper->generateAllParagraphe();
		}		
	}
}