<?php 
class Mapper {

	public function generateParagraphe ($longParag = 150, $theme = "dataDefault") { 

		$h = fopen('/home/quentinp/lorem-ipsum/data/'.$theme.'.csv', 'r');
		$tabMots = fgetcsv($h, 0, ';');
	
		$longTab = count($tabMots)-1;
		for ($i=0; $i < $longParag ; $i++) { 
			$randMot = rand(0,$longTab);
			$tabParag[] = $tabMots[$randMot];
		}
		$parag = implode(" ",$tabParag);
		$parag = ucfirst($parag).".";
		return $parag;
	}

	public function generateAllParagraphe ($nbParagraphe = 1) {
		for ($i=0; $i <= $nbParagraphe-1 ; $i++) {
			$parags[] = "<p>".$this->generateParagraphe()."</p>";
		}
		$htmlparags = implode(" ",$parags);
		return $htmlparags;
	}
}